<?php
return [
    // system messages
    'can_not_delete_item_doc_exist' => 'A document exists for this item. It cannot be deleted.',
];
