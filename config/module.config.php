<?php

namespace Lerp\Order;

use Bitkorn\User\Entity\User\Rightsnroles;
use Laminas\I18n\Translator\Loader\PhpArray;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Order\Command\MasterDataCommand;
use Lerp\Order\Controller\Ajax\CustomerOrderOfferAjaxController;
use Lerp\Order\Controller\Ajax\Offer\OfferAjaxController;
use Lerp\Order\Controller\Ajax\Offer\OfferAnalysisAjaxController;
use Lerp\Order\Controller\Ajax\Offer\OfferItemAjaxController;
use Lerp\Order\Controller\Ajax\Offer\OfferMailController;
use Lerp\Order\Controller\Ajax\Order\Maint\OrderItemMaintBasicController;
use Lerp\Order\Controller\Ajax\Order\OfficeAjaxController;
use Lerp\Order\Controller\Ajax\Order\OrderAjaxController;
use Lerp\Order\Controller\Ajax\Order\OrderAnalysisAjaxController;
use Lerp\Order\Controller\Ajax\Order\OrderItemAjaxController;
use Lerp\Order\Controller\Ajax\Order\OrderItemListAjaxController;
use Lerp\Order\Controller\Ajax\Order\OrderMailController;
use Lerp\Order\Controller\Ajax\OrderOfferController;
use Lerp\Order\Controller\Rest\Offer\Files\FileOfferRestController;
use Lerp\Order\Controller\Rest\Offer\OfferItemRestController;
use Lerp\Order\Controller\Rest\Offer\OfferRestController;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintController;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintFileController;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintFindController;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintGottenController;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintPartController;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintWorkflowController;
use Lerp\Order\Controller\Rest\Order\OrderItemListItemRestController;
use Lerp\Order\Controller\Rest\Order\OrderRestController;
use Lerp\Order\Controller\Rest\Order\Service\OrderItemServiceRestController;
use Lerp\Order\Factory\Command\MasterDataCommandFactory;
use Lerp\Order\Factory\Controller\Ajax\CustomerOrderOfferAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Offer\OfferAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Offer\OfferAnalysisAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Offer\OfferItemAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Offer\OfferMailControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\Maint\OrderItemMaintBasicControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\OfficeAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\OrderAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\OrderAnalysisAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\OrderItemAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\OrderItemListAjaxControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\Order\OrderMailControllerFactory;
use Lerp\Order\Factory\Controller\Ajax\OrderOfferControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Offer\Files\FileOfferRestControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Offer\OfferItemRestControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Offer\OfferRestControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Maint\OrderItemMaintControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Maint\OrderItemMaintFileControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Maint\OrderItemMaintFindControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Maint\OrderItemMaintGottenControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Maint\OrderItemMaintPartControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Maint\OrderItemMaintWorkflowControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\OrderItemListItemRestControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\OrderRestControllerFactory;
use Lerp\Order\Factory\Controller\Rest\Order\Service\OrderItemServiceRestControllerFactory;
use Lerp\Order\Factory\Form\Offer\Files\FileOfferFormFactory;
use Lerp\Order\Factory\Form\Offer\OfferFormFactory;
use Lerp\Order\Factory\Form\Offer\OfferItemFormFactory;
use Lerp\Order\Factory\Form\Order\Files\FileOrderFormFactory;
use Lerp\Order\Factory\Form\Order\Maint\OrderItemMaintFileFormFactory;
use Lerp\Order\Factory\Form\Order\Maint\OrderItemMaintFindFormFactory;
use Lerp\Order\Factory\Form\Order\Maint\OrderItemMaintGottenFormFactory;
use Lerp\Order\Factory\Form\Order\Maint\OrderItemMaintPartFormFactory;
use Lerp\Order\Factory\Form\Order\OrderFormFactory;
use Lerp\Order\Factory\Form\Order\OrderItemFormFactory;
use Lerp\Order\Factory\Form\Order\Service\OrderItemServiceFormFactory;
use Lerp\Order\Factory\Service\Maint\OrderItemMaintServiceFactory;
use Lerp\Order\Factory\Service\Offer\Files\FileOfferRelServiceFactory;
use Lerp\Order\Factory\Service\Offer\OfferAnalysisServiceFactory;
use Lerp\Order\Factory\Service\Offer\OfferItemServiceFactory;
use Lerp\Order\Factory\Service\Offer\OfferMailServiceFactory;
use Lerp\Order\Factory\Service\Offer\OfferServiceFactory;
use Lerp\Order\Factory\Service\Order\Files\FileOrderRelServiceFactory;
use Lerp\Order\Factory\Service\Order\OrderAnalysisServiceFactory;
use Lerp\Order\Factory\Service\Order\OrderItemListServiceFactory;
use Lerp\Order\Factory\Service\Order\OrderItemServiceFactory;
use Lerp\Order\Factory\Service\Order\OrderItemServiceServiceFactory;
use Lerp\Order\Factory\Service\Order\OrderMailServiceFactory;
use Lerp\Order\Factory\Service\Order\OrderServiceFactory;
use Lerp\Order\Factory\Service\OrderGodServiceFactory;
use Lerp\Order\Factory\Service\OrderOfferServiceFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintFileTableFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintFindTableFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintGottenTableFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintPartTableFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintTableFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintToolTableFactory;
use Lerp\Order\Factory\Table\Maint\OrderItemMaintWorkflowTableFactory;
use Lerp\Order\Factory\Table\Offer\Files\FileOfferRelTableFactory;
use Lerp\Order\Factory\Table\Offer\Mail\MailSendOfferRelTableFactory;
use Lerp\Order\Factory\Table\Offer\OfferItemTableFactory;
use Lerp\Order\Factory\Table\Offer\OfferTableFactory;
use Lerp\Order\Factory\Table\Order\Files\FileOrderRelTableFactory;
use Lerp\Order\Factory\Table\Order\Mail\MailSendOrderRelTableFactory;
use Lerp\Order\Factory\Table\Order\OrderAnalysisTableFactory;
use Lerp\Order\Factory\Table\Order\OrderItemListTableFactory;
use Lerp\Order\Factory\Table\Order\OrderItemPartTableFactory;
use Lerp\Order\Factory\Table\Order\OrderItemTableFactory;
use Lerp\Order\Factory\Table\Order\OrderTableFactory;
use Lerp\Order\Factory\Table\Order\Service\OrderItemServiceTableFactory;
use Lerp\Order\Factory\Table\Order\ViewOrderItemListStockTableFactory;
use Lerp\Order\Factory\Table\Order\ViewOrderItemStockTableFactory;
use Lerp\Order\Factory\Unique\UniqueNumberProviderFactory;
use Lerp\Order\Form\Offer\Files\FileOfferForm;
use Lerp\Order\Form\Offer\OfferForm;
use Lerp\Order\Form\Offer\OfferItemForm;
use Lerp\Order\Form\Order\Files\FileOrderForm;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFileForm;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFindForm;
use Lerp\Order\Form\Order\Maint\OrderItemMaintGottenForm;
use Lerp\Order\Form\Order\Maint\OrderItemMaintPartForm;
use Lerp\Order\Form\Order\OrderForm;
use Lerp\Order\Form\Order\OrderItemForm;
use Lerp\Order\Form\Order\Service\OrderItemServiceForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Lerp\Order\Service\Offer\Files\FileOfferRelService;
use Lerp\Order\Service\Offer\OfferAnalysisService;
use Lerp\Order\Service\Offer\OfferItemService;
use Lerp\Order\Service\Offer\OfferMailService;
use Lerp\Order\Service\Offer\OfferService;
use Lerp\Order\Service\Order\Files\FileOrderRelService;
use Lerp\Order\Service\Order\OrderAnalysisService;
use Lerp\Order\Service\Order\OrderItemListService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Service\Order\OrderItemServiceService;
use Lerp\Order\Service\Order\OrderMailService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Service\OrderGodService;
use Lerp\Order\Service\OrderOfferService;
use Lerp\Order\Table\Offer\Files\FileOfferRelTable;
use Lerp\Order\Table\Offer\Mail\MailSendOfferRelTable;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Order\Table\Order\Files\FileOrderRelTable;
use Lerp\Order\Table\Order\Mail\MailSendOrderRelTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFileTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFindTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintGottenTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintPartTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintToolTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintWorkflowTable;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Order\Table\Order\OrderAnalysisTable;
use Lerp\Order\Table\Order\OrderItemListTable;
use Lerp\Order\Table\Order\OrderItemPartTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\OrderTable;
use Lerp\Order\Table\Order\Service\OrderItemServiceTable;
use Lerp\Order\Table\Order\ViewOrderItemListStockTable;
use Lerp\Order\Table\Order\ViewOrderItemStockTable;
use Lerp\Order\Unique\UniqueNumberProviderInterface;

return [
    'router'                             => [
        'routes' => [
            /*
             * REST - offer
             */
            'lerp_order_rest_offer'                                                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-offer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferRestController::class,
                    ],
                ],
            ],
            'lerp_order_rest_offeritem'                                             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-offer-item[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferItemRestController::class,
                    ],
                ],
            ],
            /*
             * REST - offer files
             */
            'lerp_order_rest_fileoffer'                                             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-offer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FileOfferRestController::class,
                    ],
                ],
            ],
            /*
             * REST - order
             */
            'lerp_order_rest_order'                                                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderRestController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitem'                                             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\Order\OrderItemRestController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemlist'                                         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-list[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\Order\OrderItemListRestController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemlistitem'                                     => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-list-item[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\Order\OrderItemListItemRestController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderaddress'                                          => [
                'type'         => Segment::class,
                'options'      => [
                    'route'       => '/lerp-order-rest-order-address[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\Order\OrderAddressRestController::class,
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_REST => [
                        Rightsnroles::METHOD_UPDATE => [
                            Rightsnroles::KEY_ROLEMIN => 4,
                        ],
                        Rightsnroles::METHOD_DELETE => [
                            Rightsnroles::KEY_ROLEMIN => 4,
                        ],
                    ],
                ],
            ],
            'lerp_order_rest_ordercontact'                                          => [
                'type'         => Segment::class,
                'options'      => [
                    'route'       => '/lerp-order-rest-order-contact[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\Order\OrderContactRestController::class,
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_REST => [
                        Rightsnroles::METHOD_UPDATE => [
                            Rightsnroles::KEY_ROLEMIN => 4,
                        ],
                        Rightsnroles::METHOD_DELETE => [
                            Rightsnroles::KEY_ROLEMIN => 4,
                        ],
                    ],
                ],
            ],
            /*
             * REST - order-item-service
             */
            'lerp_order_rest_service_orderitemservice'                              => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-service[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemServiceRestController::class,
                    ],
                ],
            ],
            /*
             * REST - order files
             */
            'lerp_order_rest_fileorder'                                             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-order[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\Order\Files\FileOrderRestController::class,
                    ],
                ],
            ],
            /*
             * REST - maint
             */
            'lerp_order_rest_orderitemmaint'                                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-maint[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemmaintworkflow'                                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-maint-workflow[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintWorkflowController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemmaintfile'                                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-maint-file[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintFileController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemmaintfind'                                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-maint-find[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintFindController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemmaintgotten'                                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-maint-gotten[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintGottenController::class,
                    ],
                ],
            ],
            'lerp_order_rest_orderitemmaintpart'                                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-rest-order-item-maint-part[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintPartController::class,
                    ],
                ],
            ],
            /*
             * AJAX - offer
             */
            'lerp_order_ajax_offer_summary'                                         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-offer-summary[/:offer_uuid]',
                    'constraints' => [
                        'offer_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferAjaxController::class,
                        'action'     => 'offerSummary'
                    ],
                ],
            ],
            'lerp_order_ajax_offer_test'                                            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-offer-products-files[/:offer_uuid]',
                    'constraints' => [
                        'offer_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferAjaxController::class,
                        'action'     => 'getOfferProductsFiles'
                    ],
                ],
            ],
            /*
             * AJAX - offer mail
             */
            'lerp_order_ajax_offermail_senddocuments'                               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-offermail-send-documents',
                    'defaults' => [
                        'controller' => OfferMailController::class,
                        'action'     => 'sendDocuments'
                    ],
                ],
            ],
            'lerp_order_ajax_offermail_mails'                                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-offermail-get-mails/:offer_uuid',
                    'constraints' => [
                        'offer_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferMailController::class,
                        'action'     => 'getOfferMails'
                    ],
                ],
            ],
            'lerp_order_ajax_offermail_mail'                                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-offermail-get-mail/:mail_send_offer_rel_uuid',
                    'constraints' => [
                        'mail_send_offer_rel_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferMailController::class,
                        'action'     => 'getOfferMail'
                    ],
                ],
            ],
            /*
             * AJAX - offer item
             */
            'lerp_order_ajax_offeritem_test'                                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-offeritem-test[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferItemAjaxController::class,
                        'action'     => 'test'
                    ],
                ],
            ],
            /*
             * AJAX - offer analysis
             */
            'lerp_order_ajax_offer_analysis_'                                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-offer-analysis-test[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferAnalysisAjaxController::class,
                        'action'     => 'test'
                    ],
                ],
            ],
            /*
             * AJAX - order
             */
            'lerp_order_ajax_order_order_ordersummary'                              => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-order-summary[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'orderSummary'
                    ],
                ],
            ],
            'lerp_order_ajax_order_usercontactupdate'                               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-user-contact-update/:order_uuid/:user_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                        'user_uuid'  => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'orderUserContactUpdate'
                    ],
                ],
            ],
            'lerp_order_ajax_order_orderproductsfiles'                              => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-order-products-files/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'getOrderProductsFiles'
                    ],
                ],
            ],
            'lerp_order_ajax_order_orderbynocompl'                                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-by-no-complete/:order_no_compl',
                    'constraints' => [
                        'order_no_compl' => '[0-9A-Za-z-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'getOrderByNumberComplete'
                    ],
                ],
            ],
            'lerp_order_ajax_order_finish'                                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-finish/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'finish'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_ROLEMIN => 3,
                ],
            ],
            'lerp_order_ajax_order_requestunlock'                                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-request-unlock/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'requestUnlock'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_ROLEMIN => 5,
                ],
            ],
            'lerp_order_ajax_order_unlock'                                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-unlock/:order_uuid/:unlock',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                        'unlock' => '[(true)(false)]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderAjaxController::class,
                        'action'     => 'unlock'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_ROLEMIN => 5,
                    Rightsnroles::KEY_RIGHTS  => [
                        'unlock_order_finish_real'
                    ],
                ],
            ],
            /*
             * AJAX - order-mail
             */
            'lerp_order_ajax_ordermail_senddocuments'                               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-ordermail-send-documents',
                    'defaults' => [
                        'controller' => OrderMailController::class,
                        'action'     => 'sendDocuments'
                    ],
                ],
            ],
            'lerp_order_ajax_ordermail_mails'                                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-ordermail-get-mails/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderMailController::class,
                        'action'     => 'getOrderMails'
                    ],
                ],
            ],
            'lerp_order_ajax_ordermail_mail'                                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-ordermail-get-mail/:mail_send_order_rel_uuid',
                    'constraints' => [
                        'mail_send_order_rel_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderMailController::class,
                        'action'     => 'getOrderMail'
                    ],
                ],
            ],
            /*
             * AJAX - orderItem
             */
            'lerp_order_ajax_order_orderitem_orderpriority'                         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-orderitem-orderpriority[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemAjaxController::class,
                        'action'     => 'orderPriority'
                    ],
                ],
            ],
            'lerp_order_ajax_order_orderitem_itemliststock'                         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-orderitem-itemliststock/:order_item_uuid/:product_uuid_parent',
                    'constraints' => [
                        'order_item_uuid'     => '[0-9A-Fa-f-]+',
                        'product_uuid_parent' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemAjaxController::class,
                        'action'     => 'itemListStock'
                    ],
                ],
            ],
            'lerp_order_ajax_order_orderitem_orderitemsstock'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-orderitems-stock/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemAjaxController::class,
                        'action'     => 'orderItemsStock'
                    ],
                ],
            ],
            /*
             * AJAX - order_item_list
             */
            'lerp_order_ajax_order_orderitemlist_orderitems'                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-orderitemlist-orderitems[/:order_item_uuid]',
                    'constraints' => [
                        'order_item_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemListAjaxController::class,
                        'action'     => 'orderItems'
                    ],
                ],
            ],
            'lerp_order_ajax_order_orderitemlist_orderitemlistlevelforfactoryorder' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-orderitemlist-level-for-factoryorder[/:order_item_uuid[/:product_uuid]]',
                    'constraints' => [
                        'order_item_uuid' => '[0-9A-Fa-f-]+',
                        'product_uuid'    => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemListAjaxController::class,
                        'action'     => 'orderItemListLevelForFactoryorder'
                    ],
                ],
            ],
            /*
             * AJAX - order analysis
             */
            'lerp_order_ajax_order_analysis_openings'                               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-order-ajax-order-analysis-grouped',
                    'defaults' => [
                        'controller' => OrderAnalysisAjaxController::class,
                        'action'     => 'grouped'
                    ],
                ],
            ],
            /*
             * AJAX - maint
             */
            'lerp_order_ajax_order_maintbasic_finishworkflow'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-maint-finish-workflow[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintBasicController::class,
                        'action'     => 'finishWorkflow'
                    ],
                ],
            ],
            'lerp_order_ajax_order_maintbasic_streamfile'                           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-ajax-maint-stream-file/:uuid/:session',
                    'constraints' => [
                        'uuid'    => '[0-9A-Fa-f-]+',
                        'session' => '[0-9a-f]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemMaintBasicController::class,
                        'action'     => 'streamFile'
                    ],
                ],
            ],
            /*
             * AJAX - office
             */
            'lerp_order_office_masterdataspreadsheet'                               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-office-spreadsheet-masterdata/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfficeAjaxController::class,
                        'action'     => 'masterDataSpreadsheet'
                    ],
                ],
            ],
            'lerp_order_office_groupedspreadsheet'                                  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-order-office-spreadsheet-grouped',
                    'defaults' => [
                        'controller' => OfficeAjaxController::class,
                        'action'     => 'groupedSpreadsheet'
                    ],
                ],
            ],
            /*
             * Stream
             */
            'lerp_order_stream_filestream_stream'                                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-filestream[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Stream\FilestreamController::class,
                        'action'     => 'stream'
                    ],
                ],
            ],
            /*
             * OrderOffer
             */
            'lerp_order_ajax_orderoffer_offertoorder'                               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-to-offer[/:offer_uuid]',
                    'constraints' => [
                        'offer_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderOfferController::class,
                        'action'     => 'offerToOrder'
                    ],
                ],
            ],
            'lerp_order_ajax_customerorderoffer_customerorders'                     => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-customer-orders[/:customer_uuid]',
                    'constraints' => [
                        'customer_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => CustomerOrderOfferAjaxController::class,
                        'action'     => 'customerOrders'
                    ],
                ],
            ],
            'lerp_order_ajax_customerorderoffer_customeroffers'                     => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-order-customer-offers[/:customer_uuid]',
                    'constraints' => [
                        'customer_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => CustomerOrderOfferAjaxController::class,
                        'action'     => 'customerOffers'
                    ],
                ],
            ],
        ],
    ],
    'controllers'                        => [
        'factories' => [
            // REST - offer
            OfferRestController::class                                 => OfferRestControllerFactory::class,
            OfferItemRestController::class                             => OfferItemRestControllerFactory::class,
            FileOfferRestController::class                             => FileOfferRestControllerFactory::class,
            // REST - order
            OrderRestController::class                                 => OrderRestControllerFactory::class,
            Controller\Rest\Order\OrderItemRestController::class       => Factory\Controller\Rest\Order\OrderItemRestControllerFactory::class,
            Controller\Rest\Order\OrderItemListRestController::class   => Factory\Controller\Rest\Order\OrderItemListRestControllerFactory::class,
            OrderItemListItemRestController::class                     => OrderItemListItemRestControllerFactory::class,
            Controller\Rest\Order\OrderAddressRestController::class    => Factory\Controller\Rest\Order\OrderAddressRestControllerFactory::class,
            Controller\Rest\Order\OrderContactRestController::class    => Factory\Controller\Rest\Order\OrderContactRestControllerFactory::class,
            // REST - order-files
            Controller\Rest\Order\Files\FileOrderRestController::class => Factory\Controller\Rest\Order\Files\FileOrderRestControllerFactory::class,
            // REST - order-service
            OrderItemServiceRestController::class                      => OrderItemServiceRestControllerFactory::class,
            // REST - maint
            OrderItemMaintController::class                            => OrderItemMaintControllerFactory::class,
            OrderItemMaintWorkflowController::class                    => OrderItemMaintWorkflowControllerFactory::class,
            OrderItemMaintFileController::class                        => OrderItemMaintFileControllerFactory::class,
            OrderItemMaintFindController::class                        => OrderItemMaintFindControllerFactory::class,
            OrderItemMaintGottenController::class                      => OrderItemMaintGottenControllerFactory::class,
            OrderItemMaintPartController::class                        => OrderItemMaintPartControllerFactory::class,
            // AJAX
            OrderOfferController::class                                => OrderOfferControllerFactory::class,
            CustomerOrderOfferAjaxController::class                    => CustomerOrderOfferAjaxControllerFactory::class,
            // AJAX - offer
            OfferAjaxController::class                                 => OfferAjaxControllerFactory::class,
            OfferItemAjaxController::class                             => OfferItemAjaxControllerFactory::class,
            OfferMailController::class                                 => OfferMailControllerFactory::class,
            OfferAnalysisAjaxController::class                         => OfferAnalysisAjaxControllerFactory::class,
            // AJAX - order
            OrderAjaxController::class                                 => OrderAjaxControllerFactory::class,
            OrderItemAjaxController::class                             => OrderItemAjaxControllerFactory::class,
            OrderItemListAjaxController::class                         => OrderItemListAjaxControllerFactory::class,
            OrderMailController::class                                 => OrderMailControllerFactory::class,
            OrderAnalysisAjaxController::class                         => OrderAnalysisAjaxControllerFactory::class,
            // AJAX - office
            OfficeAjaxController::class                                => OfficeAjaxControllerFactory::class,
            // AJAX - maint
            OrderItemMaintBasicController::class                       => OrderItemMaintBasicControllerFactory::class,
            // stream
            Controller\Stream\FilestreamController::class              => Factory\Controller\Stream\FileStreamControllerFactory::class,
        ],
    ],
    'service_manager'                    => [
        'factories' => [
            //
            OrderOfferService::class             => OrderOfferServiceFactory::class,
            // table - offer
            OfferTable::class                    => OfferTableFactory::class,
            OfferItemTable::class                => OfferItemTableFactory::class,
            FileOfferRelTable::class             => FileOfferRelTableFactory::class,
            // table - order
            OrderTable::class                    => OrderTableFactory::class,
            OrderAnalysisTable::class            => OrderAnalysisTableFactory::class,
            OrderItemTable::class                => OrderItemTableFactory::class,
            OrderItemPartTable::class            => OrderItemPartTableFactory::class,
            FileOrderRelTable::class             => FileOrderRelTableFactory::class,
            ViewOrderItemListStockTable::class   => ViewOrderItemListStockTableFactory::class,
            ViewOrderItemStockTable::class       => ViewOrderItemStockTableFactory::class,
            // table - order_item_service
            OrderItemServiceTable::class         => OrderItemServiceTableFactory::class,
            // table - order_item_maint
            OrderItemMaintTable::class           => OrderItemMaintTableFactory::class,
            OrderItemMaintFileTable::class       => OrderItemMaintFileTableFactory::class,
            OrderItemMaintFindTable::class       => OrderItemMaintFindTableFactory::class,
            OrderItemMaintGottenTable::class     => OrderItemMaintGottenTableFactory::class,
            OrderItemMaintPartTable::class       => OrderItemMaintPartTableFactory::class,
            OrderItemMaintToolTable::class       => OrderItemMaintToolTableFactory::class,
            OrderItemMaintWorkflowTable::class   => OrderItemMaintWorkflowTableFactory::class,
            // table - order item list
            OrderItemListTable::class            => OrderItemListTableFactory::class,
            // table mail
            MailSendOfferRelTable::class         => MailSendOfferRelTableFactory::class,
            MailSendOrderRelTable::class         => MailSendOrderRelTableFactory::class,
            // service
            OrderGodService::class               => OrderGodServiceFactory::class,
            // service - offer
            OfferService::class                  => OfferServiceFactory::class,
            OfferItemService::class              => OfferItemServiceFactory::class,
            OfferMailService::class              => OfferMailServiceFactory::class,
            FileOfferRelService::class           => FileOfferRelServiceFactory::class,
            OfferAnalysisService::class          => OfferAnalysisServiceFactory::class,
            // service - order
            OrderService::class                  => OrderServiceFactory::class,
            OrderItemService::class              => OrderItemServiceFactory::class,
            OrderItemServiceService::class       => OrderItemServiceServiceFactory::class,
            OrderItemMaintService::class         => OrderItemMaintServiceFactory::class,
            OrderItemListService::class          => OrderItemListServiceFactory::class,
            OrderMailService::class              => OrderMailServiceFactory::class,
            FileOrderRelService::class           => FileOrderRelServiceFactory::class,
            OrderAnalysisService::class          => OrderAnalysisServiceFactory::class,
            // form - offer
            OfferForm::class                     => OfferFormFactory::class,
            OfferItemForm::class                 => OfferItemFormFactory::class,
            FileOfferForm::class                 => FileOfferFormFactory::class,
            // form - order
            OrderForm::class                     => OrderFormFactory::class,
            OrderItemForm::class                 => OrderItemFormFactory::class,
            OrderItemServiceForm::class          => OrderItemServiceFormFactory::class,
            OrderItemMaintFileForm::class        => OrderItemMaintFileFormFactory::class,
            OrderItemMaintFindForm::class        => OrderItemMaintFindFormFactory::class,
            OrderItemMaintGottenForm::class      => OrderItemMaintGottenFormFactory::class,
            OrderItemMaintPartForm::class        => OrderItemMaintPartFormFactory::class,
            FileOrderForm::class                 => FileOrderFormFactory::class,
            // unique
            UniqueNumberProviderInterface::class => UniqueNumberProviderFactory::class,
            // command
            MasterDataCommand::class             => MasterDataCommandFactory::class,
        ],
    ],
    'laminas-cli'                        => [
        /**
         * use it: vendor/bin/laminas lerp:*
         */
        'commands' => [
            'lerp:create-master-data-spreadsheet' => MasterDataCommand::class,
        ],
    ],
    'translator'                         => [
        'locale'                    => ['de_DE', 'en_GB'],
        'translation_file_patterns' => [
            [
                'type'        => PhpArray::class,
                'base_dir'    => __DIR__ . '/../language',
                'pattern'     => '%s.php',
                'text_domain' => 'lerp_order'
            ],
        ],
    ],
    'bitkorn_files_category_brands'      => [
        /**
         * Each module can have its own category brands for table 'file_category_rel'.
         * The key is known in the module.
         */
        'lerp_order_order' => 'order',
        'lerp_order_offer' => 'offer',
    ],
    'bitkorn_files_category_ids_exclude' => [
        /**
         * The key is the value from one entry in module.config.php[bitkorn_files_category_brands]
         */
        'order' => [7, 8],
    ],
    'lerp_order'                         => [
        'module_brand'              => 'order',
        'path_maint'                => __DIR__ . '/../../../../data/files/upload/maint',
        'file_category_id_excludes' => [7, 8], // never in regular file upload (Form 1 compute & Form 1 signed)
    ]
];
