<?php

namespace Lerp\Order\Unique;

use Laminas\Log\Logger;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Order\Table\Order\OrderTable;

/**
 * Using:
 * 1. init()
 * 2. generate(order | offer)
 * 3. getNumberComplete()
 *
 * Using quickly:
 * 1. computeGetNumberComplete(DocumentService::DOC_TYPE_*)
 */
class UniqueNumberProvider implements UniqueNumberProviderInterface
{
    const TYPE_ORDER = 1;
    const TYPE_OFFER = 2;
    protected array $types = [1 => 'order', 2 => 'offer'];

    const PAD_LENGTH = 4;
    const PAD_STRING = '0';

    /**
     * $this->prefixLeft . $this->yearShort . '-' . self::PREFIX_LOCATION + self::PAD_LENGTH
     */
    const UNIQUE_LENGTH = 11;
    protected Logger $logger;

    /**
     * Standort Nummer
     */
    const PREFIX_LOCATION = 9;
    protected string $prefixLeft;
    protected int $year;
    protected int $yearShort;
    protected \DateTime $dateTimeFrom;
    protected \DateTime $dateTimeTo;
    protected int $number;
    protected string $numberComplete;
    protected string $message;

    protected OrderTable $orderTable;
    protected OfferTable $offerTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setOrderTable(OrderTable $orderTable): void
    {
        $this->orderTable = $orderTable;
    }

    public function setOfferTable(OfferTable $offerTable): void
    {
        $this->offerTable = $offerTable;
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        $this->year = date('Y');
        $this->yearShort = substr($this->year, 2, 2);
        $this->dateTimeFrom = new \DateTime($this->year . '-01-01 00:00:00');
        $this->dateTimeTo = new \DateTime($this->year . '-12-31 23:59:59');
    }

    /**
     * @param int $type On of the self::TYPE_*
     * @return bool
     */
    public function generate(int $type): bool
    {
        if (!array_key_exists($type, $this->types)) {
            $this->message = 'Type is not supported.';
            return false;
        }
        if (!isset($this->dateTimeFrom) || !isset($this->dateTimeTo)) {
            $this->message = 'Dates are not set. Perhaps you forgot to call the init() method!?';
            return false;
        }
        switch ($type) {
            case self::TYPE_ORDER:
                if (($this->number = $this->orderTable->getMaxOrderNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-order number';
                    return false;
                }
                $this->prefixLeft = 'AUF';
                break;
            case self::TYPE_OFFER:
                if (($this->number = $this->offerTable->getMaxOfferNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-offer number';
                    return false;
                }
                $this->prefixLeft = 'ANG';
                break;
        }
        $this->number++;
        return $this->computeGlue();
    }

    protected function computeGlue(): bool
    {
        if (empty($this->prefixLeft) || empty($this->yearShort) || empty($this->number)) {
            return false;
        }
        $this->numberComplete = $this->prefixLeft . $this->yearShort . '-' . self::PREFIX_LOCATION
            . str_pad($this->number, self::PAD_LENGTH, self::PAD_STRING, STR_PAD_LEFT);
        return strlen($this->numberComplete) == self::UNIQUE_LENGTH;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getNumberComplete(): string
    {
        return $this->numberComplete;
    }

    /**
     * @param int $type On of the self::TYPE_*
     * @return string
     */
    public function computeGetNumberComplete(int $type): string
    {
        try {
            $this->init();
        } catch (\Exception $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $e->getMessage());
            $this->message = 'Error while init()';
            return '';
        }
        if (!$this->generate($type)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            $this->logger->err('message: ' . $this->message);
            return '';
        }
        return $this->numberComplete;
    }

    /**
     * @return string If $this->generate() returns false then there will be a message.
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    public function validUniqueNumber(string $uniqueNumber): bool
    {
        return preg_match('/[A-Z]{3}\d{2}-\d{' . (self::PAD_LENGTH + 1) . '}$/', $uniqueNumber) === 1;
    }
}
