<?php

namespace Lerp\Order\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemListEntity;

class OrderItemListTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_list';

    /**
     * view_order_item_list - JOIN product, product_no, order_item, quantityunit.
     * @param string $orderItemListUuid
     * @return array With quantityunit_uuid from product, because order_item_list does not have its own quantityunit_uuid.
     */
    public function getOrderItemList(string $orderItemListUuid): array
    {
        $select = new Select('view_order_item_list');
        try {
            $select->where(['order_item_list_uuid' => $orderItemListUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemListUuid
     * @return array From view_order_item_list_for_factoryorder.
     */
    public function getOrderItemListForFactoryorder(string $orderItemListUuid): array
    {
        $select = new Select('view_order_item_list_for_factoryorder');
        try {
            $select->where(['order_item_list_uuid' => $orderItemListUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected string $queryViewJoin = 'SELECT * FROM lerp_get_order_item_list_recursive(?,?) AS vl
        LEFT JOIN view_order_item_list voil ON voil.order_item_list_uuid = vl.order_item_list_uuid';

    public function getOrderItemListsForOrderItemHierarchic(string $orderItemUuid, string $productUuid): array
    {
        $params = new ParameterContainer([$orderItemUuid, $productUuid]);
        $stmt = $this->adapter->createStatement($this->queryViewJoin, $params);
        try {
            $result = $stmt->execute();
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        $resultArr = [];
        if (!$result->valid() || $result->count() < 1) {
            return $resultArr;
        }
        do {
            $resultArr[] = $result->current();
            $result->next();
        } while ($result->valid());
        return $resultArr;
    }

    /**
     * view_order_item_list - JOIN product, product_no, order_item, quantityunit.
     * @param string $orderItemUuid
     * @return array With quantityunit_uuid from product, because order_item_list does not have its own quantityunit_uuid.
     */
    public function getOrderItemListsForOrderItem(string $orderItemUuid): array
    {
        $select = new Select('view_order_item_list');
        try {
            $select->where(['view_order_item_list.order_item_uuid' => $orderItemUuid]);
            $select->order('order_item_list_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @return array From db.view_order_item_list_for_factoryorder
     */
    public function getOrderItemListLevelForFactoryorder(string $orderItemUuid, string $productUuidParent): array
    {
        $select = new Select('view_order_item_list_for_factoryorder');
        try {
            $select->where([
                'view_order_item_list_for_factoryorder.order_item_uuid' => $orderItemUuid
                , 'view_order_item_list_for_factoryorder.product_uuid_parent' => $productUuidParent
            ]);
            $select->order('order_item_list_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param OrderItemListEntity $entity
     * @return string
     */
    public function insertOrderItemList(OrderItemListEntity $entity): string
    {
        $insert = $this->sql->insert();
        $entity->setUuid($this->uuid());
        $entity->unsetEmptyValues();
        try {
            $insert->values($entity->getStorage());
            if ($this->insertWith($insert) > 0) {
                return $entity->getUuid();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxOrderPriority(string $orderItemUuid, string $productUuidParent): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_priority' => new Expression('MAX(order_item_list_order_priority)')]);
            $select->where(['order_item_uuid' => $orderItemUuid, 'product_uuid_parent' => $productUuidParent]);
            $select->group('product_uuid_parent');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return intval($result->current()['max_priority']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteOrderItemListItem(string $orderItemListUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_list_uuid' => $orderItemListUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemListsForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_uuid' => $orderItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @return array Array with product_uuid's
     */
    public function getOrderItemListRecursivParents(string $orderItemUuid, string $productUuidParent): array
    {
        $params = new ParameterContainer([$orderItemUuid, $productUuidParent]);
        $stmt = $this->adapter->createStatement('SELECT * FROM lerp_get_order_item_list_recursive_parents(?,?)', $params);
        $result = $stmt->execute($params);
        $resultArr = [];
        if (!$result->valid() || $result->count() < 1) {
            return $resultArr;
        }
        do {
            $resultArr[] = $result->current()['product_uuid_parent'];
            $result->next();
        } while ($result->valid());
        return $resultArr;
    }

    public function getNeighborsByListUuid(string $orderItemListUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectUuidParent = $this->sql->select();
            $selectUuidParent->columns(['order_item_uuid', 'product_uuid_parent']);
            $selectUuidParent->where(['order_item_list_uuid' => $orderItemListUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($selectUuidParent);
            if (!$result->valid() || $result->count() != 1) {
                return [];
            }
            $orderItemList = $result->current();
            $select->where(['order_item_uuid' => $orderItemList['order_item_uuid'], 'product_uuid_parent' => $orderItemList['product_uuid_parent']]);
            $select->order('order_item_list_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existNeighbor(string $orderItemUuid, string $productUuidParent, string $productUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_uuid' => $orderItemUuid, 'product_uuid_parent' => $productUuidParent, 'product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $orderItemListUuid
     * @param int $orderPriority
     * @return int
     */
    public function updateOrderItemListItemOrderPriority(string $orderItemListUuid, int $orderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['order_item_list_order_priority' => $orderPriority]);
            $update->where(['order_item_list_uuid' => $orderItemListUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateOrderItemListItemQuantity(string $orderItemListUuid, float $quantity): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['order_item_list_quantity' => $quantity]);
            $update->where(['order_item_list_uuid' => $orderItemListUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
