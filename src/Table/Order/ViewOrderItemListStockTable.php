<?php

namespace Lerp\Order\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ViewOrderItemListStockTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_order_item_list_stock';

    /**
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @return array
     */
    public function getOrderItemListStock(string $orderItemUuid, string $productUuidParent): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_uuid' => $orderItemUuid, 'product_uuid_parent' => $productUuidParent]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
