<?php

namespace Lerp\Order\Table\Order\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class OrderItemMaintPartTable extends AbstractLibTable
{
	/** @var string */
	protected $table = 'order_item_maint_part';

	/**
	 * @param string $orderItemMaintPartUuid
	 * @return array
	 */
	public function getOrderItemMaintPart(string $orderItemMaintPartUuid)
	{
		$select = $this->sql->select();
		try {
		    $select->where(['order_item_maint_part_uuid' => $orderItemMaintPartUuid]);
		    /** @var HydratingResultSet $result */
		    $result = $this->selectWith($select);
		    if ($result->valid() && $result->count() > 0) {
		        return $result->current()->getArrayCopy();
		    }
		} catch (\Exception $exception) {
		    $this->log($exception, __CLASS__, __FUNCTION__);
		}
		return [];
	}

    public function getOrderItemMaintParts(string $orderItemMaintUuid, string $orderItemMaintGottenUuid): array
    {
        $select = new Select('view_order_item_maint_part');
        try {
            if(!empty($orderItemMaintUuid) && empty($orderItemMaintGottenUuid)) {
                $select->where(['order_item_maint_uuid' => $orderItemMaintUuid]);
                $select->where(['order_item_maint_gotten_uuid' => null]);
            }
            if(!empty($orderItemMaintGottenUuid)) {
                $select->where(['order_item_maint_gotten_uuid' => $orderItemMaintGottenUuid]);
            }
            $select->order('order_item_maint_part_time_create ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemMaintPartForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $selectMaint = new Select('order_item_maint');
            $selectMaint->columns(['order_item_maint_uuid']);
            $selectMaint->where(['order_item_uuid' => $orderItemUuid]);

            $delete->where->in('order_item_maint_uuid', $selectMaint);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
