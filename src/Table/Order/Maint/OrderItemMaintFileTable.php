<?php

namespace Lerp\Order\Table\Order\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class OrderItemMaintFileTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_maint_file';

    /**
     * @param string $orderItemMaintFileUuid
     * @return array
     */
    public function getOrderItemMaintFile(string $orderItemMaintFileUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_maint_file_uuid' => $orderItemMaintFileUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertOrderItemMaintFile(array $storage): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $this->unsetNullFields($storage, true);
        $storage['order_item_maint_file_uuid'] = $uuid;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemMaintFileForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $selectMaint = new Select('order_item_maint');
            $selectMaint->columns(['order_item_maint_uuid']);
            $selectMaint->where(['order_item_uuid' => $orderItemUuid]);

            $delete->where->in('order_item_maint_uuid', $selectMaint);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteOrderItemMaintFile(string $orderItemMaintFileUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_maint_file_uuid' => $orderItemMaintFileUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getOrderItemMaintFilesByType(string $type, string $uuid): array
    {
        $select = $this->sql->select();
        try {
            switch ($type) {
                case 'maint':
                    $select->where(
                        [
                            'order_item_maint_uuid' => $uuid,
                            'order_item_maint_workflow_uuid' => null,
                            'order_item_maint_find_uuid' => null,
                            'order_item_maint_gotten_uuid' => null,
                        ]
                    );
                    break;
                case 'workflow':
                    $select->where(['order_item_maint_workflow_uuid' => $uuid]);
                    break;
                case 'finding':
                    $select->where(['order_item_maint_find_uuid' => $uuid]);
                    break;
                case 'gotten':
                    $select->where(['order_item_maint_gotten_uuid' => $uuid]);
                    break;
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
