<?php

namespace Lerp\Order\Table\Order\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class OrderItemMaintWorkflowTable extends AbstractLibTable
{
	/** @var string */
	protected $table = 'order_item_maint_workflow';

	/**
	 * @param string $orderItemMaintWorkflowUuid
	 * @return array
	 */
	public function getOrderItemMaintWorkflow(string $orderItemMaintWorkflowUuid)
	{
		$select = $this->sql->select();
		try {
		    $select->where(['order_item_maint_workflow_uuid' => $orderItemMaintWorkflowUuid]);
		    /** @var HydratingResultSet $result */
		    $result = $this->selectWith($select);
		    if ($result->valid() && $result->count() > 0) {
		        return $result->current()->getArrayCopy();
		    }
		} catch (\Exception $exception) {
		    $this->log($exception, __CLASS__, __FUNCTION__);
		}
		return [];
	}

    public function getOrderItemMaintWorkflowsForOrderItemMaint(string $orderItemMaintUuid): array
    {
        $select = new Select('view_order_item_maint_workflow');
        try {
            $select->where(['order_item_maint_uuid' => $orderItemMaintUuid]);
            $select->order('order_item_maint_workflow_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertOrderItemMaint(array $storage): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $storage['order_item_maint_workflow_uuid'] = $uuid;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemMaintWorkflowForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $selectMaint = new Select('order_item_maint');
            $selectMaint->columns(['order_item_maint_uuid']);
            $selectMaint->where(['order_item_uuid' => $orderItemUuid]);

            $delete->where->in('order_item_maint_uuid', $selectMaint);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function finishOrderItemMaintWorkflow(string $maintWorkflowUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'order_item_maint_workflow_time_finish' => $this->getTimestamp()
            ]);
            $update->where(['order_item_maint_workflow_uuid' => $maintWorkflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
