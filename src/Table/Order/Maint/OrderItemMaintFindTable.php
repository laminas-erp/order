<?php

namespace Lerp\Order\Table\Order\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintFindEntity;

class OrderItemMaintFindTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_maint_find';

    /**
     * @param string $orderItemMaintFindUuid
     * @return array
     */
    public function getOrderItemMaintFind(string $orderItemMaintFindUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_maint_find_uuid' => $orderItemMaintFindUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertOrderItemMaintFind(OrderItemMaintFindEntity $enity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $enity->setPrimaryKeyValue($uuid);
        $enity->unsetEmptyValues();
        try {
            $insert->values($enity->getStorage());
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getOrderItemMaintFindsForOrderItemMaint(string $orderItemMaintUuid): array
    {
        $select = new Select('view_order_item_maint_find');
        try {
            $select->where(['order_item_maint_uuid' => $orderItemMaintUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemMaintFindForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $selectMaint = new Select('order_item_maint');
            $selectMaint->columns(['order_item_maint_uuid']);
            $selectMaint->where(['order_item_uuid' => $orderItemUuid]);

            $delete->where->in('order_item_maint_uuid', $selectMaint);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
