<?php

namespace Lerp\Order\Table\Order\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class OrderItemMaintTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_maint';

    /**
     * @param string $orderItemMaintUuid
     * @return array
     */
    public function getOrderItemMaint(string $orderItemMaintUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_maint_uuid' => $orderItemMaintUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOrderItemMaintsForOrderItem(string $orderItemUuid): array
    {
        $select = new Select('view_order_item_maint');
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            $select->order('order_item_maint_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertOrderItemMaint(array $storage): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $storage['order_item_maint_uuid'] = $uuid;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemMaintForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_uuid' => $orderItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
