<?php

namespace Lerp\Order\Table\Order\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintGottenEntity;

class OrderItemMaintGottenTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_maint_gotten';

    /**
     * @param string $orderItemMaintGottenUuid
     * @return array
     */
    public function getOrderItemMaintGotten(string $orderItemMaintGottenUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_maint_gotten_uuid' => $orderItemMaintGottenUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOrderItemMaintGottens(string $orderItemMaintUuid, string $orderItemMaintFindUuid): array
    {
        $select = new Select('view_order_item_maint_gotten');
        try {
            if(!empty($orderItemMaintUuid)) {
                $select->where(['order_item_maint_uuid' => $orderItemMaintUuid]);
                $select->where(['order_item_maint_find_uuid' => null]);
            }
            if(!empty($orderItemMaintFindUuid)) {
                $select->where(['order_item_maint_find_uuid' => $orderItemMaintFindUuid]);
            }
            $select->order('order_item_maint_gotten_time_create ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemMaintGottenForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $selectMaint = new Select('order_item_maint');
            $selectMaint->columns(['order_item_maint_uuid']);
            $selectMaint->where(['order_item_uuid' => $orderItemUuid]);

            $delete->where->in('order_item_maint_uuid', $selectMaint);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
