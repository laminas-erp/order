<?php

namespace Lerp\Order\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Order\Unique\UniqueNumberProvider;

class OrderTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order';

    /**
     * @param string $orderUuid
     * @return array From view_order.
     */
    public function getOrder(string $orderUuid): array
    {
        $select = new Select('view_order');
        try {
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderNoCompl
     * @return array From view_order.
     */
    public function getOrderByNumberComplete(string $orderNoCompl): array
    {
        $select = new Select('view_order');
        try {
            $select->where(['order_no_compl' => $orderNoCompl]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $customerUuid
     * @return array From view_order.
     */
    public function getCustomerOrders(string $customerUuid): array
    {
        $select = new Select('view_order');
        try {
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param OrderEntity $orderEntity
     * @return string
     */
    public function insertOrder(OrderEntity $orderEntity): string
    {
        $insert = $this->sql->insert();
        $storage = $orderEntity->getStorage();
        if (empty($storage)) {
            return '';
        }
        $this->unsetNullFields($storage);
        $storage['order_uuid'] = $this->uuid();
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $storage['order_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderNoCompl
     * @param string $label
     * @param string $customerNo
     * @param string $offerNoCompl
     * @param int $costCentreId
     * @param string $locationPlaceUuid
     * @param string $timeCreateFrom
     * @param string $timeCreateTo
     * @param string $timeFinishScheduleFrom
     * @param string $timeFinishScheduleTo
     * @param string $timeFinishRealFrom
     * @param string $timeFinishRealTo
     * @param string $userUuidCreate
     * @param bool $onlyOpen
     * @param bool $onlyFinish
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @param bool $doCount
     * @return array From view_order.
     */
    public function searchOrder(
        string   $orderNoCompl,
        string   $label
        , string $customerNo
        , string $offerNoCompl
        , int    $costCentreId
        , string $locationPlaceUuid
        , string $timeCreateFrom
        , string $timeCreateTo
        , string $timeFinishScheduleFrom
        , string $timeFinishScheduleTo
        , string $timeFinishRealFrom
        , string $timeFinishRealTo
        , string $userUuidCreate
        , bool   $onlyOpen
        , bool   $onlyFinish
        , string $orderField, string $orderDirec, int $offset = 0, int $limit = 0, bool $doCount = false
    ): array
    {
        $select = new Select('view_order');
        try {
            if ($doCount) {
                $select->columns(['count_orders' => new Expression('COUNT(order_uuid)')]);
            }
            if (!empty($orderNoCompl)) {
                $select->where->like(new Expression('order_no_compl'), "%$orderNoCompl%");
            }
            if (!empty($label)) {
                $select->where->like('order_label', "%$label%");
            }
            if (!empty($customerNo)) {
                $select->where->like(new Expression('CAST(customer_no AS TEXT)'), "%$customerNo%");
            }
            if (!empty($offerNoCompl)) {
                $select->where->like(new Expression('offer_no_compl'), "%$offerNoCompl%");
            }
            if (!empty($costCentreId)) {
                $select->where(['cost_centre_id' => $costCentreId]);
            }
            if (!empty($locationPlaceUuid)) {
                $select->where(['location_place_uuid' => $locationPlaceUuid]);
            }
            if (!empty($timeCreateFrom)) {
                $select->where->greaterThanOrEqualTo('order_time_create', $timeCreateFrom);
            }
            if (!empty($timeCreateTo)) {
                $select->where->lessThanOrEqualTo('order_time_create', $timeCreateTo);
            }
            if (!empty($timeFinishScheduleFrom)) {
                $select->where->greaterThanOrEqualTo('order_time_finish_schedule', $timeFinishScheduleFrom);
            }
            if (!empty($timeFinishScheduleTo)) {
                $select->where->lessThanOrEqualTo('order_time_finish_schedule', $timeFinishScheduleTo);
            }
            if (!empty($timeFinishRealFrom)) {
                $select->where->greaterThanOrEqualTo('order_time_finish_real', $timeFinishRealFrom);
            }
            if (!empty($timeFinishRealTo)) {
                $select->where->lessThanOrEqualTo('order_time_finish_real', $timeFinishRealTo);
            }
            if (!empty($userUuidCreate)) {
                $select->where(['user_uuid_create' => $userUuidCreate]);
            }
            if (!empty($onlyOpen)) {
                $select->where->isNull('order_time_finish_real');
            } else if (!empty($onlyFinish)) {
                $select->where->isNotNull('order_time_finish_real');
            }
            if (!$doCount && !empty($orderField) && !empty($orderDirec)) {
                $select->order($orderField . ' ' . $orderDirec);
            }
            if (!$doCount && (!empty($offset) || !empty($limit))) {
                $select->limit($limit)->offset($offset);
            }

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$doCount) {
                    return $result->toArray();
                } else {
                    return [intval($result->current()['count_orders'])];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        if (!$doCount) {
            return [];
        } else {
            return [0];
        }
    }

    public function updateOrder(OrderEntity $orderEntity): int
    {
        $update = $this->sql->update();
        $storage = $orderEntity->getStorage();
        if (empty($storage)) {
            return '';
        }
        $this->unsetNullFields($storage);
        unset($storage['order_uuid']);
        unset($storage['order_no']);
        unset($storage['customer_uuid']);
        unset($storage['offer_uuid']);
        unset($storage['order_time_create']);
        $storage['order_time_update'] = $this->getTimestamp();
        try {
            $update->set($storage);
            $update->where(['order_uuid' => $orderEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderUuid
     * @return array Array keys: total_sum, total_sum_tax, total_sum_end
     */
    public function getOrderSummary(string $orderUuid): array
    {
        $params = new ParameterContainer([$orderUuid]);
        $stmt = $this->getAdapter()->getDriver()->createStatement('SELECT * FROM lerp_get_order_summary(:order_uuid)');
        /** @var Result $result */
        $result = $stmt->execute($params);
        if ($result->valid() && $result->count() == 1) {
            return $result->current();
        }
        return [];
    }

    public function updateOrderUserContact(string $orderUuid, string $userUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_uuid_contact' => $userUuid]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteAddressCustomerRelUuid(string $orderUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['address_customer_rel_uuid' => null]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateAddressCustomerRelUuid(string $orderUuid, string $addressCustomerRelUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['address_customer_rel_uuid' => $addressCustomerRelUuid]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteContactCustomerRelUuid(string $orderUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['contact_customer_rel_uuid' => null]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateContactCustomerRelUuid(string $orderUuid, string $contactCustomerRelUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['contact_customer_rel_uuid' => $contactCustomerRelUuid]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * The maximum of fetched number is `UniqueNumberProvider::PAD_LENGTH` digits long (e.g. 4 => max: 9999).
     * @param \DateTime $dateTimeFrom
     * @param \DateTime $dateTimeTo
     * @return int
     */
    public function getMaxOrderNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(order_no)')]);
            $select->where->greaterThanOrEqualTo('order_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('order_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('order_no', intval(str_pad('9', UniqueNumberProvider::PAD_LENGTH, '9')));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * Update ONLY order_time_finish_real WITHOUT order_time_update.
     *
     * @param string $orderUuid
     * @return int
     */
    public function updateOrderTimeFinishReal(string $orderUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['order_time_finish_real' => new Expression('CURRENT_TIMESTAMP')]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * Update ONLY order_finish_unlock WITHOUT order_time_update.
     *
     * @param string $orderUuid
     * @param bool $unlock
     * @return int
     */
    public function updateOrderFinishUnlock(string $orderUuid, bool $unlock): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['order_finish_unlock' => $unlock]);
            $update->where(['order_uuid' => $orderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
