<?php

namespace Lerp\Order\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\ParamsOrderAnalysis;

class OrderAnalysisTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_analysis_order';
    const GROUP_MONTH = 1;
    const GROUP_YEAR = 2;
    const GROUP_COST_CENTRE = 3;
    public static array $possibleGroups = [self::GROUP_MONTH => 'month', self::GROUP_YEAR => 'year', self::GROUP_COST_CENTRE => 'cost_centre'];

    /**
     * @param string $orderUuid
     * @return array From view_analysis_order.
     */
    public function getOrderAnalysis(string $orderUuid): array
    {
        $select = new Select('view_analysis_order');
        try {
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param ParamsOrderAnalysis $paramsOrderAnalysis
     * @return array
     */
    public function searchOrderAnalysis(ParamsOrderAnalysis $paramsOrderAnalysis): array
    {
        $select = new Select('view_analysis_order');
        try {
            $paramsOrderAnalysis->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $paramsOrderAnalysis->isDoCount() ? $result->current()->getArrayCopy() : $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $paramsOrderAnalysis->isDoCount() ? ['count_order_analysis' => 0] : [];
    }

    /**
     * @param ParamsOrderAnalysis $paramsOrderAnalysis With 'groupBy'
     * @return array
     */
    public function getOrderAnalysisGrouped(ParamsOrderAnalysis $paramsOrderAnalysis): array
    {
        $select = new Select('view_analysis_order');
        try {
            $paramsOrderAnalysis->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                $cumul = 0;
                foreach ($arr as &$row) {
                    $cumul += $row['sum_order_item_price'];
                    $row['sum_order_item_price_cumul'] = $cumul;
                }
                return $arr;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

}
