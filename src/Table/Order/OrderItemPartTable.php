<?php

namespace Lerp\Order\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class OrderItemPartTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_part';

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemPartForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_uuid' => $orderItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
