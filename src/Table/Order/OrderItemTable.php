<?php

namespace Lerp\Order\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemEntity;

class OrderItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item';

    const VIEW_ORDER_ITEM = 1;
    const VIEW_ORDER_ITEM_FACTORYORDER = 2;

    private static array $views = [
        self::VIEW_ORDER_ITEM              => 'view_order_item',
        self::VIEW_ORDER_ITEM_FACTORYORDER => 'view_order_item_for_factoryorder',
    ];

    protected function makeJoinDefault(Select $select): void
    {
        $select->join('product', 'product.product_uuid = order_item.product_uuid',
            ['product_structure', 'product_text_part', 'product_text_short', 'product_text_long'], Select::JOIN_LEFT);
        $select->join('product_no', 'product_no.product_no_uuid = product.product_no_uuid',
            ['product_no_no'], Select::JOIN_LEFT);
    }

    /**
     * @param string $orderItemUuid
     * @param int $view One of the constants from self::$views. The views use several sub queries.
     * @return array
     */
    public function getOrderItem(string $orderItemUuid, int $view = 0): array
    {
        if ($view) {
            $select = new Select(self::$views[$view]);
        } else {
            $select = $this->sql->select();
            $this->makeJoinDefault($select);
        }
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderUuid
     * @param int $view One of the constants from self::$views.
     * @return array
     */
    public function getOrderItems(string $orderUuid, int $view): array
    {
        $select = new Select(self::$views[$view]);
        try {
            $select->where(['order_uuid' => $orderUuid]);
            $select->order('order_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @return array From db.view_order_item_for_factoryorder.
     */
    public function getOrderItemForFactoryorder(string $orderItemUuid): array
    {
        $select = new Select(self::$views[self::VIEW_ORDER_ITEM_FACTORYORDER]);
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOrderItemsForOrder(string $orderUuid): array
    {
        $select = new Select(self::$views[self::VIEW_ORDER_ITEM]);
        try {
            $select->where(['order_uuid' => $orderUuid]);
            $select->order('order_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOrderItemsWithUuids(array $orderItemUuids): array
    {
        $select = new Select(self::$views[self::VIEW_ORDER_ITEM]);
        try {
            $select->where->in('order_item_uuid', $orderItemUuids);
            $select->order('order_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOrderItemsForOrderPriority(string $orderUuid, int $orderPriority, bool $ifUp): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_uuid' => $orderUuid]);
            if ($ifUp) {
                $select->where->lessThanOrEqualTo('order_item_order_priority', $orderPriority);
                $select->order('order_item_order_priority DESC');
            } else {
                $select->where->greaterThanOrEqualTo('order_item_order_priority', $orderPriority);
                $select->order('order_item_order_priority ASC');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateOrderItemsOrderPriority(string $orderItemUuid, int $priority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'order_item_order_priority' => $priority
            ]);
            $update->where(['order_item_uuid' => $orderItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxOrderPriority(string $orderUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_priority' => new Expression('MAX(order_item_order_priority)')]);
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return intval($result->current()['max_priority']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getOrderItemIdNext(string $orderUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_order_item_id' => new Expression('MAX(order_item_id)')]);
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $maxId = intval($result->current()['max_order_item_id']);
                if ($maxId < 1) {
                    return 1;
                }
                return ++$maxId;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 2;
    }

    public function validOrderItemIdNext(string $orderUuid, int $orderItemId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_uuid' => $orderUuid, 'order_item_id' => $orderItemId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return true;
    }

    public function insertOrderItem(OrderItemEntity $orderItem): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'order_item_uuid'           => $uuid,
                'order_uuid'                => $orderItem->getOrderUuid(),
                'order_item_id'             => $orderItem->getOrderItemId(),
                'product_uuid'              => $orderItem->getProductUuid(),
                'order_item_text_short'     => $orderItem->getOrderItemTextShort(),
                'order_item_text_long'      => $orderItem->getOrderItemTextLong(),
                'order_item_quantity'       => $orderItem->getOrderItemQuantity(),
                'quantityunit_uuid'         => $orderItem->getQuantityUnitUuid(),
                'order_item_price'          => $orderItem->getOrderItemPrice(),
                'order_item_price_total'    => $orderItem->getOrderItemPriceTotal(),
                'cost_centre_id'            => $orderItem->getCostCentreId(),
                'order_item_order_priority' => $orderItem->getOrderItemOrderPriority(),
                'order_item_taxp'           => $orderItem->getOrderItemTaxp(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateOrderItem(string $orderItemUuid, array $storage): int
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['order_item_uuid' => $orderItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_uuid' => $orderItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
