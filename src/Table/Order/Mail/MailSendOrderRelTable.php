<?php

namespace Lerp\Order\Table\Order\Mail;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class MailSendOrderRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'mail_send_order_rel';

    /**
     * @param string $mailSendUuid
     * @param string $orderUuid
     * @return string
     */
    public function insertMailSendOrderRel(string $mailSendUuid, string $orderUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'mail_send_order_rel_uuid' => $uuid,
                'mail_send_uuid'           => $mailSendUuid,
                'order_uuid'               => $orderUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderUuid
     * @return array From view_mail_send_order
     */
    public function getMailsSendOrder(string $orderUuid): array
    {
        $select = new Select('view_mail_send_order');
        try {
            $select->where(['order_uuid' => $orderUuid]);
            $select->order('mail_send_time DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $rows = $result->toArray();
                foreach ($rows as &$row) {
                    $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                    $row['doc_estimate_json'] = json_decode($row['doc_estimate_json']);
                    $row['doc_order_confirm_json'] = json_decode($row['doc_order_confirm_json']);
                    $row['doc_proforma_json'] = json_decode($row['doc_proforma_json']);
                    $row['doc_delivery_json'] = json_decode($row['doc_delivery_json']);
                    $row['doc_invoice_json'] = json_decode($row['doc_invoice_json']);
                }
                return $rows;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $mailSendOrderRelUuid
     * @return array From view_mail_send_order
     */
    public function getMailSendOrder(string $mailSendOrderRelUuid): array
    {
        $select = new Select('view_mail_send_order');
        try {
            $select->where(['mail_send_order_rel_uuid' => $mailSendOrderRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $row = $result->toArray()[0];
                $row['mail_send_cc_arr'] = explode(',', $row['mail_send_cc_csv']);
                $row['mail_send_bcc_arr'] = explode(',', $row['mail_send_bcc_csv']);
                $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                $row['doc_estimate_json'] = json_decode($row['doc_estimate_json']);
                $row['doc_order_confirm_json'] = json_decode($row['doc_order_confirm_json']);
                $row['doc_proforma_json'] = json_decode($row['doc_proforma_json']);
                $row['doc_delivery_json'] = json_decode($row['doc_delivery_json']);
                $row['doc_invoice_json'] = json_decode($row['doc_invoice_json']);
                return $row;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
