<?php

namespace Lerp\Order\Table\Order\Service;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemServiceEntity;

class OrderItemServiceTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'order_item_service';

    /**
     * @param string $orderItemUuid
     * @return array
     */
    public function getOrderItemServiceWithOrderItemUuid(string $orderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns(['order_item_service_uuid', 'order_item_service_no_extern', 'order_item_service_text_short', 'order_item_service_text_long']);
            $select->join('view_order_item', 'view_order_item.order_item_uuid = order_item_service.order_item_uuid'
                , Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['order_item_service.order_item_uuid' => $orderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @return string
     */
    public function insertOrderItemService(string $orderItemUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'order_item_service_uuid' => $uuid,
                'order_item_uuid'         => $orderItemUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderItemUuid
     * @return int
     */
    public function deleteOrderItemServiceForOrderItem(string $orderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['order_item_uuid' => $orderItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param OrderItemServiceEntity $orderItemServiceEntity
     * @return int
     */
    public function updateOrderItemService(OrderItemServiceEntity $orderItemServiceEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'order_item_service_no_extern'  => $orderItemServiceEntity->getOrderItemServiceNoExtern(),
                'order_item_service_text_short' => $orderItemServiceEntity->getOrderItemServiceTextShort(),
                'order_item_service_text_long'  => $orderItemServiceEntity->getOrderItemServiceTextLong(),
            ]);
            $update->where(['order_item_service_uuid' => $orderItemServiceEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
