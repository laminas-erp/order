<?php

namespace Lerp\Order\Table\Order\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FileOrderRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_order_rel';

    /**
     * @param string $fileOrderRelUuid
     * @return array
     */
    public function getFileOrderRel(string $fileOrderRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_order_rel_uuid' => $fileOrderRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileOrderRel(string $fileOrderRelUuid): bool
    {
        return !empty($this->getFileOrderRel($fileOrderRelUuid));
    }

    /**
     * @param string $fileOrderRelUuid
     * @return array
     */
    public function getFileOrderRelJoined(string $fileOrderRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_order_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_order_rel_uuid' => $fileOrderRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFileOrderRelForFileAndOrder(string $fileUuid, string $orderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileOrderRelForFileAndOrder(string $fileUuid, string $orderUuid): bool
    {
        $file = $this->getFileOrderRelForFileAndOrder($fileUuid, $orderUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForOrder(string $orderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_order_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFileOrderRel(string $fileUuid, string $orderUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_order_rel_uuid' => $uuid,
                'file_uuid'           => $fileUuid,
                'order_uuid'          => $orderUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $fileOrderRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_order_rel_uuid' => $fileOrderRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $orderUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFileOrderRelForFileAndOrder($fileUuid, $orderUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}
