<?php

namespace Lerp\Order\Table\Offer\Mail;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class MailSendOfferRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'mail_send_offer_rel';

    /**
     * @param string $mailSendUuid
     * @param string $offerUuid
     * @return string
     */
    public function insertMailSendOfferRel(string $mailSendUuid, string $offerUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'mail_send_offer_rel_uuid' => $uuid,
                'mail_send_uuid'           => $mailSendUuid,
                'offer_uuid'               => $offerUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $offerUuid
     * @return array From view_mail_send_offer
     */
    public function getMailsSendOffer(string $offerUuid): array
    {
        $select = new Select('view_mail_send_offer');
        try {
            $select->where(['offer_uuid' => $offerUuid]);
            $select->order('mail_send_time DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $rows = $result->toArray();
                foreach ($rows as &$row) {
                    $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                    $row['doc_offer_json'] = json_decode($row['doc_offer_json']);
                }
                return $rows;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $mailSendOfferRelUuid
     * @return array From view_mail_send_offer
     */
    public function getMailSendOffer(string $mailSendOfferRelUuid): array
    {
        $select = new Select('view_mail_send_offer');
        try {
            $select->where(['mail_send_offer_rel_uuid' => $mailSendOfferRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $row = $result->toArray()[0];
                $row['mail_send_cc_arr'] = explode(',', $row['mail_send_cc_csv']);
                $row['mail_send_bcc_arr'] = explode(',', $row['mail_send_bcc_csv']);
                $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                $row['doc_offer_json'] = json_decode($row['doc_offer_json']);
                return $row;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
