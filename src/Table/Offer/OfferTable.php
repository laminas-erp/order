<?php

namespace Lerp\Order\Table\Offer;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Order\Entity\Offer\OfferEntity;
use Lerp\Order\Entity\Offer\ParamsOfferSearch;
use Lerp\Order\Unique\UniqueNumberProvider;

class OfferTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'offer';

    /**
     * @param string $offerUuid
     * @return array
     */
    public function getOffer(string $offerUuid): array
    {
        $select = new Select('view_offer');
        try {
            $select->where(['offer_uuid' => $offerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $customerUuid
     * @return array From view_offer.
     */
    public function getCustomerOffers(string $customerUuid): array
    {
        $select = new Select('view_offer');
        try {
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param OfferEntity $offerEntity
     * @return string
     */
    public function insertOffer(OfferEntity $offerEntity): string
    {
        $insert = $this->sql->insert();
        $offerEntity->setPrimaryKeyValue($this->uuid());
        try {
            $insert->values($offerEntity->getStorageInsert());
            if ($this->insertWith($insert) > 0) {
                return $offerEntity->getPrimaryKeyValue();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param ParamsOfferSearch $offerSearch
     * @param bool $doCount If true then this func returns the count of the results.
     * @return array Either a array with the count of the results ([0 => count]) or an array with the results.
     */
    public function searchOffer(ParamsOfferSearch $offerSearch, bool $doCount = false): array
    {
        $select = new Select('view_offer');
        try {
            if ($doCount) {
                $select->columns(['count_offers' => new Expression('COUNT(offer_uuid)')]);
            }
            if (!empty($offerNoCompl = $offerSearch->getOfferNoCompl())) {
                $select->where->like(new Expression('offer_no_compl'), "%$offerNoCompl%");
            }
            if (!empty($customerNo = $offerSearch->getCustomerNo())) {
                $select->where->like(new Expression('CAST(customer_no AS TEXT)'), "%$customerNo%");
            }
            if (!empty($customerName = $offerSearch->getCustomerName())) {
                $select->where->like('customer_name', "%$customerName%");
            }
            if (!empty($orderNoCompl = $offerSearch->getOrderNoCompl())) {
                $select->where->like(new Expression('order_no_compl'), "%$orderNoCompl%");
            }
            if (!empty($costCentreId = $offerSearch->getCostCentreId())) {
                $select->where(['cost_centre_id' => $costCentreId]);
            }
            if (!empty($locationPlaceUuid = $offerSearch->getLocationPlaceUuid())) {
                $select->where(['location_place_uuid' => $locationPlaceUuid]);
            }
            if (!empty($timeCreateFrom = $offerSearch->getTimeFrom())) {
                $select->where->greaterThanOrEqualTo('offer_time_create', $timeCreateFrom);
            }
            if (!empty($timeCreateTo = $offerSearch->getTimeTo())) {
                $select->where->lessThanOrEqualTo('offer_time_create', $timeCreateTo);
            }
            if (!$doCount && !empty($orderField = $offerSearch->getOrderField()) && !empty($orderDirec = $offerSearch->getOrderDirec())) {
                $select->order($orderField . ' ' . $orderDirec);
            }
            if (!$doCount && (!empty($offset = $offerSearch->getOffset()) || !empty($limit = $offerSearch->getLimit()))) {
                $select->limit($limit)->offset($offset);
            }

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$doCount) {
                    return $result->toArray();
                } else {
                    return [intval($result->current()['count_offers'])];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateOffer(OfferEntity $offerEntity): int
    {
        $update = $this->sql->update();
        if (empty($storage = $offerEntity->getStorageUpdate())) {
            return '';
        }
        $this->unsetNullFields($storage);
        $storage['offer_time_update'] = $this->getTimestamp();
        try {
            $update->set($storage);
            $update->where(['offer_uuid' => $offerEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $offerUuid
     * @return array Array keys: total_sum, total_sum_tax, total_sum_end
     */
    public function getOfferSummary(string $offerUuid): array
    {
        $params = new ParameterContainer([$offerUuid]);
        $stmt = $this->getAdapter()->getDriver()->createStatement('SELECT * FROM lerp_get_offer_summary(:offer_uuid)');
        /** @var Result $result */
        $result = $stmt->execute($params);
        if ($result->valid() && $result->count() == 1) {
            return $result->current();
        }
        return [];
    }

    /**
     * The maximum of fetched number is `UniqueNumberProvider::PAD_LENGTH` digits long (e.g. 4 => max: 9999).
     * @param \DateTime $dateTimeFrom
     * @param \DateTime $dateTimeTo
     * @return int
     */
    public function getMaxOfferNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(offer_no)')]);
            $select->where->greaterThanOrEqualTo('offer_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('offer_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('offer_no', intval(str_pad('9', UniqueNumberProvider::PAD_LENGTH, '9')));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
