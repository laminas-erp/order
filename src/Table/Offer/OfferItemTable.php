<?php

namespace Lerp\Order\Table\Offer;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Offer\OfferEntity;
use Lerp\Order\Entity\Offer\OfferItemEntity;
use Lerp\Product\Entity\ProductEntity;

class OfferItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'offer_item';

    protected function makeJoinDefault(Select $select): void
    {
        $select->join('product', 'product.product_uuid = offer_item.product_uuid',
            ['product_structure', 'product_text_part', 'product_text_short', 'product_text_long'], Select::JOIN_LEFT);
        $select->join('product_no', 'product_no.product_no_uuid = product.product_no_uuid',
            ['product_no_no'], Select::JOIN_LEFT);
    }

    /**
     * @param string $offerItemUuid
     * @return array
     */
    public function getOfferItem(string $offerItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $this->makeJoinDefault($select);
            $select->where(['offer_item_uuid' => $offerItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $offerUuid
     * @return array From db.view_offer_item:
     */
    public function getOfferItemsForOffer(string $offerUuid): array
    {
        $select = new Select('view_offer_item');
        try {
            $select->where(['offer_uuid' => $offerUuid]);
            $select->order('offer_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOfferItemsWithUuids(array $offerItemUuids): array
    {
        $select = new Select('view_offer_item');
        try {
            $select->where->in('offer_item_uuid', $offerItemUuids);
            $select->order('offer_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getOfferItemsForOrderPriority(string $offerUuid, int $orderPriority, bool $ifUp): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['offer_uuid' => $offerUuid]);
            if ($ifUp) {
                $select->where->lessThanOrEqualTo('offer_item_order_priority', $orderPriority);
                $select->order('offer_item_order_priority DESC');
            } else {
                $select->where->greaterThanOrEqualTo('offer_item_order_priority', $orderPriority);
                $select->order('offer_item_order_priority ASC');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateOfferItemsOrderPriority(string $offerItemUuid, int $priority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'offer_item_order_priority' => $priority
            ]);
            $update->where(['offer_item_uuid' => $offerItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxOrderPriority(string $offerUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_priority' => new Expression('MAX(offer_item_order_priority)')]);
            $select->where(['offer_uuid' => $offerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return intval($result->current()['max_priority']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getOfferItemIdNext(string $offerUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_offer_item_id' => new Expression('MAX(offer_item_id)')]);
            $select->where(['offer_uuid' => $offerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $maxId = intval($result->current()['max_offer_item_id']);
                if ($maxId < 1) {
                    return 2;
                }
                return ++$maxId;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 2;
    }

    public function validOfferItemIdNext(string $offerUuid, int $offerItemId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['offer_uuid' => $offerUuid, 'offer_item_id' => $offerItemId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return true;
    }

    public function insertOfferItem(OfferItemEntity $offerItemEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'offer_item_uuid'           => $uuid,
                'offer_item_id'             => $offerItemEntity->getOfferItemId(),
                'offer_uuid'                => $offerItemEntity->getOfferUuid(),
                'product_uuid'              => $offerItemEntity->getProductUuid(),
                'offer_item_text_short'     => $offerItemEntity->getOfferItemTextShort(),
                'offer_item_text_long'      => $offerItemEntity->getOfferItemTextLong(),
                'quantityunit_uuid'         => $offerItemEntity->getQuantityUnitUuid(),
                'offer_item_price'          => $offerItemEntity->getOfferItemPrice(),
                'offer_item_price_total'    => $offerItemEntity->getOfferItemPriceTotal(),
                'cost_centre_id'            => $offerItemEntity->getCostCentreId(),
                'offer_item_order_priority' => $offerItemEntity->getOfferItemOrderPriority(),
                'offer_item_taxp'           => $offerItemEntity->getOfferItemTaxp(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateOfferItem(string $offerItemUuid, array $storage): int
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['offer_item_uuid' => $offerItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteOfferItem(string $offerItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['offer_item_uuid' => $offerItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
