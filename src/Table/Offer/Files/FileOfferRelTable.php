<?php

namespace Lerp\Order\Table\Offer\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FileOfferRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_offer_rel';

    /**
     * @param string $fileOfferRelUuid
     * @return array
     */
    public function getFileOfferRel(string $fileOfferRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_offer_rel_uuid' => $fileOfferRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileOfferRel(string $fileOfferRelUuid): bool
    {
        return !empty($this->getFileOfferRel($fileOfferRelUuid));
    }

    /**
     * @param string $fileOfferRelUuid
     * @return array
     */
    public function getFileOfferRelJoined(string $fileOfferRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_offer_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_offer_rel_uuid' => $fileOfferRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFileOfferRelForFileAndOffer(string $fileUuid, string $offerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'offer_uuid' => $offerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileOfferRelForFileAndOffer(string $fileUuid, string $offerUuid): bool
    {
        $file = $this->getFileOfferRelForFileAndOffer($fileUuid, $offerUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForOffer(string $offerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_offer_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['offer_uuid' => $offerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFileOfferRel(string $fileUuid, string $offerUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_offer_rel_uuid' => $uuid,
                'file_uuid' => $fileUuid,
                'offer_uuid' => $offerUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $fileOfferRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_offer_rel_uuid' => $fileOfferRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $offerUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFileOfferRelForFileAndOffer($fileUuid, $offerUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}
