<?php

namespace Lerp\Order\Command;

use Laminas\Log\Logger;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\OrderMasterDataServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MasterDataCommand extends Command
{
    protected Logger $logger;
    protected OrderMasterDataServiceInterface $orderMasterDataService;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setOrderMasterDataService(OrderMasterDataServiceInterface $orderMasterDataService): void
    {
        $this->orderMasterDataService = $orderMasterDataService;
    }

    public function configure()
    {
        $this->addArgument('order_uuid', InputArgument::REQUIRED, 'Order UUID');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $orderUuid = $input->getArgument('order_uuid');
        if(!(new Uuid())->isValid($orderUuid)) {
            $output->writeln('Order UUID is not a valid UUID!');
            return -1;
        }
        if(!$this->orderMasterDataService->createOrderMasterDataSpreadsheet($orderUuid)) {
            $output->writeln('Some error occurred: ' . $this->orderMasterDataService->getMessage());
            return -1;
        }
        $output->writeln('success :)');
        $output->writeln('Spreadsheet path: ' . $this->orderMasterDataService->getFqfnFile());
        return 1;
    }
}
