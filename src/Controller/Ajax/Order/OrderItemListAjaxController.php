<?php

namespace Lerp\Order\Controller\Ajax\Order;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Order\OrderItemListService;

class OrderItemListAjaxController extends AbstractUserController
{
    protected OrderItemListService $orderItemListService;

    public function setOrderItemListService(OrderItemListService $orderItemListService): void
    {
        $this->orderItemListService = $orderItemListService;
    }

    /**
     * @return JsonModel A flat (none hierarchical) list of all orderItemLists for orderItem - without this orderItem.
     */
    public function orderItemsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemUuid = $this->params('order_item_uuid');
        if (!(new Uuid())->isValid($orderItemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->orderItemListService->getOrderItemListsForOrderItem($orderItemUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function orderItemListLevelForFactoryorderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemUuid = $this->params('order_item_uuid');
        $productUuid = $this->params('product_uuid');
        if (!($uuid = new Uuid())->isValid($orderItemUuid) || !$uuid->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->orderItemListService->getOrderItemListLevelForFactoryorder($orderItemUuid, $productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
