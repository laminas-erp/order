<?php

namespace Lerp\Order\Controller\Ajax\Order;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Order\OrderItemService;

class OrderItemAjaxController extends AbstractUserController
{
    protected OrderItemService $orderItemService;

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    /**
     * @return JsonModel
     */
    public function orderPriorityAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemUuid = $this->params('uuid');
        $orderPrioPrevious = filter_input(INPUT_POST, 'order_prio_previous', FILTER_SANITIZE_NUMBER_INT);
        $orderPrioCurrent = filter_input(INPUT_POST, 'order_prio_current', FILTER_SANITIZE_NUMBER_INT);
        if (!(new Uuid())->isValid($orderItemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderItemService->updateOrderItemListOrderPriority($orderItemUuid, $orderPrioPrevious, $orderPrioCurrent)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function itemListStockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $oiUuid = $this->params('order_item_uuid');
        $productUuidParent = $this->params('product_uuid_parent');
        $jsonModel->setArr($this->orderItemService->getOrderItemListStock($oiUuid, $productUuidParent));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function orderItemsStockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!(new Uuid())->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->orderItemService->getOrderItemsStock($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
