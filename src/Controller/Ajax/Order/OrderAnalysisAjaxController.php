<?php

namespace Lerp\Order\Controller\Ajax\Order;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Date;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\ParamsOrderAnalysis;
use Lerp\Order\Service\Order\OrderAnalysisService;

class OrderAnalysisAjaxController extends AbstractUserController
{
    protected Date $date;
    protected Uuid $uuid;
    protected OrderAnalysisService $orderAnalysisService;

    public function __construct()
    {
        $this->date = new Date();
        $this->uuid = new Uuid();
    }

    public function setOrderAnalysisService(OrderAnalysisService $orderAnalysisService): void
    {
        $this->orderAnalysisService = $orderAnalysisService;
    }

    /**
     * @return JsonModel
     */
    public function groupedAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $paramsAnalysis = new ParamsOrderAnalysis();
        $paramsAnalysis->setFromParamsArray($request->getPost()->toArray());
        $jsonModel->setArr($this->orderAnalysisService->getOrderAnalysisGrouped($paramsAnalysis));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
