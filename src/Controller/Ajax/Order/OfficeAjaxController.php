<?php

namespace Lerp\Order\Controller\Ajax\Order;

use Bitkorn\Trinket\Tools\Render\RenderTool;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\ParamsOrderAnalysis;
use Lerp\Order\Service\OrderAnalysisGroupedSpreadsheetServiceInterface;
use Lerp\Order\Service\OrderMasterDataServiceInterface;

class OfficeAjaxController extends AbstractUserController
{
    protected OrderMasterDataServiceInterface $orderMasterDataService;
    protected OrderAnalysisGroupedSpreadsheetServiceInterface $orderAnalysisGroupedSpreadsheetService;

    public function setOrderMasterDataService(OrderMasterDataServiceInterface $orderMasterDataService): void
    {
        $this->orderMasterDataService = $orderMasterDataService;
    }

    public function setOrderAnalysisGroupedSpreadsheetService(OrderAnalysisGroupedSpreadsheetServiceInterface $orderAnalysisGroupedSpreadsheetService): void
    {
        $this->orderAnalysisGroupedSpreadsheetService = $orderAnalysisGroupedSpreadsheetService;
    }

    /**
     * @return JsonModel
     */
    public function masterDataSpreadsheetAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (empty($orderUuid) || !(new Uuid())->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->orderMasterDataService->createOrderMasterDataSpreadsheet($orderUuid);
        if (!file_exists($this->orderMasterDataService->getFqfnFile())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        RenderTool::outputAttachment($this->orderMasterDataService->getFqfnFile()
            , 'Auftrag_' . $this->orderMasterDataService->getOrderNoNo() . '_Stammblatt.xlsx'
            , 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function groupedSpreadsheetAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $request = $this->getRequest();
        if (!$request->isGet() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $this->userService->setSessionHashManually($request->getQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $paramsAnalysis = new ParamsOrderAnalysis();
        $paramsAnalysis->setFromParamsArray($request->getQuery()->toArray());
        $this->orderAnalysisGroupedSpreadsheetService->createOrderAnalysisGroupedSpreadsheet($paramsAnalysis);
        if (!file_exists($this->orderAnalysisGroupedSpreadsheetService->getFqfnFile())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        RenderTool::outputAttachment($this->orderAnalysisGroupedSpreadsheetService->getFqfnFile()
            , date('Y-m-d_H-i-s') . '_AuftragAnalyseGruppiert.xlsx'
            , 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return $jsonModel;
    }
}
