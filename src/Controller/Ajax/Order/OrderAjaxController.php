<?php

namespace Lerp\Order\Controller\Ajax\Order;

use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Entity\User\Rightsnroles;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Order\Files\FileOrderRelService;
use Lerp\Order\Service\Order\OrderMailService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Unique\UniqueNumberProvider;

class OrderAjaxController extends AbstractUserController
{
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected OrderService $orderService;
    protected OrderMailService $orderMailService;
    protected FileOrderRelService $fileOrderRelService;
    protected UniqueNumberProvider $uniqueNumberProvider;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setOrderMailService(OrderMailService $orderMailService): void
    {
        $this->orderMailService = $orderMailService;
    }

    public function setFileOrderRelService(FileOrderRelService $fileOrderRelService): void
    {
        $this->fileOrderRelService = $fileOrderRelService;
    }

    public function setUniqueNumberProvider(UniqueNumberProvider $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    /**
     * @return JsonModel
     */
    public function orderSummaryAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('uuid');
        if (!$this->uuid->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->orderService->getOrderSummary($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function orderUserContactUpdateAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        $userUuid = $this->params('user_uuid');
        if (!$this->uuid->isValid($orderUuid) || !$this->uuid->isValid($userUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderService->updateOrderUserContact($orderUuid, $userUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET productFiles from products within the order (order_uuid).
     * @return JsonModel
     */
    public function getOrderProductsFilesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!$this->uuid->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOrderRelService->getProductFilesForOrder($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Get Order by order_no_compl (e.g. AUF22-90001).
     * @return JsonModel
     */
    public function getOrderByNumberCompleteAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderNoCompl = $this->params('order_no_compl');
        if (!$this->uniqueNumberProvider->validUniqueNumber($orderNoCompl)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->orderService->getOrderByNumberComplete($orderNoCompl));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Finish an order.
     *
     * @return JsonModel
     */
    public function finishAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!$this->uuid->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderService->updateOrderTimeFinishReal($orderUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * Send the email with the request to unlocking an order.
     *
     * @return JsonModel
     */
    public function requestUnlockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!$this->uuid->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderMailService->sendOrderUnlockRequest($orderUuid, $this->userService->getUserEntity())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * UPDATE order_finish_unlock.
     *
     * @return JsonModel
     */
    public function unlockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!$this->uuid->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderService->updateOrderFinishUnlock($orderUuid, $this->params('unlock') == 'true')) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
