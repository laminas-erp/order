<?php

namespace Lerp\Order\Controller\Ajax\Order;

use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Order\Files\FileOrderRelService;
use Lerp\Order\Service\Order\OrderMailService;

class OrderMailController extends AbstractUserController
{
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected OrderMailService $orderMailService;
    protected FileOrderRelService $fileOrderRelService;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setOrderMailService(OrderMailService $orderMailService): void
    {
        $this->orderMailService = $orderMailService;
    }

    public function setFileOrderRelService(FileOrderRelService $fileOrderRelService): void
    {
        $this->fileOrderRelService = $fileOrderRelService;
    }

    /**
     * @return JsonModel
     */
    public function sendDocumentsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $mailEntity = new MailEntity(new \HTMLPurifier());
        $mailEntity->handleHttpPost();
        if (!$this->uuid->isValid($orderUuid = filter_input(INPUT_POST, 'order_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $docDeliveryUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_delivery_uuids');
        $docEstimateUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_estimate_uuids');
        $docInvoiceUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_invoice_uuids');
        $docOrderConfirmUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_order_confirm_uuids');
        $fileUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'file_uuids');

        if ($this->orderMailService->sendDocuments($mailEntity, $orderUuid, $docDeliveryUuids, $docEstimateUuids, $docInvoiceUuids, $docOrderConfirmUuids
            , $fileUuids, $this->userService->getUserUuid())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getOrderMailsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!$this->uuid->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->orderMailService->getMailsSendOrder($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getOrderMailAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $mailSendOrderRelUuid = $this->params('mail_send_order_rel_uuid');
        if (!$this->uuid->isValid($mailSendOrderRelUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->orderMailService->getMailSendOrder($mailSendOrderRelUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
