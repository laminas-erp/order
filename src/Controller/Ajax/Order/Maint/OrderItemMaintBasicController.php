<?php

namespace Lerp\Order\Controller\Ajax\Order\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintFileEntity;
use Lerp\Order\Service\Maint\OrderItemMaintService;

class OrderItemMaintBasicController extends AbstractUserController
{
    protected OrderItemMaintService $orderItemMaintService;

    public function setOrderItemMaintService(OrderItemMaintService $orderItemMaintService): void
    {
        $this->orderItemMaintService = $orderItemMaintService;
    }

    /**
     * @return JsonModel
     */
    public function finishWorkflowAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemWorkflowUuid = $this->params('uuid');
        if (!(new Uuid())->isValid($orderItemWorkflowUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderItemMaintService->finishOrderItemMaintWorkflow($orderItemWorkflowUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    public function streamFileAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params('session'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $fileUuid = $this->params('uuid');
        $entity = new OrderItemMaintFileEntity();
        if (!(new Uuid())->isValid($fileUuid)
            || empty($fileData = $this->orderItemMaintService->getOrderItemMaintFile($fileUuid))
            || !$entity->exchangeArrayFromDatabase($fileData)
            || empty($folder = $this->orderItemMaintService->computeOrderItemMaintFileFolder($entity))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $filename = $entity->getOrderItemMaintFileFilenameComplete();
        $absPath = $folder . DIRECTORY_SEPARATOR . $filename;
        if (!file_exists($absPath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        header('Content-type: ' . $entity->getOrderItemMaintFileMimetype());
        header('Content-Disposition: inline; filename="' . $filename . '"'); // inline || attachment
        header('Content-length: ' . filesize($absPath));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        $resource = fopen('php://output', 'w');
        $raw = file_get_contents($absPath);
        fwrite($resource, $raw);

        exit();
    }
}
