<?php

namespace Lerp\Order\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Offer\OfferService;
use Lerp\Order\Service\Order\OrderService;

class CustomerOrderOfferAjaxController extends AbstractUserController
{
    protected Uuid $uuid;
    protected OrderService $orderService;
    protected OfferService $offerService;

    public function __construct()
    {
        $this->uuid = new Uuid();
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setOfferService(OfferService $offerService): void
    {
        $this->offerService = $offerService;
    }

    /**
     * @return JsonModel
     */
    public function customerOrdersAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $customerUuid = $this->params('customer_uuid');
        if (!$this->uuid->isValid($customerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->orderService->getCustomerOrders($customerUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function customerOffersAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $customerUuid = $this->params('customer_uuid');
        if (!$this->uuid->isValid($customerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->offerService->getCustomerOffers($customerUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
