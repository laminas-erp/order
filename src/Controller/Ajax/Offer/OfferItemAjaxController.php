<?php

namespace Lerp\Order\Controller\Ajax\Offer;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Order\Service\Offer\OfferItemService;

class OfferItemAjaxController extends AbstractUserController
{
    protected OfferItemService $offerItemService;

    public function setOfferItemService(OfferItemService $offerItemService): void
    {
        $this->offerItemService = $offerItemService;
    }

    /**
     * @return JsonModel
     */
    public function testAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
