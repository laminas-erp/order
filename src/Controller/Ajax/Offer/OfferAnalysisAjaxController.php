<?php

namespace Lerp\Order\Controller\Ajax\Offer;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Order\Service\Offer\OfferAnalysisService;

class OfferAnalysisAjaxController extends AbstractUserController
{
    protected OfferAnalysisService $offerAnalysisService;

    public function setOfferAnalysisService(OfferAnalysisService $offerAnalysisService): void
    {
        $this->offerAnalysisService = $offerAnalysisService;
    }

    /**
     * @return JsonModel
     */
    public function testAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
