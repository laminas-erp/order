<?php

namespace Lerp\Order\Controller\Ajax\Offer;

use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Offer\OfferMailService;

class OfferMailController extends AbstractUserController
{
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected OfferMailService $offerMailService;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setOfferMailService(OfferMailService $offerMailService): void
    {
        $this->offerMailService = $offerMailService;
    }

    /**
     * @return JsonModel
     */
    public function sendDocumentsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $mailEntity = new MailEntity(new \HTMLPurifier());
        $mailEntity->handleHttpPost();
        if(!$this->uuid->isValid($offerUuid = filter_input(INPUT_POST, 'offer_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $docOfferUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_offer_uuids');
        $fileUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'file_uuids');

        if ($this->offerMailService->sendDocuments($mailEntity, $offerUuid, $docOfferUuids, $fileUuids, $this->userService->getUserUuid())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getOfferMailsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $offerUuid = $this->params('offer_uuid');
        if (!$this->uuid->isValid($offerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->offerMailService->getMailsSendOffer($offerUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getOfferMailAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $mailSendOfferRelUuid = $this->params('mail_send_offer_rel_uuid');
        if (!$this->uuid->isValid($mailSendOfferRelUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->offerMailService->getMailSendOffer($mailSendOfferRelUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
