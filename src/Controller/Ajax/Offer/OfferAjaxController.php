<?php

namespace Lerp\Order\Controller\Ajax\Offer;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Offer\Files\FileOfferRelService;
use Lerp\Order\Service\Offer\OfferService;

class OfferAjaxController extends AbstractUserController
{
    protected OfferService $offerService;
    protected Uuid $uuid;
    protected FileOfferRelService $fileOfferRelService;

    public function __construct()
    {
        $this->uuid = new Uuid();
    }

    public function setOfferService(OfferService $offerService): void
    {
        $this->offerService = $offerService;
    }

    public function setFileOfferRelService(FileOfferRelService $fileOfferRelService): void
    {
        $this->fileOfferRelService = $fileOfferRelService;
    }

    /**
     * @return JsonModel
     */
    public function offerSummaryAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $offerUuid = $this->params('offer_uuid');
        if (!$this->uuid->isValid($offerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->offerService->getOfferSummary($offerUuid));
        $jsonModel->setSuccess(1);

        return $jsonModel;
    }

    /**
     * GET productFiles from products within the order (order_uuid).
     * @return JsonModel
     */
    public function getOfferProductsFilesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $offerUuid = $this->params('offer_uuid');
        if (!$this->uuid->isValid($offerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOfferRelService->getProductFilesForOffer($offerUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
