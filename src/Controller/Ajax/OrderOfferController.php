<?php

namespace Lerp\Order\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Order\Service\OrderOfferService;

class OrderOfferController extends AbstractUserController
{
    protected OrderOfferService $orderOfferService;

    public function setOrderOfferService(OrderOfferService $orderOfferService): void
    {
        $this->orderOfferService = $orderOfferService;
    }

	/**
	 * @return JsonModel
	 */
	public function offerToOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $offerUuid = $this->params('offer_uuid');
        if(!empty($orderUuid = $this->orderOfferService->createOrderFromOffer($offerUuid, $this->userService->getUserUuid()))) {
            $jsonModel->setUuid($orderUuid);
            $jsonModel->setSuccess(1);
        } else {
            $jsonModel->addMessage($this->orderOfferService->getMessage());
        }
		return $jsonModel;
	}
}
