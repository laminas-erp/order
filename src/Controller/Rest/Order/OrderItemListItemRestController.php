<?php

namespace Lerp\Order\Controller\Rest\Order;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\Order\OrderItemListEntity;
use Lerp\Order\Service\Order\OrderItemListService;

/**
 * Class OrderItemListItemRestController
 * @package Lerp\Order\Controller\Rest\Order
 * Handel items in an order_item_list.
 */
class OrderItemListItemRestController extends AbstractUserRestController
{
    protected OrderItemListService $orderItemListService;

    public function setOrderItemListService(OrderItemListService $orderItemListService): void
    {
        $this->orderItemListService = $orderItemListService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !($uv = new Uuid())->isValid($data['product_uuid_parent'])
            || !$uv->isValid($data['order_item_uuid'])
            || !$uv->isValid($data['product_uuid'])
            || !$this->orderItemListService->validInsertOrderItemList($data['order_item_uuid'], $data['product_uuid_parent'], $data['product_uuid'])
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $entity = new OrderItemListEntity();
        if (
            $entity->exchangeArrayFromDatabase([
                'order_item_uuid' => $data['order_item_uuid'],
                'product_uuid_parent' => $data['product_uuid_parent'],
                'product_uuid' => $data['product_uuid'],
                'order_item_list_quantity' => floatval($data['product_list_quantity']),
                'order_item_list_order_priority' => 0,
            ])
            && !empty($orderItemListUuid = $this->orderItemListService->insertOrderItemListWithEntity($entity))
        ) {
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('order_item_list_uuid', $orderItemListUuid);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id order_item_list_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderItemListService->deleteOrderItemListItem($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data 'field' = field name; 'value' = 'up' | 'down' (if field = 'order_priority')
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        switch ($data['field']) {
            case 'quantity':
                if ($this->orderItemListService->updateListItemQuantity($id, floatval($data['value']))) {
                    $jsonModel->setSuccess(1);
                }
                break;
            case 'order_priority':
                if ($this->orderItemListService->updateListItemOrderPriority($id, filter_var($data['value'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
                    $jsonModel->setSuccess(1);
                }
                break;
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
