<?php

namespace Lerp\Order\Controller\Rest\Order;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\I18n\Translator\Translator;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Order\Form\Order\OrderItemForm;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Table\Order\OrderItemTable;

class OrderItemRestController extends AbstractUserRestController
{
    protected OrderItemService $orderItemService;
    protected array $languagesSupported = [];
    protected OrderItemForm $orderItemForm;
    protected OrderDocumentService $orderDocumentService;
    protected Translator $translator;

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    public function setLanguagesSupported(array $languagesSupported): void
    {
        $this->languagesSupported = $languagesSupported;
    }

    public function setOrderItemForm(OrderItemForm $orderItemForm): void
    {
        $this->orderItemForm = $orderItemForm;
    }

    public function setOrderDocumentService(OrderDocumentService $orderDocumentService): void
    {
        $this->orderDocumentService = $orderDocumentService;
    }

    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * GET
     * @return JsonModel Order Items for an order - by order_uuid as query param.
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = (new FilterChainStringSanitize())->filter($this->getRequest()->getQuery('order_uuid', ''));
        if (!(new Uuid())->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($items = $this->orderItemService->getOrderItems($orderUuid, OrderItemTable::VIEW_ORDER_ITEM))) {
            $jsonModel->setArr($items);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (
            empty($data['order_uuid']) || empty($data['product_uuid']) || empty($data['lang'])
            || !(new Uuid())->isValid($data['order_uuid']) || !(new Uuid())->isValid($data['product_uuid'])
            || !in_array($data['lang'], $this->languagesSupported)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($uuid = $this->orderItemService->insertOrderItemSimple($data['order_uuid'], $data['product_uuid'], $data['lang']))) {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->orderItemForm->init();
        $this->orderItemForm->setData($data);
        if (!$this->orderItemForm->isValid()) {
            $jsonModel->addMessages($this->orderItemForm->getMessages());
            return $jsonModel;
        }
        if ($this->orderItemService->updateOrderItem($id, $this->orderItemForm->getData())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id order_item_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderDocumentService->existDocForOrderItem($id)) {
            $jsonModel->addMessage($this->translator->translate('can_not_delete_item_doc_exist', 'lerp_order'));
            return $jsonModel;
        }
        if ($this->orderItemService->deleteOrderItem($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id order_item_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($orderItem = $this->orderItemService->getOrderItem($id))) {
            $jsonModel->setObj($orderItem);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
