<?php

namespace Lerp\Order\Controller\Rest\Order\Service;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\Order\OrderItemServiceEntity;
use Lerp\Order\Form\Order\Service\OrderItemServiceForm;
use Lerp\Order\Service\Order\OrderItemServiceService;

class OrderItemServiceRestController extends AbstractUserRestController
{
    protected OrderItemServiceService $orderItemServiceService;
    protected OrderItemServiceForm $orderItemServiceForm;

    public function setOrderItemServiceService(OrderItemServiceService $orderItemServiceService): void
    {
        $this->orderItemServiceService = $orderItemServiceService;
    }

    public function setOrderItemServiceForm(OrderItemServiceForm $orderItemServiceForm): void
    {
        $this->orderItemServiceForm = $orderItemServiceForm;
    }

    /**
     * GET
     * @param string $id order_item_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->orderItemServiceService->getOrderItemServiceWithOrderItemUuid($id));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id order_item_service_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id) || empty($data)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->orderItemServiceForm->init();
        $this->orderItemServiceForm->setPrimaryKeyAvailable(true);
        $data['order_item_service_uuid'] = $id;
        $this->orderItemServiceForm->setData($data);
        if (!$this->orderItemServiceForm->isValid()) {
            $jsonModel->addMessages($this->orderItemServiceForm->getMessages());
            return $jsonModel;
        }
        $entity = new OrderItemServiceEntity();
        if (!$entity->exchangeArrayFromDatabase($this->orderItemServiceForm->getData())
            || !$this->orderItemServiceService->updateOrderItemService($entity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
