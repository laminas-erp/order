<?php

namespace Lerp\Order\Controller\Rest\Order\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Order\Form\Order\Files\FileOrderForm;
use Lerp\Order\Service\Order\Files\FileOrderRelService;

class FileOrderRestController extends AbstractUserRestController
{
    protected string $moduleBrand = '';
    protected FileOrderForm $fileOrderForm;
    protected FileOrderRelService $fileOrderRelService;

    public function setModuleBrand(string $moduleBrand): void
    {
        $this->moduleBrand = $moduleBrand;
    }

    public function setFileOrderForm(FileOrderForm $fileOrderForm): void
    {
        $this->fileOrderForm = $fileOrderForm;
    }

    public function setFileOrderRelService(FileOrderRelService $fileOrderRelService): void
    {
        $this->fileOrderRelService = $fileOrderRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $jsonModel;
        }
        $orderUuid = $data['order_uuid'];
        if (!(new Uuid())->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->fileOrderForm->getFileFieldset()->setFileInputAvailable(true);
        $this->fileOrderForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($data, $request->getFiles()->toArray());
            $this->fileOrderForm->setData($post);
            if ($this->fileOrderForm->isValid()) {
                $formData = $this->fileOrderForm->getData();
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                if (!empty($fileUuid = $this->fileOrderRelService->handleFileUpload($fileEntity, $orderUuid, $this->moduleBrand))) {
                    $jsonModel->setVariable('fileUuid', $fileUuid);
                    $jsonModel->setSuccess(1);
                }
            } else {
                $jsonModel->addMessages($this->fileOrderForm->getMessages());
            }
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id file_order_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->fileOrderRelService->deleteFile($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = filter_input(INPUT_GET, 'order_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($orderUuid) && !empty($files = $this->fileOrderRelService->getFilesForOrder($orderUuid))) {
            $jsonModel->setArr($files);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id order_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['order_uuid'] = $id; // currently unused
        $this->fileOrderForm->getFileFieldset()->setPrimaryKeyAvailable(true);
        $this->fileOrderForm->getFileFieldset()->setFileInputAvailable(false);
        $this->fileOrderForm->init();
        $this->fileOrderForm->setData($data);
        $fileEntity = new FileEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->fileOrderForm->isValid()) {
            $jsonModel->addMessages($this->fileOrderForm->getMessages());
            return $jsonModel;
        }
        if (!$fileEntity->exchangeArrayFromDatabase($this->fileOrderForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->fileOrderRelService->updateFile($fileEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($file = $this->fileOrderRelService->getFileOrderRelJoined($id))) {
            $jsonModel->setObj($file);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
