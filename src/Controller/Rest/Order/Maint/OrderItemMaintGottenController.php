<?php

namespace Lerp\Order\Controller\Rest\Order\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintGottenEntity;
use Lerp\Order\Form\Order\Maint\OrderItemMaintGottenForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Laminas\Validator\Uuid;

class OrderItemMaintGottenController extends AbstractUserRestController
{
    protected OrderItemMaintService $orderItemMaintService;
    protected OrderItemMaintGottenForm $orderItemMaintGottenForm;

    public function setOrderItemMaintService(OrderItemMaintService $orderItemMaintService): void
    {
        $this->orderItemMaintService = $orderItemMaintService;
    }

    public function setOrderItemMaintGottenForm(OrderItemMaintGottenForm $orderItemMaintGottenForm): void
    {
        $this->orderItemMaintGottenForm = $orderItemMaintGottenForm;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemMaintUuid = filter_input(INPUT_GET, 'order_item_maint_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $orderItemMaintFindUuid = filter_input(INPUT_GET, 'order_item_maint_find_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (
            (!empty($orderItemMaintUuid) && !(new Uuid())->isValid($orderItemMaintUuid))
            || (!empty($orderItemMaintFindUuid) && !(new Uuid())->isValid($orderItemMaintFindUuid))
            || (empty($orderItemMaintUuid) && empty($orderItemMaintFindUuid))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($gottens = $this->orderItemMaintService->getOrderItemMaintGottens($orderItemMaintUuid, $orderItemMaintFindUuid))) {
            $jsonModel->setArr($gottens);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->orderItemMaintGottenForm->init();
        $this->orderItemMaintGottenForm->setData($data);
        $entity = new OrderItemMaintGottenEntity();
        if (!$this->orderItemMaintGottenForm->isValid()) {
            $jsonModel->addMessages($this->orderItemMaintGottenForm->getMessages());
            return $jsonModel;
        }
        if (!$entity->exchangeArrayFromDatabase($this->orderItemMaintGottenForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->orderItemMaintService->insertOrderItemMaintGotten($entity))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
