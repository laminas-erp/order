<?php

namespace Lerp\Order\Controller\Rest\Order\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintFindEntity;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFindForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Laminas\Validator\Uuid;

class OrderItemMaintFindController extends AbstractUserRestController
{
    protected OrderItemMaintService $orderItemMaintService;
    protected OrderItemMaintFindForm $orderItemMaintFindForm;

    public function setOrderItemMaintService(OrderItemMaintService $orderItemMaintService): void
    {
        $this->orderItemMaintService = $orderItemMaintService;
    }

    public function setOrderItemMaintFindForm(OrderItemMaintFindForm $orderItemMaintFindForm): void
    {
        $this->orderItemMaintFindForm = $orderItemMaintFindForm;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemMaintUuid = filter_input(INPUT_GET, 'order_item_maint_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new Uuid())->isValid($orderItemMaintUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($finds = $this->orderItemMaintService->getOrderItemMaintFindsForOrderItemMaint($orderItemMaintUuid))) {
            $jsonModel->setArr($finds);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->orderItemMaintFindForm->init();
        $this->orderItemMaintFindForm->setData($data);
        $entity = new OrderItemMaintFindEntity();
        if (!$this->orderItemMaintFindForm->isValid()) {
            $jsonModel->addMessages($this->orderItemMaintFindForm->getMessages());
            return $jsonModel;
        }
        if (!$entity->exchangeArrayFromDatabase($this->orderItemMaintFindForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->orderItemMaintService->insertOrderItemMaintFind($entity))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
