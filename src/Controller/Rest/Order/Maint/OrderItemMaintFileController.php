<?php

namespace Lerp\Order\Controller\Rest\Order\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFileForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Laminas\Validator\Uuid;

class OrderItemMaintFileController extends AbstractUserRestController
{
    protected OrderItemMaintFileForm $orderItemMaintFileForm;
    protected OrderItemMaintService $orderItemMaintService;

    public function setOrderItemMaintFileForm(OrderItemMaintFileForm $orderItemMaintFileForm): void
    {
        $this->orderItemMaintFileForm = $orderItemMaintFileForm;
    }

    public function setOrderItemMaintService(OrderItemMaintService $orderItemMaintService): void
    {
        $this->orderItemMaintService = $orderItemMaintService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data = ArrayUtils::merge($data, $_FILES);
        $this->orderItemMaintFileForm->init();
        $this->orderItemMaintFileForm->setData($data);
        if (!$this->orderItemMaintFileForm->isValid()) {
            $jsonModel->addMessages($this->orderItemMaintFileForm->getMessages());
            return $jsonModel;
        }

        $formData = $this->orderItemMaintFileForm->getData();
        if (empty($uuid = $this->orderItemMaintService->insertOrderItemMaintFile($formData))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        } else {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }

        return $jsonModel;
    }

    /**
     * GET files for workflow | finding | gotten
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $type = filter_input(INPUT_GET, 'type', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuid = filter_input(INPUT_GET, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!in_array($type, OrderItemMaintService::$maintFileTypes) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($files = $this->orderItemMaintService->getOrderItemMaintFilesByType($type, $uuid))) {
            $jsonModel->setArr($files);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
