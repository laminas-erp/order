<?php

namespace Lerp\Order\Controller\Rest\Order\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintPartEntity;
use Lerp\Order\Form\Order\Maint\OrderItemMaintPartForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Laminas\Validator\Uuid;

class OrderItemMaintPartController extends AbstractUserRestController
{
    protected OrderItemMaintService $orderItemMaintService;
    protected OrderItemMaintPartForm $orderItemMaintPartForm;

    public function setOrderItemMaintService(OrderItemMaintService $orderItemMaintService): void
    {
        $this->orderItemMaintService = $orderItemMaintService;
    }

    public function setOrderItemMaintPartForm(OrderItemMaintPartForm $orderItemMaintPartForm): void
    {
        $this->orderItemMaintPartForm = $orderItemMaintPartForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->orderItemMaintPartForm->init();
        $this->orderItemMaintPartForm->setData($data);
        if (!$this->orderItemMaintPartForm->isValid()) {
            $jsonModel->addMessages($this->orderItemMaintPartForm->getMessages());
            return $jsonModel;
        }
        $entity = new OrderItemMaintPartEntity();
        if (
            !$entity->exchangeArrayFromDatabase($this->orderItemMaintPartForm->getData())
            || empty($uuid = $this->orderItemMaintService->insertOrderItemMaintPart($entity))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setUuid($uuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $maintUuid = filter_input(INPUT_GET, 'order_item_maint_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $maintGottenUuid = filter_input(INPUT_GET, 'order_item_maint_gotten_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (
            (!empty($maintUuid) && !(new Uuid())->isValid($maintUuid))
            || (!empty($maintGottenUuid) && !(new Uuid())->isValid($maintGottenUuid))
            || (empty($maintUuid) && empty($maintGottenUuid))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($parts = $this->orderItemMaintService->getOrderItemMaintParts($maintUuid, $maintGottenUuid))) {
            $jsonModel->setArr($parts);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
