<?php

namespace Lerp\Order\Controller\Rest\Order;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Service\Order\OrderItemListService;

class OrderItemListRestController extends AbstractUserRestController
{
    protected OrderItemListService $orderItemListService;

    public function setOrderItemListService(OrderItemListService $orderItemListService): void
    {
        $this->orderItemListService = $orderItemListService;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderItemUuid = (new FilterChainStringSanitize())->filter($this->getRequest()->getQuery('order_item_uuid', ''));
        $productUuid = (new FilterChainStringSanitize())->filter($this->getRequest()->getQuery('product_uuid', ''));
        if (!($u = new Uuid())->isValid($orderItemUuid) || !$u->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($items = $this->orderItemListService->getOrderItemListsForOrderItemHierarchic($orderItemUuid, $productUuid))) {
            $jsonModel->setArr($items);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
