<?php

namespace Lerp\Order\Controller\Rest\Order;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Entity\User\Rightsnroles;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Customer\Service\Contact\ContactCustomerRelService;
use Lerp\Order\Service\Order\OrderService;

class OrderContactRestController extends AbstractUserRestController
{
    protected OrderService $orderService;
    protected ContactCustomerRelService $contactCustomerRelService;

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setContactCustomerRelService(ContactCustomerRelService $contactCustomerRelService): void
    {
        $this->contactCustomerRelService = $contactCustomerRelService;
    }

    /**
     * DELETE maps to delete().
     * @param string $id order_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles(Rightsnroles::METHOD_DELETE)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderService->updateContactCustomerRelUuid($id, null)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id order_uuid
     * @param array $data ['contact_customer_rel_uuid' => '']
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles(Rightsnroles::METHOD_UPDATE)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = new Uuid();
        if (!$uuid->isValid($id) || !isset($data['contact_customer_rel_uuid']) || !$uuid->isValid($data['contact_customer_rel_uuid'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->orderService->updateContactCustomerRelUuid($id, $data['contact_customer_rel_uuid'])) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
