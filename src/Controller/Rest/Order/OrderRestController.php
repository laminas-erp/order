<?php

namespace Lerp\Order\Controller\Rest\Order;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Form\FormInterface;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Order\Form\Order\OrderForm;
use Lerp\Order\Service\Order\OrderService;

class OrderRestController extends AbstractUserRestController
{
    protected OrderForm $orderForm;
    protected OrderService $orderService;

    public function setOrderForm(OrderForm $orderForm): void
    {
        $this->orderForm = $orderForm;
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->orderForm->setData($data);
        if (!$this->orderForm->isValid()) {
            $jsonModel->addMessages($this->orderForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->orderForm->getData(FormInterface::VALUES_AS_ARRAY);
        $formData['user_uuid_create'] = $this->userService->getUserUuid();
        $formData['user_uuid_contact'] = $this->userService->getUserUuid();
        $formData['order_lang_iso'] = $this->userService->getUserEntity()->getUserLangIso();
        $entity = new OrderEntity();
        if (!$entity->exchangeArrayFromDatabase($formData) || empty($orderUuid = $this->orderService->insertOrder($entity))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setUuid($orderUuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET search with query params
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $dateFilter = new DateTimeFormatter();
        /** @var Request $request */
        $request = $this->getRequest();
        $filter = new FilterChainStringSanitize();
        $orderNoCompl = $filter->filter($request->getQuery('order_no_compl', ''));
        $label = $filter->filter($request->getQuery('order_label', ''));
        $customerNo = $filter->filter($request->getQuery('customer_no', ''));
        $offerNoCompl = $filter->filter($request->getQuery('offer_no_compl', ''));
        $costCentreId = (int)filter_input(INPUT_GET, 'cost_centre_id', FILTER_SANITIZE_NUMBER_INT);
        $locationPlaceUuid = (int)filter_input(INPUT_GET, 'location_place_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $timeCreateFrom = $dateFilter->filter($filter->filter($request->getQuery('order_time_create_from', '')));
        $timeCreateTo = $dateFilter->filter($filter->filter($request->getQuery('order_time_create_to', '')));
        $timeFinishScheduleFrom = $dateFilter->filter($filter->filter($request->getQuery('order_time_finish_schedule_from', '')));
        $timeFinishScheduleTo = $dateFilter->filter($filter->filter($request->getQuery('order_time_finish_schedule_to', '')));
        $timeFinishRealFrom = $dateFilter->filter($filter->filter($request->getQuery('order_time_finish_real_from', '')));
        $timeFinishRealTo = $dateFilter->filter($filter->filter($request->getQuery('order_time_finish_real_to', '')));
        $userUuidCreate = $filter->filter($request->getQuery('user_uuid_create', ''));
        $onlyOpen = filter_input(INPUT_GET, 'only_open', FILTER_VALIDATE_BOOL, ['options' => ['default' => false]]);
        $onlyFinish = filter_input(INPUT_GET, 'only_finish', FILTER_VALIDATE_BOOL, ['options' => ['default' => false]]);

        $orderField = $filter->filter($request->getQuery('order_field', ''));
        $orderDirec = $filter->filter($request->getQuery('order_direc', ''));
        $offset = (int)filter_input(INPUT_GET, 'offset', FILTER_SANITIZE_NUMBER_INT);
        $limit = (int)filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT);

        $jsonModel->setArr($this->orderService->searchOrder(
            $orderNoCompl, $label, $customerNo, $offerNoCompl, $costCentreId, $locationPlaceUuid
            , $timeCreateFrom, $timeCreateTo, $timeFinishScheduleFrom, $timeFinishScheduleTo, $timeFinishRealFrom, $timeFinishRealTo
            , $userUuidCreate, $onlyOpen, $onlyFinish, $orderField, $orderDirec, $offset, $limit));
        $jsonModel->setCount($this->orderService->getSearchOrderCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($order = $this->orderService->getOrder($id))) {
            $jsonModel->setObj($order);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->orderForm->setPrimaryKeyAvailable(true);
        $this->orderForm->setData($data);
        if (!$this->orderForm->isValid()) {
            $jsonModel->addMessages($this->orderForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->orderForm->getData(FormInterface::VALUES_AS_ARRAY);
        $formData['order_uuid'] = $id;
        $formData['user_uuid_update'] = $this->userService->getUserUuid();
        $entity = new OrderEntity();
        if (!$entity->exchangeArrayFromDatabase($formData) || !$this->orderService->updateOrder($entity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
