<?php

namespace Lerp\Order\Controller\Rest\Offer;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Form\FormInterface;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Entity\Offer\OfferEntity;
use Lerp\Order\Entity\Offer\ParamsOfferSearch;
use Lerp\Order\Form\Offer\OfferForm;
use Lerp\Order\Service\Offer\OfferService;

class OfferRestController extends AbstractUserRestController
{
    protected OfferService $offerService;
    protected OfferForm $offerForm;

    public function setOfferService(OfferService $offerService): void
    {
        $this->offerService = $offerService;
    }

    public function setOfferForm(OfferForm $offerForm): void
    {
        $this->offerForm = $offerForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->offerForm->setData($data);
        if (!$this->offerForm->isValid()) {
            $jsonModel->addMessages($this->offerForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->offerForm->getData(FormInterface::VALUES_AS_ARRAY);
        $formData['user_uuid_create'] = $this->userService->getUserUuid();
        $formData['offer_lang_iso'] = $this->userService->getUserEntity()->getUserLangIso();
        $entity = new OfferEntity();
        if (!$entity->exchangeArrayFromDatabase($formData) || empty($offerUuid = $this->offerService->insertOffer($entity))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setUuid($offerUuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $params = new ParamsOfferSearch();
        $params->setFromParamsArray($this->params()->fromQuery());
        $jsonModel->setArr($this->offerService->searchOffer($params));
        $jsonModel->setCount($this->offerService->getSearchOfferCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($order = $this->offerService->getOffer($id))) {
            $jsonModel->setObj($order);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->offerForm->setPrimaryKeyAvailable(true);
        $this->offerForm->setData($data);
        if (!$this->offerForm->isValid()) {
            $jsonModel->addMessages($this->offerForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->offerForm->getData(FormInterface::VALUES_AS_ARRAY);
        $formData['offer_uuid'] = $id;
        $formData['user_uuid_update'] = $this->userService->getUserUuid();
        $entity = new OfferEntity();
        if (!$entity->exchangeArrayFromDatabase($formData) || !$this->offerService->updateOffer($entity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
