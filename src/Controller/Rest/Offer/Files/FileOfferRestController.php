<?php

namespace Lerp\Order\Controller\Rest\Offer\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Order\Form\Offer\Files\FileOfferForm;
use Lerp\Order\Service\Offer\Files\FileOfferRelService;

class FileOfferRestController extends AbstractUserRestController
{
    protected string $moduleBrand = '';
    protected FileOfferForm $fileOfferForm;
    protected FileOfferRelService $fileOfferRelService;

    public function setModuleBrand(string $moduleBrand): void
    {
        $this->moduleBrand = $moduleBrand;
    }

    public function setFileOfferForm(FileOfferForm $fileOfferForm): void
    {
        $this->fileOfferForm = $fileOfferForm;
    }

    public function setFileOfferRelService(FileOfferRelService $fileOfferRelService): void
    {
        $this->fileOfferRelService = $fileOfferRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $jsonModel;
        }
        $offerUuid = $data['offer_uuid'];
        if (!(new Uuid())->isValid($offerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->fileOfferForm->getFileFieldset()->setFileInputAvailable(true);
        $this->fileOfferForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($data, $request->getFiles()->toArray());
            $this->fileOfferForm->setData($post);
            if ($this->fileOfferForm->isValid()) {
                $formData = $this->fileOfferForm->getData();
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                if (!empty($fileUuid = $this->fileOfferRelService->handleFileUpload($fileEntity, $offerUuid, $this->moduleBrand))) {
                    $jsonModel->setVariable('fileUuid', $fileUuid);
                    $jsonModel->setSuccess(1);
                }
            } else {
                $jsonModel->addMessages($this->fileOfferForm->getMessages());
            }
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id file_offer_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->fileOfferRelService->deleteFile($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $offerUuid = filter_input(INPUT_GET, 'offer_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($offerUuid) && !empty($files = $this->fileOfferRelService->getFilesForOffer($offerUuid))) {
            $jsonModel->setArr($files);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['offer_uuid'] = $id; // currently unused
        $this->fileOfferForm->getFileFieldset()->setPrimaryKeyAvailable(true);
        $this->fileOfferForm->getFileFieldset()->setFileInputAvailable(false);
        $this->fileOfferForm->init();
        $this->fileOfferForm->setData($data);
        $fileEntity = new FileEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->fileOfferForm->isValid()) {
            $jsonModel->addMessages($this->fileOfferForm->getMessages());
            return $jsonModel;
        }
        if (!$fileEntity->exchangeArrayFromDatabase($this->fileOfferForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->fileOfferRelService->updateFile($fileEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($file = $this->fileOfferRelService->getFileOfferRelJoined($id))) {
            $jsonModel->setObj($file);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
