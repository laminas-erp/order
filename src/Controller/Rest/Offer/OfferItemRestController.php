<?php

namespace Lerp\Order\Controller\Rest\Offer;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Order\Form\Offer\OfferItemForm;
use Lerp\Order\Service\Offer\OfferItemService;

class OfferItemRestController extends AbstractUserRestController
{
    protected OfferItemService $offerItemService;
    protected array $languagesSupported = [];
    protected OfferItemForm $offerItemForm;

    public function setOfferItemService(OfferItemService $offerItemService): void
    {
        $this->offerItemService = $offerItemService;
    }

    public function setLanguagesSupported(array $languagesSupported): void
    {
        $this->languagesSupported = $languagesSupported;
    }

    public function setOfferItemForm(OfferItemForm $offerItemForm): void
    {
        $this->offerItemForm = $offerItemForm;
    }

    /**
     * GET
     * @return JsonModel Offer Items for an offer - by offer_uuid as query param.
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($offerUuid = $this->getRequest()->getQuery('offer_uuid', ''))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($items = $this->offerItemService->getOfferItemsForOffer($offerUuid))) {
            $jsonModel->setArr($items);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = new Uuid();
        if (
            empty($data['offer_uuid']) || empty($data['product_uuid']) || empty($data['lang'])
            || !$uuid->isValid($data['offer_uuid']) || !$uuid->isValid($data['product_uuid'])
            || !in_array($data['lang'], $this->languagesSupported)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($uuid = $this->offerItemService->insertOfferItem($data['offer_uuid'], $data['product_uuid'], $data['lang']))) {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        /**
         * @todo aktivieren, wenn OfferDocumentService fertig
         */
        //if ($this->offerDocumentService->existDocForOfferItem($id)) {
        //    $jsonModel->addMessage($this->translator->translate('can_not_delete_item_doc_exist', 'lerp_order'));
        //    return $jsonModel;
        //}
        if ($this->offerItemService->deleteOfferItem($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->offerItemForm->init();
        $this->offerItemForm->setData($data);
        if (!$this->offerItemForm->isValid()) {
            $jsonModel->addMessages($this->offerItemForm->getMessages());
            return $jsonModel;
        }
        if ($this->offerItemService->updateOfferItem($id, $this->offerItemForm->getData())) {
            $jsonModel->setSuccess(1);
        }

        return $jsonModel;
    }
}
