<?php

namespace Lerp\Order\Form\Offer;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OfferForm extends AbstractForm implements InputFilterProviderInterface
{

    protected Adapter $adapter;
    protected array $costCentreIdAssoc = [];
    protected array $locationPlaceUuidAssoc = [];
    protected array $langIsos;

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setCostCentreIdAssoc(array $costCentreIdAssoc): void
    {
        $this->costCentreIdAssoc = $costCentreIdAssoc;
    }

    public function setLocationPlaceUuidAssoc(array $locationPlaceUuidAssoc): void
    {
        $this->locationPlaceUuidAssoc = $locationPlaceUuidAssoc;
    }

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'offer_uuid']);
        }
        $this->add(['name' => 'customer_uuid']);
        $this->add(['name' => 'cost_centre_id']);
        $this->add(['name' => 'location_place_uuid']);
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'offer_lang_iso']);
        }
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];
        if ($this->primaryKeyAvailable) {
            $filter['offer_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }
        $filter['customer_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Uuid::class
                ],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'customer',
                        'field' => 'customer_uuid'
                    ]
                ]
            ]
        ];
        $filter['cost_centre_id'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->costCentreIdAssoc)
                    ]
                ]
            ]
        ];
        $filter['location_place_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->locationPlaceUuidAssoc)
                    ]
                ]
            ]
        ];
        if ($this->primaryKeyAvailable) {
            $filter['offer_lang_iso'] = [
                'required' => true,
                'filters' => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => $this->langIsos,
                        ]
                    ]
                ]
            ];
        }
        return $filter;
    }
}
