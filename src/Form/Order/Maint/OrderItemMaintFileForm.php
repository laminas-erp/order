<?php

namespace Lerp\Order\Form\Order\Maint;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\File\MimeType;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OrderItemMaintFileForm extends AbstractForm implements InputFilterProviderInterface
{

    /**
     * @var string
     */
    protected $acceptedMimeTypes = 'image/jpeg,image/png,application/pdf,text/plain,text/csv';

    public static $extMimeMap = [
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'png' => 'image/png',
        'pdf' => 'application/pdf',
        'txt' => 'text/plain',
        'csv' => 'text/csv',
    ];

    /**
     * @var bool
     */
    protected $fileUploadAvailable = true;

    /**
     * @param bool $fileUploadAvailable
     */
    public function setFileUploadAvailable(bool $fileUploadAvailable): void
    {
        $this->fileUploadAvailable = $fileUploadAvailable;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_item_maint_file_uuid']);
        }
        $this->add(['name' => 'order_item_maint_uuid']);

        $this->add(['name' => 'order_item_maint_find_uuid']);
        $this->add(['name' => 'order_item_maint_gotten_uuid']);
        $this->add(['name' => 'order_item_maint_workflow_uuid']);

        $this->add(['name' => 'order_item_maint_file_label']);
        $this->add(['name' => 'order_item_maint_file_desc']);

        if ($this->fileUploadAvailable) {
            $this->add(['name' => 'file_upload']);
        }
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['order_item_maint_file_uuid'] = [
                'required' => true,
                'validators' => [['name' => Uuid::class,]]
            ];
        }

        $filter['order_item_maint_uuid'] = [
            'required' => true,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_find_uuid'] = [
            'required' => false,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_gotten_uuid'] = [
            'required' => false,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_workflow_uuid'] = [
            'required' => false,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_file_label'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['order_item_maint_file_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        if ($this->fileUploadAvailable) {
            $filter['file_upload'] = [
                'required' => true,
                'validators' => [
                    [
                        'name' => MimeType::class,
                        'break_chain_on_failure' => true,
                        'options' => [
                            'mimeType' => implode(', ', array_unique(self::$extMimeMap)),
                            'magicFile' => false, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'enableHeaderCheck' => true,
                            'messages' => [
                                MimeType::FALSE_TYPE => "Wrong filetype '%type%', only: " . implode(', ', array_unique(self::$extMimeMap)),
                            ],
                        ]
                    ],
                    [
                        'name' => 'filesize',
                        'options' => [
                            'max' => 10000000,
                        ],
                    ],
                ],
            ];
        }

        return $filter;
    }
}
