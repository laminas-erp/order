<?php

namespace Lerp\Order\Form\Order\Maint;

use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OrderItemMaintPartForm extends AbstractForm implements InputFilterProviderInterface
{
    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_item_maint_part_uuid']);
        }
        $this->add(['name' => 'order_item_maint_uuid']);
        $this->add(['name' => 'order_item_maint_gotten_uuid']);
        $this->add(['name' => 'product_uuid']);
        $this->add(['name' => 'order_item_maint_part_quantity']);
        $this->add(['name' => 'quantityunit_uuid']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['order_item_maint_part_uuid'] = [
                'required' => true,
                'validators' => [['name' => Uuid::class,]]
            ];
        }

        $filter['order_item_maint_uuid'] = [
            'required' => true,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_gotten_uuid'] = [
            'required' => false,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['product_uuid'] = [
            'required' => true,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_part_quantity'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [['name' => FloatValidator::class]]
        ];

        return $filter;
    }
}
