<?php

namespace Lerp\Order\Form\Order\Maint;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OrderItemMaintGottenForm extends AbstractForm implements InputFilterProviderInterface
{
    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_item_maint_gotten_uuid']);
        }
        $this->add(['name' => 'order_item_maint_uuid']);
        $this->add(['name' => 'order_item_maint_find_uuid']);
        $this->add(['name' => 'order_item_maint_gotten_label']);
        $this->add(['name' => 'order_item_maint_gotten_desc']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['order_item_maint_gotten_uuid'] = [
                'required' => true,
                'validators' => [['name' => Uuid::class,]]
            ];
        }

        $filter['order_item_maint_uuid'] = [
            'required' => true,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_find_uuid'] = [
            'required' => false,
            'validators' => [['name' => Uuid::class,]]
        ];

        $filter['order_item_maint_gotten_label'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['order_item_maint_gotten_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
