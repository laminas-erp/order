<?php

namespace Lerp\Order\Form\Order;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OrderItemForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $costCentreIdAssoc = [];

    public function setCostCentreIdAssoc(array $costCentreIdAssoc): void
    {
        $this->costCentreIdAssoc = $costCentreIdAssoc;
    }

    public function init()
    {
        if($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_item_uuid']);
        }
        $this->add(['name' => 'order_item_text_short']);
        $this->add(['name' => 'order_item_text_long']);
        $this->add(['name' => 'order_item_price']);
        $this->add(['name' => 'order_item_quantity']);
        $this->add(['name' => 'quantityunit_uuid']);
        $this->add(['name' => 'cost_centre_id']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if($this->primaryKeyAvailable) {
            $filter['order_item_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name' => Uuid::class,
                    ]
                ]
            ];
        }

        $filter['order_item_text_short'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['order_item_text_long'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['order_item_price'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        $filter['order_item_quantity'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        $filter['quantityunit_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name' => Uuid::class,
                ]
            ]
        ];

        $filter['cost_centre_id'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->costCentreIdAssoc)
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
