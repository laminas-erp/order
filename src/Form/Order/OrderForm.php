<?php

namespace Lerp\Order\Form\Order;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OrderForm extends AbstractForm implements InputFilterProviderInterface
{

    protected Adapter $adapter;
    protected array $costCentreIdAssoc = [];
    protected array $locationPlaceUuidAssoc = [];
    protected array $langIsos;

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setCostCentreIdAssoc(array $costCentreIdAssoc): void
    {
        $this->costCentreIdAssoc = $costCentreIdAssoc;
    }

    public function setLocationPlaceUuidAssoc(array $locationPlaceUuidAssoc): void
    {
        $this->locationPlaceUuidAssoc = $locationPlaceUuidAssoc;
    }

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_uuid']);
        }
        $this->add(['name' => 'order_label']);
        $this->add(['name' => 'customer_uuid']);
        $this->add(['name' => 'offer_uuid']);
        $this->add(['name' => 'cost_centre_id']);
        $this->add(['name' => 'location_place_uuid']);
        $this->add(['name' => 'project_type_id']);
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_label_yours']);
            $this->add(['name' => 'order_time_finish_schedule']);
            $this->add(['name' => 'order_time_request']);
            $this->add(['name' => 'order_lang_iso']);
            $this->add(['name' => 'order_comment']);
        }
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['order_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['order_label'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 80,
                    ]
                ]
            ]
        ];
        $filter['customer_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Uuid::class
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table'   => 'customer',
                        'field'   => 'customer_uuid'
                    ]
                ]
            ]
        ];
        $filter['offer_uuid'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Uuid::class
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table'   => 'offer',
                        'field'   => 'offer_uuid'
                    ]
                ]
            ]
        ];

        $filter['cost_centre_id'] = [
            'required'   => true,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->costCentreIdAssoc)
                    ]
                ]
            ]
        ];

        $filter['location_place_uuid'] = [
            'required'   => true,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name'    => Uuid::class,
                    'options' => [
                        'haystack' => array_keys($this->locationPlaceUuidAssoc)
                    ]
                ]
            ]
        ];
        $filter['project_type_id'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table'   => 'project_type',
                        'field'   => 'project_type_id'
                    ]
                ]
            ]
        ];

        if ($this->primaryKeyAvailable) {
            $filter['order_label_yours'] = [
                'required'      => false,
                'filters'       => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name'    => StringLength::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'max'      => 80,
                        ]
                    ]
                ]
            ];
            $filter['order_time_finish_schedule'] = [
                'required'   => false,
                'filters'    => [
                    ['name' => DateTimeFormatter::class]
                ],
                'validators' => [
                ]
            ];
            $filter['order_time_request'] = [
                'required'   => false,
                'filters'    => [
                    ['name' => DateTimeFormatter::class]
                ],
                'validators' => [
                ]
            ];
            $filter['order_lang_iso'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    [
                        'name'    => InArray::class,
                        'options' => [
                            'haystack' => $this->langIsos,
                        ]
                    ]
                ]
            ];

            $filter['order_comment'] = [
                'required'      => false,
                'filters'       => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name'    => StringLength::class,
                        'options' => [
                            'max' => 4000,
                        ]
                    ]
                ]
            ];
        }

        return $filter;
    }
}
