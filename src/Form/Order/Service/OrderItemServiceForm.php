<?php

namespace Lerp\Order\Form\Order\Service;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class OrderItemServiceForm extends AbstractForm implements InputFilterProviderInterface
{
    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'order_item_service_uuid']);
        }
        $this->add(['name' => 'order_item_service_no_extern']);
        $this->add(['name' => 'order_item_service_text_short']);
        $this->add(['name' => 'order_item_service_text_long']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['order_item_service_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }
        $filter['order_item_service_no_extern'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 80,
                    ]
                ]
            ]
        ];
        $filter['order_item_service_text_short'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 190,
                    ]
                ]
            ]
        ];
        $filter['order_item_service_text_long'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 40000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
