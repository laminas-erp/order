<?php

namespace Lerp\Order\Factory\Service\Offer;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Service\MailService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\OfferDocumentService;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Order\Service\Offer\OfferMailService;
use Lerp\Order\Table\Offer\Mail\MailSendOfferRelTable;

class OfferMailServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OfferMailService();
        $service->setLogger($container->get('logger'));
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setMailService($container->get(MailService::class));
        $service->setFileService($container->get(FileService::class));
        $service->setOfferDocumentService($container->get(OfferDocumentService::class));
        $service->setDocOfferSendTable($container->get(DocOfferSendTable::class));
        $service->setMailSendOfferRelTable($container->get(MailSendOfferRelTable::class));
        return $service;
    }
}
