<?php

namespace Lerp\Order\Factory\Service\Offer;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Offer\OfferItemService;
use Lerp\Order\Service\Offer\OfferService;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Product\Service\Calc\ProductCalcService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductTextService;

class OfferItemServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OfferItemService();
        $service->setLogger($container->get('logger'));
        $service->setOfferItemTable($container->get(OfferItemTable::class));
        $service->setOfferService($container->get(OfferService::class));
        $service->setProductService($container->get(ProductService::class));
        $service->setProductTextService($container->get(ProductTextService::class));
        $service->setProductCalcService($container->get(ProductCalcService::class));
        return $service;
    }
}
