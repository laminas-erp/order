<?php

namespace Lerp\Order\Factory\Service\Offer\Files;

use Bitkorn\Files\Service\FileService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Offer\Files\FileOfferRelService;
use Lerp\Order\Table\Offer\Files\FileOfferRelTable;
use Lerp\Product\Table\Files\FileProductRelTable;

class FileOfferRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileOfferRelService();
        $service->setLogger($container->get('logger'));
        $service->setFileOfferRelTable($container->get(FileOfferRelTable::class));
        $service->setFileProductRelTable($container->get(FileProductRelTable::class));
        $service->setFileService($container->get(FileService::class));
        return $service;
    }
}
