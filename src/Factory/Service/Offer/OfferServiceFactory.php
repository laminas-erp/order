<?php

namespace Lerp\Order\Factory\Service\Offer;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Offer\OfferService;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Order\Unique\UniqueNumberProviderInterface;

class OfferServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OfferService();
        $service->setLogger($container->get('logger'));
        $service->setOfferTable($container->get(OfferTable::class));
        $service->setOfferItemTable($container->get(OfferItemTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        return $service;
    }
}
