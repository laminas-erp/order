<?php

namespace Lerp\Order\Factory\Service\Maint;

use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFileTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFindTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintGottenTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintPartTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintToolTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintWorkflowTable;
use Lerp\Product\Service\ProductService;

class OrderItemMaintServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderItemMaintService();
        $service->setLogger($container->get('logger'));
        $service->setOrderItemMaintTable($container->get(OrderItemMaintTable::class));
        $service->setOrderItemMaintFileTable($container->get(OrderItemMaintFileTable::class));
        $service->setOrderItemMaintFindTable($container->get(OrderItemMaintFindTable::class));
        $service->setOrderItemMaintGottenTable($container->get(OrderItemMaintGottenTable::class));
        $service->setOrderItemMaintPartTable($container->get(OrderItemMaintPartTable::class));
        $service->setOrderItemMaintToolTable($container->get(OrderItemMaintToolTable::class));
        $service->setOrderItemMaintWorkflowTable($container->get(OrderItemMaintWorkflowTable::class));
        $service->setFolderTool($container->get(FolderTool::class));
        $service->setPathMaint($container->get('config')['lerp_order']['path_maint']);
        $service->setProductService($container->get(ProductService::class));
        return $service;
    }
}
