<?php

namespace Lerp\Order\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Offer\OfferItemService;
use Lerp\Order\Service\Offer\OfferService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Service\OrderOfferService;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Product\Service\ProductService;

class OrderOfferServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderOfferService();
        $service->setLogger($container->get('logger'));
        $service->setOfferTable($container->get(OfferTable::class));
        $service->setOfferService($container->get(OfferService::class));
        $service->setOfferItemService($container->get(OfferItemService::class));
        $service->setOrderService($container->get(OrderService::class));
        $service->setProductService($container->get(ProductService::class));
        $service->setOrderItemService($container->get(OrderItemService::class));
        return $service;
    }
}
