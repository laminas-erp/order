<?php

namespace Lerp\Order\Factory\Service\Order;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Service\MailService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\Helper\BasePath;
use Laminas\View\Helper\Url;
use Laminas\View\HelperPluginManager;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Document\Table\Order\DocDeliverySendTable;
use Lerp\Document\Table\Order\DocEstimateSendTable;
use Lerp\Document\Table\Order\DocInvoiceSendTable;
use Lerp\Document\Table\Order\DocOrderConfirmSendTable;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Order\Service\Order\OrderMailService;
use Lerp\Order\Table\Order\Mail\MailSendOrderRelTable;
use Lerp\Order\Table\Order\OrderTable;

class OrderMailServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderMailService();
        $service->setLogger($container->get('logger'));
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setMailService($container->get(MailService::class));
        $service->setFileService($container->get(FileService::class));
        $service->setOrderDocumentService($container->get(OrderDocumentService::class));
        $service->setDocDeliverySendTable($container->get(DocDeliverySendTable::class));
        $service->setDocEstimateSendTable($container->get(DocEstimateSendTable::class));
        $service->setDocInvoiceSendTable($container->get(DocInvoiceSendTable::class));
        $service->setDocOrderConfirmSendTable($container->get(DocOrderConfirmSendTable::class));
        $service->setMailSendOrderRelTable($container->get(MailSendOrderRelTable::class));
        $service->setOrderTable($container->get(OrderTable::class));
        $service->setUserEquipService($container->get(UserEquipService::class));
        $config = $container->get('config');
        $service->setAppUrl($config['lerp_application']['app_url']);
        $service->setAppProtocol($config['lerp_application']['app_protocol']);
        return $service;
    }
}
