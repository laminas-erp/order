<?php

namespace Lerp\Order\Factory\Service\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Order\OrderAnalysisService;
use Lerp\Order\Table\Order\OrderAnalysisTable;

class OrderAnalysisServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderAnalysisService();
        $service->setLogger($container->get('logger'));
        $service->setOrderAnalysisTable($container->get(OrderAnalysisTable::class));
        return $service;
    }
}
