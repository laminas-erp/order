<?php

namespace Lerp\Order\Factory\Service\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Order\OrderItemListService;
use Lerp\Order\Table\Order\OrderItemListTable;
use Lerp\Product\Table\ProductTable;

class OrderItemListServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderItemListService();
        $service->setLogger($container->get('logger'));
        $service->setOrderItemListTable($container->get(OrderItemListTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        return $service;
    }
}
