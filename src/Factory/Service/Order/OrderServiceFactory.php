<?php

namespace Lerp\Order\Factory\Service\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Table\Order\OrderItemPartTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\OrderTable;
use Lerp\Order\Unique\UniqueNumberProviderInterface;

class OrderServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderService();
        $service->setLogger($container->get('logger'));
        $service->setOrderTable($container->get(OrderTable::class));
        $service->setOrderItemTable($container->get(OrderItemTable::class));
        $service->setOrderItemPartTable($container->get(OrderItemPartTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        return $service;
    }
}
