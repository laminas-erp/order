<?php

namespace Lerp\Order\Factory\Service\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Order\OrderItemServiceService;
use Lerp\Order\Table\Order\Service\OrderItemServiceTable;

class OrderItemServiceServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderItemServiceService();
        $service->setLogger($container->get('logger'));
        $service->setOrderItemServiceTable($container->get(OrderItemServiceTable::class));
        return $service;
    }
}
