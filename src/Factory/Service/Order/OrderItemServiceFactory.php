<?php

namespace Lerp\Order\Factory\Service\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Lerp\Order\Service\Order\OrderItemListService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Table\Order\OrderItemPartTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\Service\OrderItemServiceTable;
use Lerp\Order\Table\Order\ViewOrderItemListStockTable;
use Lerp\Order\Table\Order\ViewOrderItemStockTable;
use Lerp\Product\Service\Calc\ProductCalcService;
use Lerp\Product\Service\Maint\ProductMaintService;
use Lerp\Product\Service\ProductListService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductTextService;

class OrderItemServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderItemService();
        $service->setLogger($container->get('logger'));
        $service->setOrderItemTable($container->get(OrderItemTable::class));
        $service->setOrderItemPartTable($container->get(OrderItemPartTable::class));
        $service->setOrderItemServiceTable($container->get(OrderItemServiceTable::class));
        $service->setViewOrderItemListStockTable($container->get(ViewOrderItemListStockTable::class));
        $service->setViewOrderItemStockTable($container->get(ViewOrderItemStockTable::class));
        $service->setProductService($container->get(ProductService::class));
        $service->setProductTextService($container->get(ProductTextService::class));
        $service->setProductCalcService($container->get(ProductCalcService::class));
        $service->setProductListService($container->get(ProductListService::class));
        $service->setOrderService($container->get(OrderService::class));
        $service->setOrderItemListService($container->get(OrderItemListService::class));
        $service->setOrderItemMaintService($container->get(OrderItemMaintService::class));
        $service->setProductMaintService($container->get(ProductMaintService::class));
        return $service;
    }
}
