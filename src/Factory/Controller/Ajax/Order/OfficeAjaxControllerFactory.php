<?php

namespace Lerp\Order\Factory\Controller\Ajax\Order;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Ajax\Order\OfficeAjaxController;
use Lerp\Order\Service\Order\OrderAnalysisService;
use Lerp\Order\Service\OrderAnalysisGroupedSpreadsheetServiceInterface;
use Lerp\Order\Service\OrderMasterDataServiceInterface;

class OfficeAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OfficeAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOrderMasterDataService($container->get(OrderMasterDataServiceInterface::class));
        $controller->setOrderAnalysisGroupedSpreadsheetService($container->get(OrderAnalysisGroupedSpreadsheetServiceInterface::class));
        return $controller;
    }
}
