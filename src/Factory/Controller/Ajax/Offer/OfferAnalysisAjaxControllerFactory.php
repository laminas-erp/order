<?php

namespace Lerp\Order\Factory\Controller\Ajax\Offer;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Ajax\Offer\OfferAnalysisAjaxController;
use Lerp\Order\Service\Offer\OfferAnalysisService;

class OfferAnalysisAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OfferAnalysisAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOfferAnalysisService($container->get(OfferAnalysisService::class));
        return $controller;
    }
}
