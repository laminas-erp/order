<?php

namespace Lerp\Order\Factory\Controller\Rest\Offer;

use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Rest\Offer\OfferItemRestController;
use Lerp\Order\Form\Offer\OfferItemForm;
use Lerp\Order\Service\Offer\OfferItemService;

class OfferItemRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OfferItemRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOfferItemService($container->get(OfferItemService::class));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $controller->setLanguagesSupported($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        $controller->setOfferItemForm($container->get(OfferItemForm::class));
        return $controller;
    }
}
