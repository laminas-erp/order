<?php

namespace Lerp\Order\Factory\Controller\Rest\Order\Service;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Rest\Order\Service\OrderItemServiceRestController;
use Lerp\Order\Form\Order\Service\OrderItemServiceForm;
use Lerp\Order\Service\Order\OrderItemServiceService;

class OrderItemServiceRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OrderItemServiceRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOrderItemServiceService($container->get(OrderItemServiceService::class));
        $controller->setOrderItemServiceForm($container->get(OrderItemServiceForm::class));
        return $controller;
    }
}
