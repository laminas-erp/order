<?php

namespace Lerp\Order\Factory\Controller\Rest\Order\Files;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Rest\Order\Files\FileOrderRestController;
use Lerp\Order\Form\Order\Files\FileOrderForm;
use Lerp\Order\Service\Order\Files\FileOrderRelService;

class FileOrderRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FileOrderRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFileOrderForm($container->get(FileOrderForm::class));
        $controller->setFileOrderRelService($container->get(FileOrderRelService::class));
        $controller->setModuleBrand($container->get('config')['lerp_order']['module_brand']);
        return $controller;
    }
}
