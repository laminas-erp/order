<?php

namespace Lerp\Order\Factory\Controller\Rest\Order\Maint;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintFileController;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFileForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;

class OrderItemMaintFileControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new OrderItemMaintFileController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setOrderItemMaintFileForm($container->get(OrderItemMaintFileForm::class));
        $controller->setOrderItemMaintService($container->get(OrderItemMaintService::class));
		return $controller;
	}
}
