<?php

namespace Lerp\Order\Factory\Controller\Rest\Order\Maint;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Controller\Rest\Order\Maint\OrderItemMaintFindController;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFindForm;
use Lerp\Order\Service\Maint\OrderItemMaintService;

class OrderItemMaintFindControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OrderItemMaintFindController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOrderItemMaintService($container->get(OrderItemMaintService::class));
        $controller->setOrderItemMaintFindForm($container->get(OrderItemMaintFindForm::class));
        return $controller;
    }
}
