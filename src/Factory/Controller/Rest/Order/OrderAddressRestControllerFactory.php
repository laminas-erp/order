<?php

namespace Lerp\Order\Factory\Controller\Rest\Order;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Customer\Service\Address\AddressCustomerRelService;
use Lerp\Order\Controller\Rest\Order\OrderAddressRestController;
use Lerp\Order\Service\Order\OrderService;

class OrderAddressRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OrderAddressRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOrderService($container->get(OrderService::class));
        $controller->setAddressCustomerRelService($container->get(AddressCustomerRelService::class));
        return $controller;
    }
}
