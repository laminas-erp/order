<?php

namespace Lerp\Order\Factory\Controller\Rest\Order;

use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Order\Controller\Rest\Order\OrderItemRestController;
use Lerp\Order\Form\Order\OrderItemForm;
use Lerp\Order\Service\Order\OrderItemService;

class OrderItemRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OrderItemRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOrderItemService($container->get(OrderItemService::class));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $controller->setLanguagesSupported($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        $controller->setOrderItemForm($container->get(OrderItemForm::class));
        $controller->setOrderDocumentService($container->get(OrderDocumentService::class));
        $controller->setTranslator($container->get('translator'));
        return $controller;
    }
}
