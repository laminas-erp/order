<?php

namespace Lerp\Order\Factory\Form\Offer;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\CostCentreService;
use Lerp\Order\Form\Offer\OfferItemForm;

class OfferItemFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new OfferItemForm();
        /** @var $css CostCentreService */
        $css = $container->get(CostCentreService::class);
        $form->setCostCentreIdAssoc($css->getCostCentreIdAssoc());
        return $form;
    }
}
