<?php

namespace Lerp\Order\Factory\Form\Order;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\CostCentreService;
use Lerp\Location\Table\LocationPlaceTable;
use Lerp\Order\Form\Order\OrderForm;

class OrderFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var  $form */
        $form = new OrderForm();
        $form->setAdapter($container->get('dbDefault'));
        /** @var $css CostCentreService */
        $css = $container->get(CostCentreService::class);
        $form->setCostCentreIdAssoc($css->getCostCentreIdAssoc());
        /** @var LocationPlaceTable $lpt */
        $lpt = $container->get(LocationPlaceTable::class);
        $form->setLocationPlaceUuidAssoc($lpt->getLocationPlacesUuidAssoc());
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setLangIsos($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        return $form;
    }
}
