<?php

namespace Lerp\Order\Factory\Unique;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Order\Table\Order\OrderTable;
use Lerp\Order\Unique\UniqueNumberProvider;

class UniqueNumberProviderFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $unique = new UniqueNumberProvider();
        $unique->setLogger($container->get('logger'));
        $unique->setOrderTable($container->get(OrderTable::class));
        $unique->setOfferTable($container->get(OfferTable::class));
        return $unique;
    }
}
