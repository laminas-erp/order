<?php

namespace Lerp\Order\Service\Order\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Sql\Select;
use Lerp\Order\Table\Order\Files\FileOrderRelTable;
use Lerp\Product\Table\Files\FileProductRelTable;

class FileOrderRelService extends AbstractService
{
    protected FileOrderRelTable $fileOrderRelTable;
    protected FileProductRelTable $fileProductRelTable;
    protected FileService $fileService;

    public function setFileOrderRelTable(FileOrderRelTable $fileOrderRelTable): void
    {
        $this->fileOrderRelTable = $fileOrderRelTable;
    }

    public function setFileProductRelTable(FileProductRelTable $fileProductRelTable): void
    {
        $this->fileProductRelTable = $fileProductRelTable;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $fileOrderRelUuid
     * @return array
     */
    public function getFileOrderRel(string $fileOrderRelUuid): array
    {
        return $this->fileOrderRelTable->getFileOrderRel($fileOrderRelUuid);
    }

    public function getFileOrderRelJoined(string $fileOrderRelUuid): array
    {
        return $this->fileOrderRelTable->getFileOrderRelJoined($fileOrderRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $orderUuid
     * @param string $folderBrand
     * @return string fileOrderRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $orderUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }
        return $this->insertFileOrderRel($fileUuid, $orderUuid);
    }

    /**
     * @param string $filePath
     * @param string $fileLabel
     * @param string $fileDesc
     * @param string $orderUuid
     * @param string $folderBrand
     * @return string
     */
    public function handleFileCopy(string $filePath, string $fileLabel, string $fileDesc, string $orderUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileCopy($filePath, $fileLabel, $fileDesc, $folderBrand))) {
            return '';
        }
        return $this->insertFileOrderRel($fileUuid, $orderUuid);
    }

    protected function insertFileOrderRel(string $fileUuid, string $orderUuid): string
    {
        if (empty($fileOrderRelUuid = $this->fileOrderRelTable->insertFileOrderRel($fileUuid, $orderUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $fileOrderRelUuid;
    }

    public function deleteFile(string $fileOrderRelUuid): bool
    {
        $connection = $this->beginTransaction($this->fileOrderRelTable);

        $fileRel = $this->fileOrderRelTable->getFileOrderRel($fileOrderRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->fileOrderRelTable->deleteFile($fileOrderRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForOrder(string $orderUuid): array
    {
        return $this->fileOrderRelTable->getFilesForOrder($orderUuid);
    }

    public function updateFile(FileEntity $fileEntity, string $orderUuid): bool
    {
        return $this->fileOrderRelTable->updateFile($fileEntity, $orderUuid) >= 0;
    }

    public function getProductFilesForOrder(string $orderUuid): array
    {
        $select = new Select('order_item');
        $select->columns(['product_uuid']);
        $select->where(['order_uuid' => $orderUuid]);
        return $this->fileProductRelTable->getFilesForProductsWithSelect($select);
    }
}
