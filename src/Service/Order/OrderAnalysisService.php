<?php

namespace Lerp\Order\Service\Order;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Order\Entity\ParamsOrderAnalysis;
use Lerp\Order\Table\Order\OrderAnalysisTable;

class OrderAnalysisService extends AbstractService
{
    protected OrderAnalysisTable $orderAnalysisTable;

    public function setOrderAnalysisTable(OrderAnalysisTable $orderAnalysisTable): void
    {
        $this->orderAnalysisTable = $orderAnalysisTable;
    }

    /**
     * @param ParamsOrderAnalysis $paramsOrderAnalysis
     * @return array FROM view_analysis_order.
     */
    public function searchOrderAnalysis(ParamsOrderAnalysis $paramsOrderAnalysis): array
    {
        return $this->orderAnalysisTable->searchOrderAnalysis($paramsOrderAnalysis);
    }

    /**
     * @param ParamsOrderAnalysis $paramsOrderAnalysis With 'groupBy'
     * @return array
     */
    public function getOrderAnalysisGrouped(ParamsOrderAnalysis $paramsOrderAnalysis): array
    {
        return $this->orderAnalysisTable->getOrderAnalysisGrouped($paramsOrderAnalysis);
    }
}
