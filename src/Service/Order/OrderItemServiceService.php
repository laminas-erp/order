<?php

namespace Lerp\Order\Service\Order;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Order\Entity\Order\OrderItemServiceEntity;
use Lerp\Order\Table\Order\Service\OrderItemServiceTable;

class OrderItemServiceService extends AbstractService
{
    protected OrderItemServiceTable $orderItemServiceTable;

    public function setOrderItemServiceTable(OrderItemServiceTable $orderItemServiceTable): void
    {
        $this->orderItemServiceTable = $orderItemServiceTable;
    }

    /**
     * @param string $orderItemUuid
     * @return array
     */
    public function getOrderItemServiceWithOrderItemUuid(string $orderItemUuid): array
    {
        return $this->orderItemServiceTable->getOrderItemServiceWithOrderItemUuid($orderItemUuid);
    }

    /**
     * @param OrderItemServiceEntity $orderItemServiceEntity
     * @return bool
     */
    public function updateOrderItemService(OrderItemServiceEntity $orderItemServiceEntity): bool
    {
        return $this->orderItemServiceTable->updateOrderItemService($orderItemServiceEntity) >= 0;
    }
}
