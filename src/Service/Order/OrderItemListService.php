<?php

namespace Lerp\Order\Service\Order;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Log\Logger;
use Lerp\Order\Entity\Order\OrderItemListEntity;
use Lerp\Order\Table\Order\OrderItemListTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Product\Table\ProductTable;

class OrderItemListService extends AbstractService
{
    protected OrderItemListTable $orderItemListTable;
    protected ProductTable $productTable;

    public function setOrderItemListTable(OrderItemListTable $orderItemListTable): void
    {
        $this->orderItemListTable = $orderItemListTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    /**
     * E.g. if it comes from POST.
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @param string $productUuid
     * @return bool
     */
    public function validInsertOrderItemList(string $orderItemUuid, string $productUuidParent, string $productUuid): bool
    {
        $productParent = $this->productTable->getProductSimple($productUuidParent);
        if (
            $productUuidParent == $productUuid
            || $productParent['product_structure'] !== 'list'
            || in_array($productUuid, $this->getOrderItemListRecursivParents($orderItemUuid, $productUuidParent))
            || $this->orderItemListTable->existNeighbor($orderItemUuid, $productUuidParent, $productUuid)
        ) {
            return false;
        }
        return true;
    }

    public function getOrderItemList(string $orderItemListUuid): array
    {
        return $this->orderItemListTable->getOrderItemList($orderItemListUuid);
    }

    public function getOrderItemListForFactoryorder(string $orderItemListUuid): array
    {
        return $this->orderItemListTable->getOrderItemListForFactoryorder($orderItemListUuid);
    }

    /**
     * @param string $orderItemUuid
     * @return array A flat (none hierarchical) list of all orderItemLists for orderItem - without this orderItem.
     */
    public function getOrderItemListsForOrderItem(string $orderItemUuid): array
    {
        return $this->orderItemListTable->getOrderItemListsForOrderItem($orderItemUuid);
    }

    /**
     * @param string $orderItemUuid
     * @param string $productUuid
     * @return array
     */
    public function getOrderItemListsForOrderItemHierarchic(string $orderItemUuid, string $productUuid): array
    {
        return $this->orderItemListTable->getOrderItemListsForOrderItemHierarchic($orderItemUuid, $productUuid);
    }

    /**
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @return array From db.view_order_item_list_for_factoryorder
     */
    public function getOrderItemListLevelForFactoryorder(string $orderItemUuid, string $productUuidParent): array
    {
        return $this->orderItemListTable->getOrderItemListLevelForFactoryorder($orderItemUuid, $productUuidParent);
    }

    /**
     * @param OrderItemListEntity $entity
     * @return string
     */
    public function insertOrderItemListWithEntity(OrderItemListEntity $entity): string
    {
        return $this->orderItemListTable->insertOrderItemList($entity);
    }

    /**
     * @param string $orderItemUuid
     * @return bool
     */
    public function deleteOrderItemListsForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemListTable->deleteOrderItemListsForOrderItem($orderItemUuid) >= 0;
    }

    public function deleteOrderItemListItem(string $orderItemListUuid): int
    {
        return $this->orderItemListTable->deleteOrderItemListItem($orderItemListUuid) >= 0;
    }

    /**
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @return array Array with product_uuid's
     */
    public function getOrderItemListRecursivParents(string $orderItemUuid, string $productUuidParent): array
    {
        return $this->orderItemListTable->getOrderItemListRecursivParents($orderItemUuid, $productUuidParent);
    }

    /**
     * @param string $orderItemListUuid
     * @param string $direc
     * @return bool TRUE if nothing to update.
     */
    public function updateListItemOrderPriority(string $orderItemListUuid, string $direc): bool
    {
        $neighbors = $this->orderItemListTable->getNeighborsByListUuid($orderItemListUuid);
        if (empty($neighbors)) {
            false;
        }
        foreach ($neighbors as $i => $neighbor) {
            if ($neighbor['order_item_list_uuid'] == $orderItemListUuid) {
                $self = $neighbor;
                $top = isset($neighbors[$i - 1]) ? $neighbors[$i - 1] : null;
                $down = isset($neighbors[$i + 1]) ? $neighbors[$i + 1] : null;
            }
        }
        if ($direc === 'up') {
            if (!isset($top)) {
                return false;
            }
            $this->orderItemListTable->updateOrderItemListItemOrderPriority($top['order_item_list_uuid'], $self['order_item_list_order_priority']);
            $this->orderItemListTable->updateOrderItemListItemOrderPriority($self['order_item_list_uuid'], $top['order_item_list_order_priority']);
        } else if ($direc === 'down') {
            if (!isset($down)) {
                return false;
            }
            $this->orderItemListTable->updateOrderItemListItemOrderPriority($down['order_item_list_uuid'], $self['order_item_list_order_priority']);
            $this->orderItemListTable->updateOrderItemListItemOrderPriority($self['order_item_list_uuid'], $down['order_item_list_order_priority']);
        }
        return true;
    }

    /**
     * @param string $orderItemListUuid
     * @param float $quantity
     * @return bool
     */
    public function updateListItemQuantity(string $orderItemListUuid, float $quantity): bool
    {
        return $this->orderItemListTable->updateOrderItemListItemQuantity($orderItemListUuid, $quantity) > 0;
    }
}
