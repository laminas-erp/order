<?php

namespace Lerp\Order\Service\Order;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Order\Table\Order\OrderItemPartTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\OrderTable;
use Lerp\Order\Unique\UniqueNumberProvider;
use Lerp\Order\Unique\UniqueNumberProviderInterface;

class OrderService extends AbstractService
{
    protected int $searchOrderCount = 0;
    protected OrderTable $orderTable;
    protected OrderItemTable $orderItemTable;
    protected OrderItemPartTable $orderItemPartTable;
    protected UniqueNumberProviderInterface $uniqueNumberProvider;

    public function getSearchOrderCount(): int
    {
        return $this->searchOrderCount;
    }

    public function setOrderTable(OrderTable $orderTable): void
    {
        $this->orderTable = $orderTable;
    }

    public function setOrderItemTable(OrderItemTable $orderItemTable): void
    {
        $this->orderItemTable = $orderItemTable;
    }

    public function setOrderItemPartTable(OrderItemPartTable $orderItemPartTable): void
    {
        $this->orderItemPartTable = $orderItemPartTable;
    }

    public function setUniqueNumberProvider(UniqueNumberProviderInterface $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    /**
     * @param OrderEntity $orderEntity
     * @return string Order UUID from the new created order.
     */
    public function insertOrder(OrderEntity $orderEntity): string
    {
        $orderEntity->unsetEmptyValues();
        $orderEntity->setOrderNoCompl($this->uniqueNumberProvider->computeGetNumberComplete(UniqueNumberProvider::TYPE_ORDER));
        $orderEntity->setOrderNo($this->uniqueNumberProvider->getNumber());
        return $this->orderTable->insertOrder($orderEntity);
    }

    /**
     * @param string $orderNoCompl
     * @param string $label
     * @param string $customerNo
     * @param string $offerNoCompl
     * @param int $costCentreId
     * @param string $locationPlaceUuid
     * @param string $timeCreateFrom
     * @param string $timeCreateTo
     * @param string $timeFinishScheduleFrom
     * @param string $timeFinishScheduleTo
     * @param string $timeFinishRealFrom
     * @param string $timeFinishRealTo
     * @param string $userUuidCreate
     * @param bool $onlyOpen
     * @param bool $onlyFinish
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function searchOrder(
        string   $orderNoCompl,
        string   $label
        , string $customerNo
        , string $offerNoCompl
        , int    $costCentreId
        , string $locationPlaceUuid
        , string $timeCreateFrom
        , string $timeCreateTo
        , string $timeFinishScheduleFrom
        , string $timeFinishScheduleTo
        , string $timeFinishRealFrom
        , string $timeFinishRealTo
        , string $userUuidCreate
        , bool   $onlyOpen
        , bool   $onlyFinish
        , string $orderField, string $orderDirec, int $offset = 0, int $limit = 0
    ): array
    {
        $orderEntity = new OrderEntity();
        if (!$orderEntity->fieldExistDatabase($orderField)) {
            $orderField = '';
        }
        if (!in_array($orderDirec, ['ASC', 'DESC'])) {
            $orderDirec = 'DESC';
        }
        $this->searchOrderCount = (int)$this->orderTable->searchOrder($orderNoCompl, $label, $customerNo, $offerNoCompl, $costCentreId, $locationPlaceUuid
            , $timeCreateFrom, $timeCreateTo, $timeFinishScheduleFrom, $timeFinishScheduleTo, $timeFinishRealFrom, $timeFinishRealTo
            , $userUuidCreate, $onlyOpen, $onlyFinish, $orderField, $orderDirec, $offset, $limit, true)[0];
        return $this->orderTable->searchOrder($orderNoCompl, $label, $customerNo, $offerNoCompl, $costCentreId, $locationPlaceUuid
            , $timeCreateFrom, $timeCreateTo, $timeFinishScheduleFrom, $timeFinishScheduleTo, $timeFinishRealFrom, $timeFinishRealTo
            , $userUuidCreate, $onlyOpen, $onlyFinish, $orderField, $orderDirec, $offset, $limit);
    }

    /**
     * @param string $orderUuid
     * @return array From view_order.
     */
    public function getOrder(string $orderUuid): array
    {
        return $this->orderTable->getOrder($orderUuid);
    }

    /**
     * @param string $orderNoCompl
     * @return array From view_order.
     */
    public function getOrderByNumberComplete(string $orderNoCompl): array
    {
        return $this->orderTable->getOrderByNumberComplete($orderNoCompl);
    }

    /**
     * @param string $customerUuid
     * @return array From view_order.
     */
    public function getCustomerOrders(string $customerUuid): array
    {
        return $this->orderTable->getCustomerOrders($customerUuid);
    }

    /**
     * Update ONLY order_time_finish_real WITHOUT order_time_update.
     *
     * @param string $orderUuid
     * @return bool
     */
    public function updateOrderTimeFinishReal(string $orderUuid): bool
    {
        return $this->orderTable->updateOrderTimeFinishReal($orderUuid) >= 0;
    }

    /**
     * Update ONLY order_finish_unlock WITHOUT order_time_update.
     *
     * @param string $orderUuid
     * @param bool $unlock
     * @return bool
     */
    public function updateOrderFinishUnlock(string $orderUuid, bool $unlock): bool
    {
        return $this->orderTable->updateOrderFinishUnlock($orderUuid, $unlock) >= 0;
    }

    /**
     * UPDATE order fields they are not empty in the entity. AND set 'order_finish_unlock'=false
     *
     * @param OrderEntity $orderEntity
     * @return bool
     */
    public function updateOrder(OrderEntity $orderEntity): bool
    {
        return $this->orderTable->updateOrder($orderEntity) >= 0;
    }

    /**
     * @param string $orderUuid
     * @return array Array keys: total_sum, total_sum_tax, total_sum_end
     */
    public function getOrderSummary(string $orderUuid): array
    {
        return $this->orderTable->getOrderSummary($orderUuid);
    }

    public function getOrderItemIdNext(string $orderUuid): int
    {
        return $this->orderItemTable->getOrderItemIdNext($orderUuid);
    }

    public function updateOrderUserContact(string $orderUuid, string $userUuid): bool
    {
        return $this->orderTable->updateOrderUserContact($orderUuid, $userUuid) >= 0;
    }

    /**
     * @param string $orderUuid
     * @param string|null $addressCustomerRelUuid
     * @return bool
     */
    public function updateAddressCustomerRelUuid(string $orderUuid, ?string $addressCustomerRelUuid): bool
    {
        if (!$addressCustomerRelUuid) {
            return $this->orderTable->deleteAddressCustomerRelUuid($orderUuid) >= 0;
        }
        return $this->orderTable->updateAddressCustomerRelUuid($orderUuid, $addressCustomerRelUuid) >= 0;
    }

    /**
     * @param string $orderUuid
     * @param string|null $contactCustomerRelUuid
     * @return bool
     */
    public function updateContactCustomerRelUuid(string $orderUuid, ?string $contactCustomerRelUuid): bool
    {
        if (!$contactCustomerRelUuid) {
            return $this->orderTable->deleteContactCustomerRelUuid($orderUuid) >= 0;
        }
        return $this->orderTable->updateContactCustomerRelUuid($orderUuid, $contactCustomerRelUuid) >= 0;
    }
}
