<?php

namespace Lerp\Order\Service\Order;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\User\Entity\User\UserEntity;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Document\Table\Order\DocDeliverySendTable;
use Lerp\Document\Table\Order\DocEstimateSendTable;
use Lerp\Document\Table\Order\DocInvoiceSendTable;
use Lerp\Document\Table\Order\DocOrderConfirmSendTable;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Order\Table\Order\Mail\MailSendOrderRelTable;
use Lerp\Order\Table\Order\OrderTable;

class OrderMailService extends AbstractService
{
    protected DocumentService $documentService;
    protected MailService $mailService;
    protected FileService $fileService;
    protected OrderDocumentService $orderDocumentService;
    protected DocDeliverySendTable $docDeliverySendTable;
    protected DocEstimateSendTable $docEstimateSendTable;
    protected DocInvoiceSendTable $docInvoiceSendTable;
    protected DocOrderConfirmSendTable $docOrderConfirmSendTable;
    protected MailSendOrderRelTable $mailSendOrderRelTable;
    protected OrderTable $orderTable;
    protected UserEquipService $userEquipService;
    protected string $appUrl;
    protected string $appProtocol;

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    public function setOrderDocumentService(OrderDocumentService $orderDocumentService): void
    {
        $this->orderDocumentService = $orderDocumentService;
    }

    public function setDocDeliverySendTable(DocDeliverySendTable $docDeliverySendTable): void
    {
        $this->docDeliverySendTable = $docDeliverySendTable;
    }

    public function setDocEstimateSendTable(DocEstimateSendTable $docEstimateSendTable): void
    {
        $this->docEstimateSendTable = $docEstimateSendTable;
    }

    public function setDocInvoiceSendTable(DocInvoiceSendTable $docInvoiceSendTable): void
    {
        $this->docInvoiceSendTable = $docInvoiceSendTable;
    }

    public function setDocOrderConfirmSendTable(DocOrderConfirmSendTable $docOrderConfirmSendTable): void
    {
        $this->docOrderConfirmSendTable = $docOrderConfirmSendTable;
    }

    public function setMailSendOrderRelTable(MailSendOrderRelTable $mailSendOrderRelTable): void
    {
        $this->mailSendOrderRelTable = $mailSendOrderRelTable;
    }

    public function setOrderTable(OrderTable $orderTable): void
    {
        $this->orderTable = $orderTable;
    }

    public function setUserEquipService(UserEquipService $userEquipService): void
    {
        $this->userEquipService = $userEquipService;
    }

    public function setAppUrl(string $appUrl): void
    {
        $this->appUrl = $appUrl;
    }

    public function setAppProtocol(string $appProtocol): void
    {
        $this->appProtocol = $appProtocol;
    }

    /**
     * @param MailEntity $mailEntity
     * @param string $orderUuid
     * @param array $docDeliveryUuids
     * @param array $docEstimateUuids
     * @param array $docInvoiceUuids
     * @param array $docOrderConfirmUuids
     * @param array $fileUuids
     * @param string $userUuid
     * @return bool
     */
    public function sendDocuments(MailEntity $mailEntity, string $orderUuid,
                                  array      $docDeliveryUuids, array $docEstimateUuids, array $docInvoiceUuids, array $docOrderConfirmUuids,
                                  array      $fileUuids, string $userUuid): bool
    {
        if (empty($orderUuid)) {
            return false;
        }
        $conn = $this->beginTransaction($this->docDeliverySendTable);
        if (empty($mailSendUuid = $this->mailService->saveMail($mailEntity, $fileUuids, $userUuid))
            || empty($mailSendOrderRelUuid = $this->mailSendOrderRelTable->insertMailSendOrderRel($mailSendUuid, $orderUuid))) {
            $conn->rollback();
            return false;
        }
        $recipients = implode(',', $mailEntity->getRecipients());
        foreach ($fileUuids as $uuid) {
            if (empty($file = $this->fileService->getFileForOutput($uuid))) {
                continue;
            }
            $filepath = realpath($file['fqfn']);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, $file['file_mimetype']);
        }
        foreach ($docDeliveryUuids as $uuid) {
            if (empty($doc = $this->orderDocumentService->getDocDelivery($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_ORDER_DELIVERY
                    , $doc['order_time_create_unix']
                    , $doc['order_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_delivery_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docDeliverySendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendOrderRelUuid)) {
                $conn->rollback();
                return false;
            }
        }
        foreach ($docEstimateUuids as $uuid) {
            if (empty($doc = $this->orderDocumentService->getDocEstimate($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_ESTIMATE
                    , $doc['order_time_create_unix']
                    , $doc['order_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_estimate_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docEstimateSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendOrderRelUuid)) {
                $conn->rollback();
                return false;
            }
        }
        foreach ($docInvoiceUuids as $uuid) {
            if (empty($doc = $this->orderDocumentService->getDocInvoice($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_INVOICE
                    , $doc['order_time_create_unix']
                    , $doc['order_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_invoice_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docInvoiceSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendOrderRelUuid)) {
                $conn->rollback();
                return false;
            }
        }
        foreach ($docOrderConfirmUuids as $uuid) {
            if (empty($doc = $this->orderDocumentService->getDocOrderConfirm($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_ORDER_CONFIRM
                    , $doc['order_time_create_unix']
                    , $doc['order_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_order_confirm_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docOrderConfirmSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendOrderRelUuid)) {
                $conn->rollback();
                return false;
            }
        }

        if ($this->mailService->sendMail($mailEntity) < 1) {
            $conn->rollback();
            return false;
        }
        $conn->commit();
        return true;
    }

    /**
     * @param string $orderUuid
     * @return array From view_mail_send_order
     */
    public function getMailsSendOrder(string $orderUuid): array
    {
        return $this->mailSendOrderRelTable->getMailsSendOrder($orderUuid);
    }

    /**
     * @param string $mailSendOrderRelUuid
     * @return array From view_mail_send_order
     */
    public function getMailSendOrder(string $mailSendOrderRelUuid): array
    {
        return $this->mailSendOrderRelTable->getMailSendOrder($mailSendOrderRelUuid);
    }

    /**
     * Send the email with the request to unlocking an order.
     * User with right 'unlock_order_finish_real' (table user_right) are recipients.
     *
     * @param string $orderUuid
     * @param UserEntity $userEntity The sender!
     * @return bool
     */
    public function sendOrderUnlockRequest(string $orderUuid, UserEntity $userEntity): bool
    {
        if (empty($order = $this->orderTable->getOrder($orderUuid))
            || empty($recipients = $this->userEquipService->getUsersByRightAlias('unlock_order_finish_real'))) {
            return false;
        }
        $orderNoCompl = $order['order_no_compl'];
        $mailEntity = new MailEntity(new \HTMLPurifier());
        $mailEntity->setFromName($userEntity->getUserLogin() . ' (' . $userEntity->getUserEmail() . ')');
        $mailEntity->setFromEmail($userEntity->getUserEmail());
        $mailEntity->setSubject('Bitte den Auftrag ' . $orderNoCompl . ' entsperren');

        $recipient = array_shift($recipients);
        $mailEntity->setToEmail($recipient['user_email']);
        $mailEntity->setToName($recipient['user_details_name_first'] . ' ' . $recipient['user_details_name_last']);
        foreach ($recipients as $recipient) {
            $mailEntity->addCc($recipient['user_email']);
        }
        $url = $this->appProtocol . '://' . $this->appUrl . '/order/order/' . $order['order_uuid'];

        $mailEntity->setContentHtml('Bitte den Auftrag <a href="' . $url . '">' . $orderNoCompl . '</a> entsperren.');
        if ($this->mailService->sendMail($mailEntity) < 1) {
            return false;
        }
        return true;
    }
}
