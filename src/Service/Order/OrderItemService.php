<?php

namespace Lerp\Order\Service\Order;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Log\Logger;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Order\Entity\Order\OrderItemEntity;
use Lerp\Order\Entity\Order\OrderItemListEntity;
use Lerp\Order\Service\Maint\OrderItemMaintService;
use Lerp\Order\Table\Order\OrderItemPartTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\Service\OrderItemServiceTable;
use Lerp\Order\Table\Order\ViewOrderItemListStockTable;
use Lerp\Order\Table\Order\ViewOrderItemStockTable;
use Lerp\Product\Entity\ProductEntity;
use Lerp\Product\Service\Maint\ProductMaintService;
use Lerp\Product\Service\ProductListService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductTextService;
use Lerp\ProductCalc\Service\ProductCalcService;

class OrderItemService extends AbstractService
{
    protected OrderItemTable $orderItemTable;
    protected OrderItemPartTable $orderItemPartTable;
    protected OrderItemServiceTable $orderItemServiceTable;
    protected ViewOrderItemListStockTable $viewOrderItemListStockTable;
    protected ViewOrderItemStockTable $viewOrderItemStockTable;
    protected ProductService $productService;
    protected ProductTextService $productTextService;
    protected ProductCalcService $productCalcService;
    protected ProductListService $productListService;
    protected OrderService $orderService;
    protected OrderItemListService $orderItemListService;
    protected OrderItemMaintService $orderItemMaintService;
    protected ProductMaintService $productMaintService;

    public function setOrderItemTable(OrderItemTable $orderItemTable): void
    {
        $this->orderItemTable = $orderItemTable;
    }

    public function setOrderItemPartTable(OrderItemPartTable $orderItemPartTable): void
    {
        $this->orderItemPartTable = $orderItemPartTable;
    }

    public function setOrderItemServiceTable(OrderItemServiceTable $orderItemServiceTable): void
    {
        $this->orderItemServiceTable = $orderItemServiceTable;
    }

    public function setViewOrderItemListStockTable(ViewOrderItemListStockTable $viewOrderItemListStockTable): void
    {
        $this->viewOrderItemListStockTable = $viewOrderItemListStockTable;
    }

    public function setViewOrderItemStockTable(ViewOrderItemStockTable $viewOrderItemStockTable): void
    {
        $this->viewOrderItemStockTable = $viewOrderItemStockTable;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setProductTextService(ProductTextService $productTextService): void
    {
        $this->productTextService = $productTextService;
    }

    public function setProductCalcService(ProductCalcService $productCalcService): void
    {
        $this->productCalcService = $productCalcService;
    }

    public function setProductListService(ProductListService $productListService): void
    {
        $this->productListService = $productListService;
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setOrderItemListService(OrderItemListService $orderItemListService): void
    {
        $this->orderItemListService = $orderItemListService;
    }

    public function setOrderItemMaintService(OrderItemMaintService $orderItemMaintService): void
    {
        $this->orderItemMaintService = $orderItemMaintService;
    }

    public function setProductMaintService(ProductMaintService $productMaintService): void
    {
        $this->productMaintService = $productMaintService;
    }

    /**
     * @param string $orderItemUuid
     * @param int $view One of the constants OrderItemTable::VIEW_ORDER_ITEM or OrderItemTable::VIEW_ORDER_ITEM_FACTORYORDER. The views use several sub queries.
     * @return array
     */
    public function getOrderItem(string $orderItemUuid, int $view = 0): array
    {
        return $this->orderItemTable->getOrderItem($orderItemUuid, $view);
    }

    /**
     * @param string $orderUuid
     * @param int $view One of the constants from self::$views.
     * @return array
     */
    public function getOrderItems(string $orderUuid, int $view): array
    {
        return $this->orderItemTable->getOrderItems($orderUuid, $view);
    }

    /**
     * @param string $orderItemUuid
     * @return array From db.view_order_item_for_factoryorder.
     */
    public function getOrderItemForFactoryorder(string $orderItemUuid): array
    {
        return $this->orderItemTable->getOrderItemForFactoryorder($orderItemUuid);
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_order_item
     */
    public function getOrderItemsForOrder(string $orderUuid): array
    {
        return $this->orderItemTable->getOrderItemsForOrder($orderUuid);
    }

    public function getOrderItemsWithUuids(array $orderItemUuids): array
    {
        return $this->orderItemTable->getOrderItemsWithUuids($orderItemUuids);
    }

    /**
     * Updates orderPriority for $orderItemUuid (param) and his target place orderItem.
     *
     * @param string $orderItemUuid
     * @param int $prioPrevious
     * @param int $prioCurrent
     * @return bool
     */
    public function updateOrderItemListOrderPriority(string $orderItemUuid, int $prioPrevious, int $prioCurrent): bool
    {
        if ($prioPrevious == $prioCurrent) {
            return false;
        }
        $orderItem = $this->orderItemTable->getOrderItem($orderItemUuid);
        if (empty($orderItem)) {
            return false;
        }
        /** @var Adapter $adapter */
        $adapter = $this->orderItemTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $success = true;
        $orderPrio = $prioCurrent;
        if ($prioPrevious > $prioCurrent) {
            // down
            $items = $this->orderItemTable->getOrderItemsForOrderPriority($orderItem['order_uuid'], $prioCurrent, false);
            foreach ($items as $item) {
                $orderPrio += 10;
                if ($this->orderItemTable->updateOrderItemsOrderPriority($item['order_item_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
        } else {
            // up
            $items = $this->orderItemTable->getOrderItemsForOrderPriority($orderItem['order_uuid'], $prioCurrent, true);
            foreach ($items as $item) {
                $orderPrio -= 10;
                if ($this->orderItemTable->updateOrderItemsOrderPriority($item['order_item_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
        }
        if ($this->orderItemTable->updateOrderItemsOrderPriority($orderItemUuid, $prioCurrent) < 0) {
            $success = false;
        }
        if ($success) {
            $connection->commit();
            return true;
        } else {
            $connection->rollback();
            return false;
        }
    }

    /**
     * Fetch product text for $lang and use it.
     * Depending on the product structure, call another function to create the order item.
     *
     * @param string $orderUuid
     * @param string $productUuid
     * @param string $lang
     * @return string order_item_uuid
     */
    public function insertOrderItemSimple(string $orderUuid, string $productUuid, string $lang): string
    {
        $orderData = $this->orderService->getOrder($orderUuid);
        $orderEntity = new OrderEntity();
        $productData = $this->productService->getProduct($productUuid);
        $productEntity = new ProductEntity();
        if (empty($orderData) || !$orderEntity->exchangeArrayFromDatabase($orderData) || empty($productData)) {
            return '';
        }
        if (!empty($productText = $this->productTextService->getProductTextsForProductAndLang($productUuid, $lang))) {
            $productData['product_text_short'] = $productText['product_text_text_short'];
            $productData['product_text_long'] = $productText['product_text_text_long'];
        }
        if (!$productEntity->exchangeArrayFromDatabase($productData)) {
            return '';
        }
        $orderItem = new OrderItemEntity();
        $orderItem->setOrderUuid($orderUuid);
        $orderItem->setProductUuid($productEntity->getUuid());
        $orderItem->setOrderItemTextShort($productEntity->getProductTextShort());
        $orderItem->setOrderItemTextLong($productEntity->getProductTextLong());
        $orderItem->setQuantityunitUuid($productEntity->getQuantityUnitUuid());
        $orderItem->setOrderItemQuantity(1);
        $orderItem->setCostCentreId($orderEntity->getCostCentreId());
        $orderItem->setOrderItemTaxp($productEntity->getTaxp());
        if (
            !empty($productCalc = $this->productCalcService->getProductCalcLastForProduct($productEntity->getUuid()))
            && isset($productCalc['product_calc_price_set'])
        ) {
            $orderItem->setOrderItemPrice($productCalc['product_calc_price_set']);
            $orderItem->setOrderItemPriceTotal($productCalc['product_calc_price_set']);
        } else {
            $orderItem->setOrderItemPrice(0);
            $orderItem->setOrderItemPriceTotal(0);
        }
        $orderItem->setOrderItemOrderPriority($this->orderItemTable->getMaxOrderPriority($orderUuid) + 10);
        return $this->insertOrderItemWithEntity($productEntity->getStructure(), $orderItem);
    }

    /**
     * @param string $productStructure Depending on the product structure, different things are done.
     * @param OrderItemEntity|null $orderItem
     * @param bool $withTransaction
     * @return string order_item_uuid
     */
    public function insertOrderItemWithEntity(string $productStructure, OrderItemEntity $orderItem = null, bool $withTransaction = true): string
    {
        return match ($productStructure) {
            'pure', 'simple', 'standard', 'group' => $this->insertOrderItem($orderItem),
            'service' => $this->insertOrderItemProductStructureService($orderItem),
            'list' => $this->insertOrderItemProductStructureList($orderItem, $withTransaction),
            'maintenance' => $this->insertOrderItemProductStructureMaintenance($orderItem, $withTransaction),
            default => '',
        };
    }

    /**
     * This function is used by all other functions to add a new item.
     * Set a valid OrderItemId if required.
     *
     * @param OrderItemEntity $orderItem
     * @return string
     */
    protected function insertOrderItem(OrderItemEntity $orderItem): string
    {
        if (empty($orderItem->getOrderItemId()) || !$this->orderItemTable->validOrderItemIdNext($orderItem->getOrderUuid(), $orderItem->getOrderItemId())) {
            $orderItem->setOrderItemId($this->orderService->getOrderItemIdNext($orderItem->getOrderUuid()));
        }
        return $this->orderItemTable->insertOrderItem($orderItem);
    }

    protected function insertOrderItemProductStructureService(OrderItemEntity $orderItem): string
    {
        if (empty($uuid = $this->insertOrderItem($orderItem))) {
            return '';
        }
        if (empty($this->orderItemServiceTable->insertOrderItemService($uuid))) {
            $this->deleteOrderItem($uuid);
            return '';
        }
        return $uuid;
    }

    /**
     * INSERT
     * - FROM product AND product_calc INTO order_item
     * - FROM hierarchically product_list & FROM product INTO order_item_list
     *
     * @param OrderItemEntity|null $orderItem
     * @param bool $withTransaction
     * @return string The order_item_uuid
     */
    protected function insertOrderItemProductStructureList(OrderItemEntity $orderItem = null, bool $withTransaction = true): string
    {
        if (empty($uuid = $this->insertOrderItem($orderItem))) {
            return '';
        }
        $productLists = $this->productListService->getProductListRecursive($orderItem->getProductUuid());
        if (empty($productLists)) {
            return $uuid;
        }
        if ($withTransaction) {
            $conn = $this->beginTransaction($this->orderItemTable);
        }
        foreach ($productLists as $pla) {
            $orderItemList = [];
            $orderItemList['order_item_uuid'] = $uuid;
            $orderItemList['product_uuid_parent'] = $pla['product_uuid_parent'];
            $orderItemList['product_uuid'] = $pla['product_uuid'];
            $orderItemList['order_item_list_quantity'] = $pla['product_list_quantity'];
            $orderItemList['order_item_list_order_priority'] = $pla['product_list_order_priority'];
            $entity = new OrderItemListEntity();
            if (!$entity->exchangeArrayFromDatabase($orderItemList) || !$this->orderItemListService->insertOrderItemListWithEntity($entity)) {
                if ($withTransaction) {
                    $conn->rollback();
                }
                return '';
            }
        }
        if ($withTransaction) {
            $conn->commit();
        }
        return $uuid;
    }

    /**
     * INSERT data
     * - FROM product AND product_calc INTO order_item
     * - FROM product_maint INTO order_item_maint
     * - FROM product_maint_workflow INTO order_item_maint_workflow
     *
     * @param OrderItemEntity|null $orderItem
     * @param bool $withTransaction
     * @return string
     */
    protected function insertOrderItemProductStructureMaintenance(OrderItemEntity $orderItem = null, bool $withTransaction = true): string
    {
        if (
            empty($orderItemUuid = $this->insertOrderItem($orderItem))
            || empty($productMaints = $this->productMaintService->getProductMaintsForProduct($orderItem->getProductUuid()))
            || empty($product = $this->productService->getProduct($orderItem->getProductUuid()))
        ) {
            return $orderItemUuid;
        }
        if ($withTransaction) {
            $conn = $this->beginTransaction($this->orderItemTable);
        }
        foreach ($productMaints as $productMaint) {
            $arr = [
                'product_maint_uuid'              => $productMaint['product_maint_uuid'],
                'order_item_uuid'                 => $orderItemUuid,
                'order_item_maint_label'          => $productMaint['product_maint_label'],
                'order_item_maint_desc'           => $productMaint['product_maint_desc'],
                'order_item_maint_order_priority' => $productMaint['product_maint_order_priority'],
            ];
            if (empty($orderItemMaintUuid = $this->orderItemMaintService->insertOrderItemMaint($arr))) {
                if ($withTransaction) {
                    $conn->rollback();
                }
                return '';
            }
            $pmwfs = $this->productMaintService->getProductMaintWorkflowsForProductMaint($productMaint['product_maint_uuid']);
            if (empty($pmwfs)) {
                continue;
            }
            foreach ($pmwfs as $pmwf) {
                $arr = [
                    'product_maint_workflow_uuid'              => $pmwf['product_maint_workflow_uuid'],
                    'order_item_maint_uuid'                    => $orderItemMaintUuid,
                    'order_item_maint_workflow_label'          => $pmwf['product_maint_workflow_label'],
                    'order_item_maint_workflow_desc'           => $pmwf['product_maint_workflow_desc'],
                    'order_item_maint_workflow_order_priority' => $pmwf['product_maint_workflow_order_priority'],
                ];
                if (empty($this->orderItemMaintService->insertOrderItemMaintWorkflow($arr))) {
                    if ($withTransaction) {
                        $conn->rollback();
                    }
                    return '';
                }

            }
        }
        if ($withTransaction) {
            $conn->commit();
        }
        return $orderItemUuid;
    }

    public function updateOrderItem(string $orderItemUuid, array $storage): bool
    {
        $storage['order_item_price_total'] = $storage['order_item_price'] * $storage['order_item_quantity'];
        return $this->orderItemTable->updateOrderItem($orderItemUuid, $storage) >= 0;
    }

    /**
     * DELETE FROM
     * - order_item
     *   - order_item_list
     *   - order_item_maint
     *     - order_item_maint_file
     *     - order_item_maint_find
     *     - order_item_maint_gotten
     *     - order_item_maint_part
     *     - order_item_maint_tool
     *     - order_item_maint_workflow
     *   - order_item_part
     *
     * Check if doc exist for orderItem must be done!
     *
     * @param string $orderItemUuid
     * @return bool
     */
    public function deleteOrderItem(string $orderItemUuid): bool
    {
        $orderItem = $this->orderItemTable->getOrderItem($orderItemUuid);
        if (empty($orderItem)) {
            return false;
        }
        /** @var Adapter $adapter */
        $adapter = $this->orderItemTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        $success = true;
        switch ($orderItem['product_structure']) {
            case 'simple':
            case 'standard':
            case 'group':
                if (!$this->deleteOrderItemPart($orderItemUuid)) {
                    $success = false;
                }
                break;
            case 'service':
                if (!$this->deleteOrderItemPart($orderItemUuid) || !$this->deleteOrderItemService($orderItemUuid)) {
                    $success = false;
                }
                break;
            case 'list':
                if (!$this->deleteOrderItemPart($orderItemUuid) || !$this->deleteOrderItemLists($orderItemUuid)) {
                    $success = false;
                }
                break;
            case 'maintenance':
                if (!$this->deleteOrderItemPart($orderItemUuid) || !$this->deleteOrderItemMaints($orderItemUuid)) {
                    $success = false;
                }
                break;
        }

        if ($success && $this->orderItemTable->deleteOrderItem($orderItemUuid) > 0) {
            $connection->commit();
            return true;
        }
        $connection->rollback();
        return false;
    }

    /**
     * DELETE FROM
     * - order_item_part
     *
     * @param string $orderItemUuid
     * @return bool
     */
    protected function deleteOrderItemPart(string $orderItemUuid): bool
    {
        return $this->orderItemPartTable->deleteOrderItemPartForOrderItem($orderItemUuid) >= 0;
    }

    /**
     * DELETE FROM
     * - order_item_list
     *
     * @param string $orderItemUuid
     * @return bool
     */
    protected function deleteOrderItemLists(string $orderItemUuid): bool
    {
        return $this->orderItemListService->deleteOrderItemListsForOrderItem($orderItemUuid);
    }

    /**
     * DELETE FROM
     * - order_item_service
     *
     * @param string $orderItemUuid
     * @return bool
     */
    protected function deleteOrderItemService(string $orderItemUuid): bool
    {
        return $this->orderItemServiceTable->deleteOrderItemServiceForOrderItem($orderItemUuid) >= 0;
    }

    /**
     * DELETE FROM
     * - order_item_maint
     * - order_item_maint_file (with files)
     * - order_item_maint_find
     * - order_item_maint_gotten
     * - order_item_maint_part
     * - order_item_maint_tool
     * - order_item_maint_workflow
     *
     * @param string $orderItemUuid
     * @return bool
     */
    protected function deleteOrderItemMaints(string $orderItemUuid): bool
    {
        return $this->orderItemMaintService->deleteAllForOrderItem($orderItemUuid);
    }

    /**
     * Stock for orderItemList items.
     * @param string $orderItemUuid
     * @param string $productUuidParent
     * @return array From db.view_order_item_list_stock
     */
    public function getOrderItemListStock(string $orderItemUuid, string $productUuidParent): array
    {
        return $this->viewOrderItemListStockTable->getOrderItemListStock($orderItemUuid, $productUuidParent);
    }

    /**
     * @param string $orderUuid
     * @return array From view_order_item_stock.
     */
    public function getOrderItemsStock(string $orderUuid): array
    {
        return $this->viewOrderItemStockTable->getViewOrderItemsStock($orderUuid);
    }
}
