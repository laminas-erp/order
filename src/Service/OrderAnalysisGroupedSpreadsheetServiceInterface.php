<?php

namespace Lerp\Order\Service;

use Lerp\Order\Entity\ParamsOrderAnalysis;

interface OrderAnalysisGroupedSpreadsheetServiceInterface
{
    public function createOrderAnalysisGroupedSpreadsheet(ParamsOrderAnalysis $paramsOrderAnalysis): bool;

    public function getMessage(): string;
}
