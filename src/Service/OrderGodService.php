<?php

namespace Lerp\Order\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFileTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFindTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintGottenTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintPartTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintToolTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintWorkflowTable;
use Lerp\Order\Table\Order\OrderItemListTable;
use Lerp\Order\Table\Order\OrderItemPartTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\OrderTable;

class OrderGodService extends AbstractService
{
    protected OfferTable $offerTable;
    protected OfferItemTable $offerItemTable;

    protected OrderTable $orderTable;
    protected OrderItemTable $orderItemTable;
    protected OrderItemListTable $orderItemListTable;
    protected OrderItemPartTable $orderItemPartTable;

    protected OrderItemMaintTable $orderItemMaintTable;
    protected OrderItemMaintFileTable $orderItemMaintFileTable;
    protected OrderItemMaintFindTable $orderItemMaintFindTable;
    protected OrderItemMaintGottenTable $orderItemMaintGottenTable;
    protected OrderItemMaintPartTable $orderItemMaintPartTable;
    protected OrderItemMaintToolTable $orderItemMaintToolTable;
    protected OrderItemMaintWorkflowTable $orderItemMaintWorkflowTable;
}
