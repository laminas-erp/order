<?php

namespace Lerp\Order\Service\Offer\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Sql\Select;
use Lerp\Order\Table\Offer\Files\FileOfferRelTable;
use Lerp\Product\Table\Files\FileProductRelTable;

class FileOfferRelService extends AbstractService
{
    protected FileOfferRelTable $fileOfferRelTable;
    protected FileProductRelTable $fileProductRelTable;
    protected FileService $fileService;

    public function setFileOfferRelTable(FileOfferRelTable $fileOfferRelTable): void
    {
        $this->fileOfferRelTable = $fileOfferRelTable;
    }

    public function setFileProductRelTable(FileProductRelTable $fileProductRelTable): void
    {
        $this->fileProductRelTable = $fileProductRelTable;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $fileOfferRelUuid
     * @return array
     */
    public function getFileOfferRel(string $fileOfferRelUuid): array
    {
        return $this->fileOfferRelTable->getFileOfferRel($fileOfferRelUuid);
    }

    public function getFileOfferRelJoined(string $fileOfferRelUuid): array
    {
        return $this->fileOfferRelTable->getFileOfferRelJoined($fileOfferRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $offerUuid
     * @param string $folderBrand
     * @return string fileOfferRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $offerUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }
        if (empty($fileOfferRelUuid = $this->fileOfferRelTable->insertFileOfferRel($fileUuid, $offerUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $fileOfferRelUuid;
    }

    public function deleteFile(string $fileOfferRelUuid): bool
    {
        $connection = $this->beginTransaction($this->fileOfferRelTable);

        $fileRel = $this->fileOfferRelTable->getFileOfferRel($fileOfferRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->fileOfferRelTable->deleteFile($fileOfferRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForOffer(string $offerUuid): array
    {
        return $this->fileOfferRelTable->getFilesForOffer($offerUuid);
    }

    public function updateFile(FileEntity $fileEntity, string $offerUuid): bool
    {
        return $this->fileOfferRelTable->updateFile($fileEntity, $offerUuid) >= 0;
    }

    public function getProductFilesForOffer(string $offerUuid): array
    {
        $select = new Select('offer_item');
        $select->columns(['product_uuid']);
        $select->where(['offer_uuid' => $offerUuid]);
        return $this->fileProductRelTable->getFilesForProductsWithSelect($select);
    }
}
