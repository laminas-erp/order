<?php

namespace Lerp\Order\Service\Offer;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Order\Entity\Offer\OfferEntity;
use Lerp\Order\Entity\Offer\ParamsOfferSearch;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Order\Unique\UniqueNumberProvider;

class OfferService extends AbstractService
{
    protected int $searchOfferCount = 0;
    protected OfferTable $offerTable;
    protected OfferItemTable $offerItemTable;
    protected UniqueNumberProvider $uniqueNumberProvider;

    public function getSearchOfferCount(): int
    {
        return $this->searchOfferCount;
    }

    public function setOfferTable(OfferTable $offerTable): void
    {
        $this->offerTable = $offerTable;
    }

    public function setOfferItemTable(OfferItemTable $offerItemTable): void
    {
        $this->offerItemTable = $offerItemTable;
    }

    public function setUniqueNumberProvider(UniqueNumberProvider $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    /**
     * @param OfferEntity $offerEntity
     * @return string
     */
    public function insertOffer(OfferEntity $offerEntity): string
    {
        $offerEntity->setOfferNoCompl($this->uniqueNumberProvider->computeGetNumberComplete(UniqueNumberProvider::TYPE_OFFER));
        $offerEntity->setOfferNo($this->uniqueNumberProvider->getNumber());
        return $this->offerTable->insertOffer($offerEntity);
    }

    public function searchOffer(ParamsOfferSearch $offerSearch): array
    {
        $offerEntity = new OfferEntity();
        if (!$offerEntity->fieldExistDatabase($offerSearch->getOrderField())) {
            $offerSearch->setOrderField('');
        }
        if (!in_array($offerSearch->getOrderDirec(), ['ASC', 'DESC'])) {
            $offerSearch->setOrderDirec('DESC');
        }
        $this->searchOfferCount = $this->offerTable->searchOffer($offerSearch, true)[0];
        return $this->offerTable->searchOffer($offerSearch);
    }

    /**
     * @param string $offerUuid
     * @return array One offer from db.view_offer.
     */
    public function getOffer(string $offerUuid): array
    {
        return $this->offerTable->getOffer($offerUuid);
    }

    /**
     * @param string $customerUuid
     * @return array From view_offer.
     */
    public function getCustomerOffers(string $customerUuid): array
    {
        return $this->offerTable->getCustomerOffers($customerUuid);
    }

    public function updateOffer(OfferEntity $offerEntity): bool
    {
        return $this->offerTable->updateOffer($offerEntity) >= 0;
    }

    /**
     * @param string $offerUuid
     * @return array Array keys: total_sum, total_sum_tax, total_sum_end
     */
    public function getOfferSummary(string $offerUuid): array
    {
        return $this->offerTable->getOfferSummary($offerUuid);
    }
}
