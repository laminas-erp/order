<?php

namespace Lerp\Order\Service\Offer;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Log\Logger;
use Lerp\Order\Entity\Offer\OfferEntity;
use Lerp\Order\Entity\Offer\OfferItemEntity;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Product\Entity\ProductEntity;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductTextService;
use Lerp\ProductCalc\Service\ProductCalcService;

class OfferItemService extends AbstractService
{
    protected OfferItemTable $offerItemTable;
    protected OfferService $offerService;
    protected ProductService $productService;
    protected ProductTextService $productTextService;
    protected ProductCalcService $productCalcService;

    public function setOfferItemTable(OfferItemTable $offerItemTable): void
    {
        $this->offerItemTable = $offerItemTable;
    }

    public function setOfferService(OfferService $offerService): void
    {
        $this->offerService = $offerService;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setProductTextService(ProductTextService $productTextService): void
    {
        $this->productTextService = $productTextService;
    }

    public function setProductCalcService(ProductCalcService $productCalcService): void
    {
        $this->productCalcService = $productCalcService;
    }

    /**
     * @param string $offerUuid
     * @return array From db.view_offer_item:
     */
    public function getOfferItemsForOffer(string $offerUuid): array
    {
        return $this->offerItemTable->getOfferItemsForOffer($offerUuid);
    }

    /**
     * Fetch product text for $lang and use it.
     * Depending on the product structure, call another function to create the order item.
     *
     * @param string $offerUuid
     * @param string $productUuid
     * @param string $lang
     * @return string
     */
    public function insertOfferItem(string $offerUuid, string $productUuid, string $lang): string
    {
        $offerData = $this->offerService->getOffer($offerUuid);
        $offerEntity = new OfferEntity();
        $productData = $this->productService->getProduct($productUuid);
        $productEntity = new ProductEntity();
        if (empty($offerData) || !is_array($offerData) || !$offerEntity->exchangeArrayFromDatabase($offerData) || empty($productData) || !is_array($productData)) {
            return '';
        }
        if (!empty($productText = $this->productTextService->getProductTextsForProductAndLang($productUuid, $lang))) {
            $productData['product_text_short'] = $productText['product_text_text_short'];
            $productData['product_text_long'] = $productText['product_text_text_long'];
        }
        if (!$productEntity->exchangeArrayFromDatabase($productData)) {
            return '';
        }
        $offerItemEntity = new OfferItemEntity();
        $offerItemEntity->setOfferItemId($this->offerItemTable->getOfferItemIdNext($offerUuid));
        $offerItemEntity->setOfferUuid($offerEntity->getUuid());
        $offerItemEntity->setProductUuid($productUuid);
        $offerItemEntity->setOfferItemTextShort($productEntity->getProductTextShort());
        $offerItemEntity->setOfferItemTextLong($productEntity->getProductTextLong());
        $offerItemEntity->setOfferItemQuantity(1);
        $offerItemEntity->setQuantityunitUuid($productEntity->getQuantityUnitUuid());
        if (
            !empty($productCalc = $this->productCalcService->getProductCalcLastForProduct($productEntity->getUuid()))
            && isset($productCalc['product_calc_price_set'])
        ) {
            $offerItemEntity->setOfferItemPrice($productCalc['product_calc_price_set']);
            $offerItemEntity->setOfferItemPriceTotal($productCalc['product_calc_price_set']);
        } else {
            $offerItemEntity->setOfferItemPrice(0);
            $offerItemEntity->setOfferItemPriceTotal(0);
        }
        $offerItemEntity->setOfferItemTaxp($productEntity->getTaxp());
        $offerItemEntity->setCostCentreId($offerEntity->getCostCentreId());
        $offerItemEntity->setOfferItemOrderPriority($this->offerItemTable->getMaxOrderPriority($offerUuid) + 10);
        /**
         * @todo muss unterschieden werden? ...haben wir Stueckliste | Maintenance in Angebot ...wenn Nein, dann kann der switch(productStructure) weg!
         */
        switch ($productEntity->getStructure()) {
            case 'simple':
            case 'standard':
            case 'group':
            case 'service':
            case 'list':
            case 'maintenance':
                return $this->insertOfferItemWithEntity($offerItemEntity);
        }
    }

    /**
     * @param OfferItemEntity $offerItemEntity
     * @return string
     */
    protected function insertOfferItemWithEntity(OfferItemEntity $offerItemEntity): string
    {
        return $this->offerItemTable->insertOfferItem($offerItemEntity);
    }

    public function updateOfferItem(string $offerItemUuid, array $storage): bool
    {
        $storage['offer_item_price_total'] = $storage['offer_item_price'] * $storage['offer_item_quantity'];
        return $this->offerItemTable->updateOfferItem($offerItemUuid, $storage) >= 0;
    }

    /**
     * Check if doc exist for orderItem must be done!
     * @param string $offerItemUuid
     * @return bool
     */
    public function deleteOfferItem(string $offerItemUuid): bool
    {
        $offerItem = $this->offerItemTable->getOfferItem($offerItemUuid);
        if (empty($offerItem)) {
            return false;
        }
        return $this->offerItemTable->deleteOfferItem($offerItemUuid) > 0;
    }
}
