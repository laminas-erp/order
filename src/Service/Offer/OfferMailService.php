<?php

namespace Lerp\Order\Service\Offer;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\OfferDocumentService;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Order\Table\Offer\Mail\MailSendOfferRelTable;

class OfferMailService extends AbstractService
{
    protected DocumentService $documentService;
    protected MailService $mailService;
    protected FileService $fileService;
    protected OfferDocumentService $offerDocumentService;
    protected DocOfferSendTable $docOfferSendTable;
    protected MailSendOfferRelTable $mailSendOfferRelTable;

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    public function setOfferDocumentService(OfferDocumentService $offerDocumentService): void
    {
        $this->offerDocumentService = $offerDocumentService;
    }

    public function setDocOfferSendTable(DocOfferSendTable $docOfferSendTable): void
    {
        $this->docOfferSendTable = $docOfferSendTable;
    }

    public function setMailSendOfferRelTable(MailSendOfferRelTable $mailSendOfferRelTable): void
    {
        $this->mailSendOfferRelTable = $mailSendOfferRelTable;
    }

    /**
     * @param MailEntity $mailEntity
     * @param string $offerUuid
     * @param array $docOfferUuids
     * @param array $fileUuids
     * @param string $userUuid
     * @return bool
     */
    public function sendDocuments(MailEntity $mailEntity, string $offerUuid, array $docOfferUuids, array $fileUuids, string $userUuid): bool
    {
        if (empty($offerUuid)) {
            return false;
        }
        $conn = $this->beginTransaction($this->docOfferSendTable);
        if (empty($mailSendUuid = $this->mailService->saveMail($mailEntity, $fileUuids, $userUuid))
            || empty($mailSendOfferRelUuid = $this->mailSendOfferRelTable->insertMailSendOfferRel($mailSendUuid, $offerUuid))) {
            $conn->rollback();
            return false;
        }
        $recipients = implode(',', $mailEntity->getRecipients());
        foreach ($fileUuids as $uuid) {
            if (empty($file = $this->fileService->getFileForOutput($uuid))) {
                continue;
            }
            $filepath = realpath($file['fqfn']);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, $file['file_mimetype']);
        }
        foreach ($docOfferUuids as $uuid) {
            if (empty($doc = $this->offerDocumentService->getDocOffer($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_OFFER
                    , $doc['offer_time_create_unix']
                    , $doc['offer_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_offer_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docOfferSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendOfferRelUuid)) {
                $conn->rollback();
                return false;
            }
        }

        if ($this->mailService->sendMail($mailEntity) < 1) {
            $conn->rollback();
            return false;
        }
        $conn->commit();
        return true;
    }

    /**
     * @param string $offerUuid
     * @return array From view_mail_send_offer
     */
    public function getMailsSendOffer(string $offerUuid): array
    {
        return $this->mailSendOfferRelTable->getMailsSendOffer($offerUuid);
    }

    /**
     * @param string $mailSendOfferRelUuid
     * @return array From view_mail_send_offer
     */
    public function getMailSendOffer(string $mailSendOfferRelUuid): array
    {
        return $this->mailSendOfferRelTable->getMailSendOffer($mailSendOfferRelUuid);
    }

}
