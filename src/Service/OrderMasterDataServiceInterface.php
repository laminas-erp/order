<?php

namespace Lerp\Order\Service;

interface OrderMasterDataServiceInterface
{
    public function createOrderMasterDataSpreadsheet(string $orderUuid): bool;

    public function getMessage(): string;
}
