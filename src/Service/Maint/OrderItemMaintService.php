<?php

namespace Lerp\Order\Service\Maint;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintFileEntity;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintFindEntity;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintGottenEntity;
use Lerp\Order\Entity\Order\OrderItemMaint\OrderItemMaintPartEntity;
use Lerp\Order\Form\Order\Maint\OrderItemMaintFileForm;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFileTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintFindTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintGottenTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintPartTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintToolTable;
use Lerp\Order\Table\Order\Maint\OrderItemMaintWorkflowTable;
use Lerp\Product\Service\ProductService;

class OrderItemMaintService extends AbstractService
{
    public static array $maintFileTypes = ['maint', 'workflow', 'finding', 'gotten'];

    protected OrderItemMaintTable $orderItemMaintTable;
    protected OrderItemMaintFileTable $orderItemMaintFileTable;
    protected OrderItemMaintFindTable $orderItemMaintFindTable;
    protected OrderItemMaintGottenTable $orderItemMaintGottenTable;
    protected OrderItemMaintPartTable $orderItemMaintPartTable;
    protected OrderItemMaintToolTable $orderItemMaintToolTable;
    protected OrderItemMaintWorkflowTable $orderItemMaintWorkflowTable;
    protected FolderTool $folderTool;
    protected string $pathMaint = '';
    protected ProductService $productService;

    public function setOrderItemMaintTable(OrderItemMaintTable $orderItemMaintTable): void
    {
        $this->orderItemMaintTable = $orderItemMaintTable;
    }

    public function setOrderItemMaintFileTable(OrderItemMaintFileTable $orderItemMaintFileTable): void
    {
        $this->orderItemMaintFileTable = $orderItemMaintFileTable;
    }

    public function setOrderItemMaintFindTable(OrderItemMaintFindTable $orderItemMaintFindTable): void
    {
        $this->orderItemMaintFindTable = $orderItemMaintFindTable;
    }

    public function setOrderItemMaintGottenTable(OrderItemMaintGottenTable $orderItemMaintGottenTable): void
    {
        $this->orderItemMaintGottenTable = $orderItemMaintGottenTable;
    }

    public function setOrderItemMaintPartTable(OrderItemMaintPartTable $orderItemMaintPartTable): void
    {
        $this->orderItemMaintPartTable = $orderItemMaintPartTable;
    }

    public function setOrderItemMaintToolTable(OrderItemMaintToolTable $orderItemMaintToolTable): void
    {
        $this->orderItemMaintToolTable = $orderItemMaintToolTable;
    }

    public function setOrderItemMaintWorkflowTable(OrderItemMaintWorkflowTable $orderItemMaintWorkflowTable): void
    {
        $this->orderItemMaintWorkflowTable = $orderItemMaintWorkflowTable;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setPathMaint(string $pathMaint): void
    {
        if (!isset($this->folderTool)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() You must set folderTool before pathMaint.');
        }
        if (empty($pathMaint = $this->folderTool->purgeDoubleDotPath($pathMaint)) || !$this->folderTool->computeFolder($pathMaint)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Con not compute folder ' . $pathMaint);
        }
        $this->pathMaint = $pathMaint;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    /**
     * @param array $storage
     * @return string
     */
    public function insertOrderItemMaint(array $storage): string
    {
        return $this->orderItemMaintTable->insertOrderItemMaint($storage);
    }

    public function insertOrderItemMaintWorkflow(array $storage): string
    {
        return $this->orderItemMaintWorkflowTable->insertOrderItemMaint($storage);
    }

    public function insertOrderItemMaintFind(OrderItemMaintFindEntity $enity): string
    {
        return $this->orderItemMaintFindTable->insertOrderItemMaintFind($enity);
    }

    public function insertOrderItemMaintFile(array $formData): string
    {
        $fileinfo = new \finfo(FILEINFO_MIME);
        $extension01 = explode(';', $fileinfo->file($formData['file_upload']['tmp_name']));
        $extension02 = explode('/', $extension01[0]);
        $extension = $extension02[1];
        if (empty($extension) || !isset(OrderItemMaintFileForm::$extMimeMap[$extension])) {
            return '';
        }
        $timeCreate = new \DateTime();
        $formData['order_item_maint_file_filename'] = $timeCreate->getTimestamp() . '_' . FilenameTool::computeSeoFriendlyFilename($formData['order_item_maint_file_label']);
        $formData['order_item_maint_file_extension'] = $extension;
        $formData['order_item_maint_file_mimetype'] = OrderItemMaintFileForm::$extMimeMap[$extension];
        $formData['order_item_maint_file_time_create'] = $timeCreate->format('Y-m-d H:i:s.u');
        $entity = new OrderItemMaintFileEntity();
        if (!$entity->exchangeArrayFromDatabase($formData) || empty($uuid = $this->orderItemMaintFileTable->insertOrderItemMaintFile($entity->getStorage()))) {
            return '';
        }
        $folder = $this->folderTool->computeDateIdFolder(
            $this->pathMaint,
            date('Y', $timeCreate->getTimestamp()),
            date('m', $timeCreate->getTimestamp()),
            $formData['order_item_maint_uuid']
        );
        $filenameCompl = $formData['order_item_maint_file_filename'] . '.' . $extension;
        if (!move_uploaded_file($formData['file_upload']['tmp_name'], $folder . '/' . $filenameCompl)) {
            $this->orderItemMaintFileTable->deleteOrderItemMaintFile($uuid);
            return '';
        }
        return $uuid;
    }

    public function computeOrderItemMaintFileFolder(OrderItemMaintFileEntity $maintFileEntity): string
    {
        if (empty($timeCreate = $maintFileEntity->getOrderItemMaintFileTimeCreateAsDateTime())) {
            return '';
        }
        return $this->folderTool->computeDateIdFolder(
            $this->pathMaint,
            date('Y', $timeCreate->getTimestamp()),
            date('m', $timeCreate->getTimestamp()),
            $maintFileEntity->getOrderItemMaintUuid()
        );
    }

    public function insertOrderItemMaintGotten(OrderItemMaintGottenEntity $enity): string
    {
        return $this->orderItemMaintGottenTable->insertWithEntity($enity);
    }

    public function insertOrderItemMaintPart(OrderItemMaintPartEntity $enity): string
    {
        $product = $this->productService->getProduct($enity->getProductUuid());
        if (empty($product)) {
            return '';
        }
        $enity->setOrderItemMaintPartQuantity(1);
        $enity->setQuantityunitUuid($product['quantityunit_uuid']);
        return $this->orderItemMaintPartTable->insertWithEntity($enity);
    }

    public function getOrderItemMaintFilesByType(string $type, string $uuid): array
    {
        return $this->orderItemMaintFileTable->getOrderItemMaintFilesByType($type, $uuid);
    }

    public function getOrderItemMaintFile(string $maintFileUuid): array
    {
        return $this->orderItemMaintFileTable->getOrderItemMaintFile($maintFileUuid);
    }

    public function getOrderItemMaintsForOrderItem(string $orderItemUuid): array
    {
        return $this->orderItemMaintTable->getOrderItemMaintsForOrderItem($orderItemUuid);
    }

    public function getOrderItemMaintWorkflowsForOrderItemMaint(string $orderItemMaintUuid): array
    {
        return $this->orderItemMaintWorkflowTable->getOrderItemMaintWorkflowsForOrderItemMaint($orderItemMaintUuid);
    }

    public function getOrderItemMaintFindsForOrderItemMaint(string $orderItemMaintUuid): array
    {
        return $this->orderItemMaintFindTable->getOrderItemMaintFindsForOrderItemMaint($orderItemMaintUuid);
    }

    public function getOrderItemMaintGottens(string $orderItemMaintUuid, string $orderItemMaintFindUuid): array
    {
        return $this->orderItemMaintGottenTable->getOrderItemMaintGottens($orderItemMaintUuid, $orderItemMaintFindUuid);
    }

    public function getOrderItemMaintParts(string $orderItemMaintUuid, string $orderItemMaintGottenUuid): array
    {
        if (!empty($orderItemMaintGottenUuid)) {
            $orderItemMaintUuid = '';
        }
        return $this->orderItemMaintPartTable->getOrderItemMaintParts($orderItemMaintUuid, $orderItemMaintGottenUuid);
    }

    public function finishOrderItemMaintWorkflow(string $maintWorkflowUuid): bool
    {
        return $this->orderItemMaintWorkflowTable->finishOrderItemMaintWorkflow($maintWorkflowUuid) >= 0;
    }

    public function deleteAllForOrderItem(string $orderItemUuid): bool
    {
        $success = true;
        if (!$this->deleteOrderItemMaintFileForOrderItem($orderItemUuid)) {
            $success = false;
        }
        if (!$this->deleteOrderItemMaintWorkflowForOrderItem($orderItemUuid)) {
            $success = false;
        }
        if (!$this->deleteOrderItemMaintFindForOrderItem($orderItemUuid)) {
            $success = false;
        }
        if (!$this->deleteOrderItemMaintGottenForOrderItem($orderItemUuid)) {
            $success = false;
        }
        if (!$this->deleteOrderItemMaintPartForOrderItem($orderItemUuid)) {
            $success = false;
        }
        if (!$this->deleteOrderItemMaintToolForOrderItem($orderItemUuid)) {
            $success = false;
        }
        if (!$this->deleteOrderItemMaintForOrderItem($orderItemUuid)) {
            $success = false;
        }
        return $success;
    }

    protected function deleteOrderItemMaintForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintTable->deleteOrderItemMaintForOrderItem($orderItemUuid) >= 0;
    }

    /**
     * @param string $orderItemUuid
     * @return bool
     * @todo die files loeschen
     */
    protected function deleteOrderItemMaintFileForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintFileTable->deleteOrderItemMaintFileForOrderItem($orderItemUuid) >= 0;
    }

    protected function deleteOrderItemMaintFindForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintFindTable->deleteOrderItemMaintFindForOrderItem($orderItemUuid) >= 0;
    }

    protected function deleteOrderItemMaintGottenForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintGottenTable->deleteOrderItemMaintGottenForOrderItem($orderItemUuid) >= 0;
    }

    protected function deleteOrderItemMaintPartForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintPartTable->deleteOrderItemMaintPartForOrderItem($orderItemUuid) >= 0;
    }

    protected function deleteOrderItemMaintToolForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintToolTable->deleteOrderItemMaintToolForOrderItem($orderItemUuid) >= 0;
    }

    protected function deleteOrderItemMaintWorkflowForOrderItem(string $orderItemUuid): bool
    {
        return $this->orderItemMaintWorkflowTable->deleteOrderItemMaintWorkflowForOrderItem($orderItemUuid) >= 0;
    }
}
