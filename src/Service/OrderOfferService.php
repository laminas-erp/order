<?php

namespace Lerp\Order\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Order\Entity\Offer\OfferEntity;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Order\Entity\Order\OrderItemEntity;
use Lerp\Order\Service\Offer\OfferItemService;
use Lerp\Order\Service\Offer\OfferService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Table\Offer\OfferTable;
use Lerp\Product\Entity\ProductEntity;
use Lerp\Product\Service\ProductService;

class OrderOfferService extends AbstractService
{
    protected OfferTable $offerTable;
    protected OfferService $offerService;
    protected OfferItemService $offerItemService;
    protected OrderService $orderService;
    protected ProductService $productService;
    protected OrderItemService $orderItemService;

    public function setOfferTable(OfferTable $offerTable): void
    {
        $this->offerTable = $offerTable;
    }

    public function setOfferService(OfferService $offerService): void
    {
        $this->offerService = $offerService;
    }

    public function setOfferItemService(OfferItemService $offerItemService): void
    {
        $this->offerItemService = $offerItemService;
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    /**
     * @param string $offerUuid
     * @param string $userUuid
     * @return string
     */
    public function createOrderFromOffer(string $offerUuid, string $userUuid): string
    {
        $offerData = $this->offerService->getOffer($offerUuid);
        $offerEntity = new OfferEntity();
        if (empty($offerData) || !is_array($offerData) || !$offerEntity->exchangeArrayFromDatabase($offerData)) {
            $this->message = 'Die Anfrage existiert nicht.';
            return '';
        }
        $order = new OrderEntity();
        $order->setCustomerUuid($offerEntity->getCustomerUuid());
        $order->setOfferUuid($offerUuid);
        $order->setCostCentreId($offerEntity->getCostCentreId());
        $order->setLocationPlaceUuid($offerEntity->getLocationPlaceUuid());
        $order->setUserUuidCreate($userUuid);

        $conn = $this->beginTransaction($this->offerTable);
        if(empty($orderUuid = $this->orderService->insertOrder($order))) {
            $this->message = 'Es gab einen Fehler beim Erstellen des Auftrags.';
            return '';
        }
        $offerItems = $this->offerItemService->getOfferItemsForOffer($offerUuid);
        if(empty($offerItems)) {
            $conn->commit();
            return $orderUuid;
        }
        $productEntity = new ProductEntity();
        $orderItem = new OrderItemEntity();
        foreach($offerItems as $offerItem) {
            $productEntity->purge();
            $orderItem->purge();
            if(!$productEntity->exchangeArrayFromDatabase($this->productService->getProduct($offerItem['product_uuid']))) {
                $conn->rollback();
                $this->message = 'Es gab einen Fehler beim Erstellen des Produkts für ein Auftrags Item.';
                return '';
            }
            $orderItem->setOrderUuid($orderUuid);
            $orderItem->setOrderItemId($offerItem['offer_item_id']);
            $orderItem->setProductUuid($productEntity->getUuid());
            $orderItem->setOrderItemTextShort($offerItem['offer_item_text_short']);
            $orderItem->setOrderItemTextLong($offerItem['offer_item_text_long']);
            $orderItem->setQuantityUnitUuid($offerItem['quantityunit_uuid']);
            $orderItem->setOrderItemQuantity($offerItem['offer_item_quantity']);
            $orderItem->setOrderItemPrice($offerItem['offer_item_price']);
            $orderItem->setOrderItemPriceTotal($offerItem['offer_item_price'] * $offerItem['offer_item_quantity']);
            $orderItem->setCostCentreId($offerItem['cost_centre_id']);
            $orderItem->setOrderItemOrderPriority($offerItem['offer_item_order_priority']);
            $orderItem->setOrderItemTaxp($offerItem['offer_item_taxp']);
            if(empty($this->orderItemService->insertOrderItemWithEntity($productEntity->getStructure(), $orderItem))) {
                $conn->rollback();
                $this->message = 'Es gab einen Fehler beim Erstellen einer Auftrags Items.';
                return '';
            }
        }
        $conn->commit();
        return $orderUuid;
    }

}
