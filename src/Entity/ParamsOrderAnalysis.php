<?php

namespace Lerp\Order\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Date;
use Lerp\Order\Table\Order\OrderAnalysisTable;

class ParamsOrderAnalysis extends ParamsBase
{
    protected Date $date;

    protected int $costCentreId;
    protected string $locationPlaceUuid;
    protected string $timeCreateFrom;
    protected string $timeCreateTo;
    protected string $timeFinishScheduleFrom;
    protected string $timeFinishScheduleTo;
    protected string $timeFinishRealFrom;
    protected string $timeFinishRealTo;
    protected int $groupBy;

    public function __construct()
    {
        parent::__construct();
        $this->date = new Date();
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->costCentreId = $costCentreId;
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        if (!$this->uuidValid->isValid($locationPlaceUuid)) {
            return;
        }
        $this->locationPlaceUuid = $locationPlaceUuid;
    }

    public function setTimeCreateFrom(string $timeCreateFrom): void
    {
        if (!$this->date->isValid($timeCreateFrom)) {
            return;
        }
        $this->timeCreateFrom = $timeCreateFrom;
    }

    public function setTimeCreateTo(string $timeCreateTo): void
    {
        if (!$this->date->isValid($timeCreateTo)) {
            return;
        }
        $this->timeCreateTo = $timeCreateTo;
    }

    public function setTimeFinishScheduleFrom(string $timeFinishScheduleFrom): void
    {
        if (!$this->date->isValid($timeFinishScheduleFrom)) {
            return;
        }
        $this->timeFinishScheduleFrom = $timeFinishScheduleFrom;
    }

    public function setTimeFinishScheduleTo(string $timeFinishScheduleTo): void
    {
        if (!$this->date->isValid($timeFinishScheduleTo)) {
            return;
        }
        $this->timeFinishScheduleTo = $timeFinishScheduleTo;
    }

    public function setTimeFinishRealFrom(string $timeFinishRealFrom): void
    {
        if (!$this->date->isValid($timeFinishRealFrom)) {
            return;
        }
        $this->timeFinishRealFrom = $timeFinishRealFrom;
    }

    public function setTimeFinishRealTo(string $timeFinishRealTo): void
    {
        if (!$this->date->isValid($timeFinishRealTo)) {
            return;
        }
        $this->timeFinishRealTo = $timeFinishRealTo;
    }

    /**
     * @param int $groupBy On of the constants from `OrderAnalysisTable::GROUP_*`.
     */
    public function setGroupBy(int $groupBy): void
    {
        if (!array_key_exists($groupBy, OrderAnalysisTable::$possibleGroups)) {
            return;
        }
        $this->groupBy = $groupBy;
    }

    public function getGroupBy(): int
    {
        return $this->groupBy;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setCostCentreId(empty($qp['cost_centre_id']) ? 0 : intval($qp['cost_centre_id']));
        $this->setLocationPlaceUuid($qp['location_place_uuid'] ?? '');
        $this->setTimeCreateFrom($qp['time_create_from'] ?? '');
        $this->setTimeCreateTo($qp['time_create_to'] ?? '');
        $this->setTimeFinishScheduleFrom($qp['time_finish_schedule_from'] ?? '');
        $this->setTimeFinishScheduleTo($qp['time_finish_schedule_to'] ?? '');
        $this->setTimeFinishRealFrom($qp['time_finish_real_from'] ?? '');
        $this->setTimeFinishRealTo($qp['time_finish_real_to'] ?? '');
        $this->setGroupBy($qp['group_by'] ?? 0);
    }

    /**
     * @param Select $select
     * @param string $orderDefault
     */
    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        parent::computeSelect($select);
        if (!empty($this->costCentreId) && $this->groupBy != OrderAnalysisTable::GROUP_COST_CENTRE) {
            $select->where(['cost_centre_id' => $this->costCentreId]);
        }
        if (!empty($this->locationPlaceUuid)) {
            $select->where(['location_place_uuid' => $this->locationPlaceUuid]);
        }

        if (!empty($this->timeCreateFrom)) {
            $select->where->greaterThanOrEqualTo('order_time_create', $this->timeCreateFrom);
        }
        if (!empty($this->timeCreateTo)) {
            $select->where->lessThanOrEqualTo('order_time_create', $this->timeCreateTo);
        }

        if (!empty($this->timeFinishScheduleFrom)) {
            $select->where->greaterThanOrEqualTo('order_time_finish_schedule', $this->timeFinishScheduleFrom);
        }
        if (!empty($this->timeFinishScheduleTo)) {
            $select->where->lessThanOrEqualTo('order_time_finish_schedule', $this->timeFinishScheduleTo);
        }

        if (!empty($this->timeFinishRealFrom)) {
            $select->where->greaterThanOrEqualTo('order_time_finish_real', $this->timeFinishRealFrom);
        }
        if (!empty($this->timeFinishRealTo)) {
            $select->where->lessThanOrEqualTo('order_time_finish_real', $this->timeFinishRealTo);
        }
        if (isset($this->groupBy) && array_key_exists($this->groupBy, OrderAnalysisTable::$possibleGroups)) {
            $columns = [
                'sum_order_item_price'        => new Expression('SUM(sum_order_item_price)'),
                'sum_order_item_price_finish' => new Expression('SUM(sum_order_item_price) FILTER ( WHERE order_time_finish_real IS NOT NULL )'),
                'sum_order_item_tax'          => new Expression('SUM(sum_order_item_tax)'),
                'sum_invoice_price'           => new Expression('SUM(sum_invoice_price)'),
                'sum_invoice_tax'             => new Expression('SUM(sum_invoice_tax)'),
                'sum_purchase_order_price'    => new Expression('SUM(sum_purchase_order_price)'),
                'count_doc_estimate'          => new Expression('SUM(count_doc_estimate)'),
                'count_doc_delivery'          => new Expression('SUM(count_doc_delivery)'),
                'count_doc_invoice'           => new Expression('SUM(count_doc_invoice)'),
                'count_order_analysis'        => new Expression('COUNT(*)'),
                'count_finish_real'           => new Expression('COUNT(*) FILTER ( WHERE order_time_finish_real IS NOT NULL )'),
            ];
            switch ($this->groupBy) {
                case OrderAnalysisTable::GROUP_MONTH:
                    $columns = ArrayUtils::merge($columns, ['order_time_create_month']);
                    $select->group('order_time_create_month');
                    $select->order('order_time_create_month ASC');
                    break;
                case OrderAnalysisTable::GROUP_YEAR:
                    $columns = ArrayUtils::merge($columns, ['order_time_create_year']);
                    $select->group('order_time_create_year');
                    $select->order('order_time_create_year ASC');
                    break;
                case OrderAnalysisTable::GROUP_COST_CENTRE:
                    $columns = ArrayUtils::merge($columns, ['cost_centre_id', 'cost_centre_label']);
                    $select->group(['cost_centre_id', 'cost_centre_label']);
                    $select->order('cost_centre_label ASC');
                    break;
            }
            $select->columns($columns);
        }
    }

}
