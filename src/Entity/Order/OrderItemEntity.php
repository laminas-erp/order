<?php

namespace Lerp\Order\Entity\Order;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemEntity extends AbstractEntity
{
    public array $mapping = [
        'order_item_uuid'               => 'order_item_uuid',
        'order_uuid'                    => 'order_uuid',
        'order_item_id'                 => 'order_item_id',
        'product_uuid'                  => 'product_uuid',
        'order_item_text_short'         => 'order_item_text_short',
        'order_item_text_long'          => 'order_item_text_long',
        'order_item_quantity'           => 'order_item_quantity',
        'quantityunit_uuid'             => 'quantityunit_uuid',
        'order_item_price'              => 'order_item_price',
        'order_item_price_total'        => 'order_item_price_total',
        'order_item_time_create'        => 'order_item_time_create',
        'order_item_time_update'        => 'order_item_time_update',
        'order_item_time_export'        => 'order_item_time_export',
        'cost_centre_id'                => 'cost_centre_id',
        'order_item_order_priority'     => 'order_item_order_priority',
        'order_item_taxp'               => 'order_item_taxp',
        // JOIN
        'order_no'                      => 'order_no',
        'order_no_compl'                => 'order_no_compl',
        'order_label'                   => 'order_label',
        'customer_uuid'                 => 'customer_uuid',
        'offer_uuid'                    => 'offer_uuid',
        'cost_centre_id_order'          => 'cost_centre_id_order',
        'location_place_uuid'           => 'location_place_uuid',
        'order_time_create'             => 'order_time_create',
        'order_time_create_unix'        => 'order_time_create_unix',
        'order_time_finish_real'        => 'order_time_finish_real',
        'order_time_request'            => 'order_time_request',
        'user_uuid_create'              => 'user_uuid_create',
        'user_uuid_update'              => 'user_uuid_update',
        'product_dirty'                 => 'product_dirty',
        'product_structure'             => 'product_structure',
        'product_origin'                => 'product_origin',
        'product_type'                  => 'product_type',
        'product_text_short'            => 'product_text_short',
        'product_text_license'          => 'product_text_license',
        'product_text_license_remark'   => 'product_text_license_remark',
        'product_no_no'                 => 'product_no_no',
        'cost_centre_label'             => 'cost_centre_label',
        'quantityunit_name'             => 'quantityunit_name',
        'quantityunit_label'            => 'quantityunit_label',
        'quantityunit_resolution'       => 'quantityunit_resolution',
        'quantityunit_resolution_group' => 'quantityunit_resolution_group',
        'count_factoryorder'            => 'count_factoryorder',
        'sum_delivery_qntty'            => 'sum_delivery_qntty',
        'sum_estimate_qntty'            => 'sum_estimate_qntty',
        'sum_invoice_qntty'             => 'sum_invoice_qntty',
        'sum_order_confirm_qntty'       => 'sum_order_confirm_qntty',
        'sum_proforma_qntty'            => 'sum_proforma_qntty',
    ];

    protected $primaryKey = 'order_item_uuid';

    public function getOrderItemUuid(): string
    {
        if (!isset($this->storage['order_item_uuid'])) {
            return '';
        }
        return $this->storage['order_item_uuid'];
    }

    public function getOrderUuid(): string
    {
        if (!isset($this->storage['order_uuid'])) {
            return '';
        }
        return $this->storage['order_uuid'];
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->storage['order_uuid'] = $orderUuid;
    }

    public function getOrderItemId(): int
    {
        if (!isset($this->storage['order_item_id'])) {
            return 0;
        }
        return $this->storage['order_item_id'];
    }

    public function setOrderItemId(int $orderItemId): void
    {
        $this->storage['order_item_id'] = $orderItemId;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getOrderItemTextShort(): string
    {
        if (!isset($this->storage['order_item_text_short'])) {
            return '';
        }
        return $this->storage['order_item_text_short'];
    }

    public function setOrderItemTextShort(string $orderItemTextShort): void
    {
        $this->storage['order_item_text_short'] = $orderItemTextShort;
    }

    public function getOrderItemTextLong(): string
    {
        if (!isset($this->storage['order_item_text_long'])) {
            return '';
        }
        return $this->storage['order_item_text_long'];
    }

    public function setOrderItemTextLong(string $orderItemTextLong): void
    {
        $this->storage['order_item_text_long'] = $orderItemTextLong;
    }

    public function getOrderItemQuantity(): float
    {
        if (!isset($this->storage['order_item_quantity'])) {
            return 0;
        }
        return $this->storage['order_item_quantity'];
    }

    public function setOrderItemQuantity(float $quantity): void
    {
        $this->storage['order_item_quantity'] = $quantity;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getOrderItemPrice(): float
    {
        if (!isset($this->storage['order_item_price'])) {
            return 0;
        }
        return $this->storage['order_item_price'];
    }

    public function setOrderItemPrice(float $orderItemPrice): void
    {
        $this->storage['order_item_price'] = $orderItemPrice;
    }

    public function getOrderItemPriceTotal(): float
    {
        if (!isset($this->storage['order_item_price_total'])) {
            return 0;
        }
        return $this->storage['order_item_price_total'];
    }

    public function setOrderItemPriceTotal(float $itemPriceTotal): void
    {
        $this->storage['order_item_price_total'] = $itemPriceTotal;
    }

    public function getOrderItemTimeCreate(): string
    {
        if (!isset($this->storage['order_item_time_create'])) {
            return '';
        }
        return $this->storage['order_item_time_create'];
    }

    public function getOrderItemTimeUpdate(): string
    {
        if (!isset($this->storage['order_item_time_update'])) {
            return '';
        }
        return $this->storage['order_item_time_update'];
    }

    public function getOrderItemTimeExport(): string
    {
        if (!isset($this->storage['order_item_time_export'])) {
            return '';
        }
        return $this->storage['order_item_time_export'];
    }

    public function setOrderItemTimeExport(string $orderItemTimeExport): void
    {
        $this->storage['order_item_time_export'] = $orderItemTimeExport;
    }

    public function getCostCentreId(): int
    {
        if (!isset($this->storage['cost_centre_id'])) {
            return 0;
        }
        return $this->storage['cost_centre_id'];
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->storage['cost_centre_id'] = $costCentreId;
    }

    public function getOrderItemOrderPriority(): int
    {
        if (!isset($this->storage['order_item_order_priority'])) {
            return 0;
        }
        return $this->storage['order_item_order_priority'];
    }

    public function setOrderItemOrderPriority(int $orderItemOrderPriority): void
    {
        $this->storage['order_item_order_priority'] = $orderItemOrderPriority;
    }

    public function getOrderItemTaxp(): int
    {
        if (!isset($this->storage['order_item_taxp'])) {
            return 0;
        }
        return $this->storage['order_item_taxp'];
    }

    public function setOrderItemTaxp(int $orderItemTaxp): void
    {
        $this->storage['order_item_taxp'] = $orderItemTaxp;
    }

    public function getOrderNo(): int
    {
        if (!isset($this->storage['order_no'])) {
            return 0;
        }
        return $this->storage['order_no'];
    }

    public function getOrderNoCompl(): string
    {
        if (!isset($this->storage['order_no_compl'])) {
            return 0;
        }
        return $this->storage['order_no_compl'];
    }

    public function getOrderLabel(): string
    {
        if (!isset($this->storage['order_label'])) {
            return '';
        }
        return $this->storage['order_label'];
    }

    public function getCustomerUuid(): string
    {
        if (!isset($this->storage['customer_uuid'])) {
            return '';
        }
        return $this->storage['customer_uuid'];
    }

    public function getOfferUuid(): string
    {
        if (!isset($this->storage['offer_uuid'])) {
            return '';
        }
        return $this->storage['offer_uuid'];
    }

    public function getCostCentreIdOrder(): int
    {
        if (!isset($this->storage['cost_centre_id_order'])) {
            return 0;
        }
        return $this->storage['cost_centre_id_order'];
    }

    public function getLocationPlaceUuid(): int
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return 0;
        }
        return $this->storage['location_place_uuid'];
    }

    public function getOrderTimeCreate(): string
    {
        if (!isset($this->storage['order_time_create'])) {
            return '';
        }
        return $this->storage['order_time_create'];
    }

    public function getOrderTimeCreateUnix(): int
    {
        if (!isset($this->storage['order_time_create_unix'])) {
            return 0;
        }
        return $this->storage['order_time_create_unix'];
    }

    public function getOrderTimeFinishReal(): string
    {
        if (!isset($this->storage['order_time_finish_real'])) {
            return '';
        }
        return $this->storage['order_time_finish_real'];
    }

    public function getOrderTimeRequest(): string
    {
        if (!isset($this->storage['order_time_request'])) {
            return '';
        }
        return $this->storage['order_time_request'];
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function getProductDirty(): bool
    {
        if (!isset($this->storage['product_dirty'])) {
            return '';
        }
        return $this->storage['product_dirty'];
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function getProductOrigin(): string
    {
        if (!isset($this->storage['product_origin'])) {
            return '';
        }
        return $this->storage['product_origin'];
    }

    public function getProductType(): string
    {
        if (!isset($this->storage['product_type'])) {
            return '';
        }
        return $this->storage['product_type'];
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function getProductTextLicense(): string
    {
        if (!isset($this->storage['product_text_license'])) {
            return '';
        }
        return $this->storage['product_text_license'];
    }

    public function getProductTextLicenseRemark(): string
    {
        if (!isset($this->storage['product_text_license_remark'])) {
            return '';
        }
        return $this->storage['product_text_license_remark'];
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function getCostCentreLabel(): string
    {
        if (!isset($this->storage['cost_centre_label'])) {
            return '';
        }
        return $this->storage['cost_centre_label'];
    }

    public function getQuantityunitName(): string
    {
        if (!isset($this->storage['quantityunit_name'])) {
            return '';
        }
        return $this->storage['quantityunit_name'];
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function getQuantityunitResolution(): float
    {
        if (!isset($this->storage['quantityunit_resolution'])) {
            return 0;
        }
        return $this->storage['quantityunit_resolution'];
    }

    public function setQuantityunitResolution(float $quantityunitResolution): void
    {
        $this->storage['quantityunit_resolution'] = $quantityunitResolution;
    }

    public function getQuantityunitResolutionGroup(): string
    {
        if (!isset($this->storage['quantityunit_resolution_group'])) {
            return '';
        }
        return $this->storage['quantityunit_resolution_group'];
    }

    public function setQuantityunitResolutionGroup(string $quantityunitResolutionGroup): void
    {
        $this->storage['quantityunit_resolution_group'] = $quantityunitResolutionGroup;
    }

    public function getCountFactoryorder(): int
    {
        if (!isset($this->storage['count_factoryorder'])) {
            return 0;
        }
        return $this->storage['count_factoryorder'];
    }

    public function getSumDeliveryQntty(): float
    {
        if (!isset($this->storage['sum_delivery_qntty'])) {
            return 0;
        }
        return $this->storage['sum_delivery_qntty'];
    }

    public function getSumEstimateQntty(): float
    {
        if (!isset($this->storage['sum_estimate_qntty'])) {
            return 0;
        }
        return $this->storage['sum_estimate_qntty'];
    }

    public function getSumInvoiceQntty(): float
    {
        if (!isset($this->storage['sum_invoice_qntty'])) {
            return 0;
        }
        return $this->storage['sum_invoice_qntty'];
    }

    public function getSumOrderConfirmQntty(): float
    {
        if (!isset($this->storage['sum_order_confirm_qntty'])) {
            return 0;
        }
        return $this->storage['sum_order_confirm_qntty'];
    }

    public function getSumProformaQntty(): float
    {
        if (!isset($this->storage['sum_proforma_qntty'])) {
            return 0;
        }
        return $this->storage['sum_proforma_qntty'];
    }
}
