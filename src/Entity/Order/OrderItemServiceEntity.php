<?php

namespace Lerp\Order\Entity\Order;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemServiceEntity extends AbstractEntity
{
    public array $mapping = [
        'order_item_service_uuid'       => 'order_item_service_uuid',
        'order_item_uuid'               => 'order_item_uuid',
        'order_item_service_no_extern'  => 'order_item_service_no_extern',
        'order_item_service_text_short' => 'order_item_service_text_short',
        'order_item_service_text_long'  => 'order_item_service_text_long',
    ];

    protected $primaryKey = 'order_item_service_uuid';

    public function getOrderItemServiceUuid(): string
    {
        if (!isset($this->storage['order_item_service_uuid'])) {
            return '';
        }
        return $this->storage['order_item_service_uuid'];
    }

    public function setOrderItemServiceUuid(string $orderItemServiceUuid): void
    {
        $this->storage['order_item_service_uuid'] = $orderItemServiceUuid;
    }

    public function getOrderItemUuid(): string
    {
        if (!isset($this->storage['order_item_uuid'])) {
            return '';
        }
        return $this->storage['order_item_uuid'];
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        $this->storage['order_item_uuid'] = $orderItemUuid;
    }

    public function getOrderItemServiceNoExtern(): string
    {
        if (!isset($this->storage['order_item_service_no_extern'])) {
            return '';
        }
        return $this->storage['order_item_service_no_extern'];
    }

    public function setOrderItemServiceNoExtern(string $orderItemServiceNoExtern): void
    {
        $this->storage['order_item_service_no_extern'] = $orderItemServiceNoExtern;
    }

    public function getOrderItemServiceTextShort(): string
    {
        if (!isset($this->storage['order_item_service_text_short'])) {
            return '';
        }
        return $this->storage['order_item_service_text_short'];
    }

    public function setOrderItemServiceTextShort(string $orderItemServiceTextShort): void
    {
        $this->storage['order_item_service_text_short'] = $orderItemServiceTextShort;
    }

    public function getOrderItemServiceTextLong(): string
    {
        if (!isset($this->storage['order_item_service_text_long'])) {
            return '';
        }
        return $this->storage['order_item_service_text_long'];
    }

    public function setOrderItemServiceTextLong(string $orderItemServiceTextLong): void
    {
        $this->storage['order_item_service_text_long'] = $orderItemServiceTextLong;
    }
}
