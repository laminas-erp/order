<?php

namespace Lerp\Order\Entity\Order;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderEntity extends AbstractEntity
{
    public array $mapping = [
        'order_uuid'                  => 'order_uuid',
        'order_no'                    => 'order_no',
        'order_no_compl'              => 'order_no_compl',
        'order_label'                 => 'order_label',
        'order_label_yours'           => 'order_label_yours',
        'customer_uuid'               => 'customer_uuid',
        'offer_uuid'                  => 'offer_uuid',
        'cost_centre_id'              => 'cost_centre_id',
        'location_place_uuid'         => 'location_place_uuid',
        'order_time_create'           => 'order_time_create',
        'order_time_create_unix'      => 'order_time_create_unix',
        'order_time_update'           => 'order_time_update',
        'order_time_export'           => 'order_time_export',
        'order_time_finish_schedule'  => 'order_time_finish_schedule',
        'order_time_finish_real'      => 'order_time_finish_real',
        'order_time_request'          => 'order_time_request',
        'user_uuid_create'            => 'user_uuid_create',
        'user_uuid_update'            => 'user_uuid_update',
        'user_uuid_contact'           => 'user_uuid_contact',
        'order_lang_iso'              => 'order_lang_iso',
        'address_customer_rel_uuid'   => 'address_customer_rel_uuid',
        'contact_customer_rel_uuid'   => 'contact_customer_rel_uuid',
        'order_aircraft_licenseplate' => 'order_aircraft_licenseplate',
        'order_comment'               => 'order_comment',
        'project_type_id'             => 'project_type_id',
        'order_finish_unlock'         => 'order_finish_unlock',
        'customer_no'                 => 'customer_no',
        'customer_name'               => 'customer_name',
        'customer_lang_iso'           => 'customer_lang_iso',
        'address_uuid'                => 'address_uuid',
        'address_label'               => 'address_label',
        'address_name'                => 'address_name',
        'address_name2'               => 'address_name2',
        'address_street'              => 'address_street',
        'address_street_no'           => 'address_street_no',
        'address_zip'                 => 'address_zip',
        'address_city'                => 'address_city',
        'country_id'                  => 'country_id',
        'country_iso'                 => 'country_iso',
        'country_name'                => 'country_name',
        'contact_uuid'                => 'contact_uuid',
        'contact_label'               => 'contact_label',
        'contact_salut'               => 'contact_salut',
        'contact_name'                => 'contact_name',
        'contact_dept'                => 'contact_dept',
        'contact_tel'                 => 'contact_tel',
        'contact_fax'                 => 'contact_fax',
        'contact_mobile'              => 'contact_mobile',
        'contact_email'               => 'contact_email',
        'contact_www'                 => 'contact_www',
        'contact_desc'                => 'contact_desc',
        'offer_no'                    => 'offer_no',
        'offer_time_create'           => 'offer_time_create',
        'cost_centre_code'            => 'cost_centre_code',
        'cost_centre_label'           => 'cost_centre_label',
        'location_place_name'         => 'location_place_name',
        'location_place_label'        => 'location_place_label',
        'user_login'                  => 'user_login',
        'user_email'                  => 'user_email',
        'user_details_name_first'     => 'user_details_name_first',
        'user_details_name_last'      => 'user_details_name_last',
        'count_doc_order_confirm'     => 'count_doc_order_confirm',
        'count_doc_delivery'          => 'count_doc_delivery',
        'count_doc_invoice'           => 'count_doc_invoice',
        'count_factoryorder'          => 'count_factoryorder',
        'is_time_finish_dirty'        => 'is_time_finish_dirty',
    ];

    protected $primaryKey = 'order_uuid';

    public function getOrderUuid(): string
    {
        if (!isset($this->storage['order_uuid'])) {
            return '';
        }
        return $this->storage['order_uuid'];
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->storage['order_uuid'] = $orderUuid;
    }

    public function getOrderNo(): int
    {
        if (!isset($this->storage['order_no'])) {
            return 0;
        }
        return $this->storage['order_no'];
    }

    public function setOrderNo(int $orderNo): void
    {
        $this->storage['order_no'] = $orderNo;
    }

    public function getOrderNoCompl(): string
    {
        if (!isset($this->storage['order_no_compl'])) {
            return '';
        }
        return $this->storage['order_no_compl'];
    }

    public function setOrderNoCompl(string $orderNoCompl): void
    {
        $this->storage['order_no_compl'] = $orderNoCompl;
    }

    public function getOrderLabel(): string
    {
        if (!isset($this->storage['order_label'])) {
            return '';
        }
        return $this->storage['order_label'];
    }

    public function setOrderLabel(string $orderLabel): void
    {
        $this->storage['order_label'] = $orderLabel;
    }

    public function getOrderLabelYours(): string
    {
        if (!isset($this->storage['order_label_yours'])) {
            return '';
        }
        return $this->storage['order_label_yours'];
    }

    public function setOrderLabelYours(string $orderLabelYours): void
    {
        $this->storage['order_label_yours'] = $orderLabelYours;
    }

    public function getCustomerUuid(): string
    {
        if (!isset($this->storage['customer_uuid'])) {
            return '';
        }
        return $this->storage['customer_uuid'];
    }

    public function setCustomerUuid(string $customerUuid): void
    {
        $this->storage['customer_uuid'] = $customerUuid;
    }

    public function getOfferUuid(): string
    {
        if (!isset($this->storage['offer_uuid'])) {
            return '';
        }
        return $this->storage['offer_uuid'];
    }

    public function setOfferUuid(string $offerUuid): void
    {
        $this->storage['offer_uuid'] = $offerUuid;
    }

    public function getCostCentreId(): int
    {
        if (!isset($this->storage['cost_centre_id'])) {
            return 0;
        }
        return $this->storage['cost_centre_id'];
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->storage['cost_centre_id'] = $costCentreId;
    }

    public function getLocationPlaceUuid(): string
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return '';
        }
        return $this->storage['location_place_uuid'];
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->storage['location_place_uuid'] = $locationPlaceUuid;
    }

    public function getOrderTimeCreate(): string
    {
        if (!isset($this->storage['order_time_create'])) {
            return '';
        }
        return $this->storage['order_time_create'];
    }

    public function setOrderTimeCreate(string $orderTimeCreate): void
    {
        $this->storage['order_time_create'] = $orderTimeCreate;
    }

    public function getOrderTimeCreateUnix()
    {
        if (!isset($this->storage['order_time_create_unix'])) {
            return '';
        }
        return $this->storage['order_time_create_unix'];
    }

    public function setOrderTimeCreateUnix($orderTimeCreateUnix): void
    {
        $this->storage['order_time_create_unix'] = $orderTimeCreateUnix;
    }

    public function getOrderTimeUpdate(): string
    {
        if (!isset($this->storage['order_time_update'])) {
            return '';
        }
        return $this->storage['order_time_update'];
    }

    public function setOrderTimeUpdate(string $orderTimeUpdate): void
    {
        $this->storage['order_time_update'] = $orderTimeUpdate;
    }

    public function getOrderTimeExport(): string
    {
        if (!isset($this->storage['order_time_export'])) {
            return '';
        }
        return $this->storage['order_time_export'];
    }

    public function setOrderTimeExport(string $orderTimeExport): void
    {
        $this->storage['order_time_export'] = $orderTimeExport;
    }

    public function getOrderTimeFinishSchedule(): string
    {
        if (!isset($this->storage['order_time_finish_schedule'])) {
            return '';
        }
        return $this->storage['order_time_finish_schedule'];
    }

    public function setOrderTimeFinishSchedule(string $orderTimeFinishSchedule): void
    {
        $this->storage['order_time_finish_schedule'] = $orderTimeFinishSchedule;
    }

    public function getOrderTimeFinishReal(): string
    {
        if (!isset($this->storage['order_time_finish_real'])) {
            return '';
        }
        return $this->storage['order_time_finish_real'];
    }

    public function setOrderTimeFinishReal(string $orderTimeFinishReal): void
    {
        $this->storage['order_time_finish_real'] = $orderTimeFinishReal;
    }

    public function getOrderTimeRequest(): string
    {
        if (!isset($this->storage['order_time_request'])) {
            return '';
        }
        return $this->storage['order_time_request'];
    }

    public function setOrderTimeRequest(string $orderTimeRequest): void
    {
        $this->storage['order_time_request'] = $orderTimeRequest;
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function setUserUuidUpdate(string $userUuidUpdate): void
    {
        $this->storage['user_uuid_update'] = $userUuidUpdate;
    }

    public function getUserUuidContact(): string
    {
        if (!isset($this->storage['user_uuid_contact'])) {
            return '';
        }
        return $this->storage['user_uuid_contact'];
    }

    public function setUserUuidContact(string $userUuidContact): void
    {
        $this->storage['user_uuid_contact'] = $userUuidContact;
    }

    public function getOrderLangIso(): string
    {
        if (!isset($this->storage['order_lang_iso'])) {
            return '';
        }
        return $this->storage['order_lang_iso'];
    }

    public function setOrderLangIso(string $orderLangIso): void
    {
        $this->storage['order_lang_iso'] = $orderLangIso;
    }

    public function getAddressCustomerRelUuid(): string
    {
        if (!isset($this->storage['address_customer_rel_uuid'])) {
            return '';
        }
        return $this->storage['address_customer_rel_uuid'];
    }

    public function setAddressCustomerRelUuid(string $addressCustomerRelUuid): void
    {
        $this->storage['address_customer_rel_uuid'] = $addressCustomerRelUuid;
    }

    public function getContactCustomerRelUuid(): string
    {
        if (!isset($this->storage['contact_customer_rel_uuid'])) {
            return '';
        }
        return $this->storage['contact_customer_rel_uuid'];
    }

    public function setContactCustomerRelUuid(string $contactCustomerRelUuid): void
    {
        $this->storage['contact_customer_rel_uuid'] = $contactCustomerRelUuid;
    }

    public function getOrderAircraftLicenseplate(): string
    {
        if (!isset($this->storage['order_aircraft_licenseplate'])) {
            return '';
        }
        return $this->storage['order_aircraft_licenseplate'];
    }

    public function setOrderAircraftLicenseplate(string $orderAircraftLicenseplate): void
    {
        $this->storage['order_aircraft_licenseplate'] = $orderAircraftLicenseplate;
    }

    public function getOrderComment(): string
    {
        if (!isset($this->storage['order_comment'])) {
            return '';
        }
        return $this->storage['order_comment'];
    }

    public function setOrderComment(string $orderComment): void
    {
        $this->storage['order_comment'] = $orderComment;
    }

    public function addOrderComment(string $orderComment): void
    {
        if (!isset($this->storage['order_comment'])) {
            $this->storage['order_comment'] = '';
        }
        $this->storage['order_comment'] .= $orderComment;
    }

    public function getProjectTypeId(): int
    {
        if (!isset($this->storage['project_type_id'])) {
            return 0;
        }
        return $this->storage['project_type_id'];
    }

    public function setProjectTypeId(int $projectTypeId): void
    {
        $this->storage['project_type_id'] = $projectTypeId;
    }

    public function getOrderFinishUnlock(): bool
    {
        if (!isset($this->storage['order_finish_unlock'])) {
            return false;
        }
        return $this->storage['order_finish_unlock'];
    }

    public function setOrderFinishUnlock(bool $orderFinishUnlock): void
    {
        $this->storage['order_finish_unlock'] = $orderFinishUnlock;
    }

    public function getCustomerNo(): int
    {
        if (!isset($this->storage['customer_no'])) {
            return 0;
        }
        return $this->storage['customer_no'];
    }

    public function setCustomerNo(int $customerNo): void
    {
        $this->storage['customer_no'] = $customerNo;
    }

    public function getCustomerName(): string
    {
        if (!isset($this->storage['customer_name'])) {
            return '';
        }
        return $this->storage['customer_name'];
    }

    public function setCustomerName(string $customerName): void
    {
        $this->storage['customer_name'] = $customerName;
    }

    public function getCustomerLangIso(): string
    {
        if (!isset($this->storage['customer_lang_iso'])) {
            return '';
        }
        return $this->storage['customer_lang_iso'];
    }

    public function setCustomerLangIso(string $customerLangIso): void
    {
        $this->storage['customer_lang_iso'] = $customerLangIso;
    }

    public function getAddressUuid(): string
    {
        if (!isset($this->storage['address_uuid'])) {
            return '';
        }
        return $this->storage['address_uuid'];
    }

    public function setAddressUuid(string $addressUuid): void
    {
        $this->storage['address_uuid'] = $addressUuid;
    }

    public function getAddressLabel(): string
    {
        if (!isset($this->storage['address_label'])) {
            return '';
        }
        return $this->storage['address_label'];
    }

    public function setAddressLabel(string $addressLabel): void
    {
        $this->storage['address_label'] = $addressLabel;
    }

    public function getAddressName(): string
    {
        if (!isset($this->storage['address_name'])) {
            return '';
        }
        return $this->storage['address_name'];
    }

    public function setAddressName(string $addressName): void
    {
        $this->storage['address_name'] = $addressName;
    }

    public function getAddressName2(): string
    {
        if (!isset($this->storage['address_name2'])) {
            return '';
        }
        return $this->storage['address_name2'];
    }

    public function setAddressName2(string $addressName2): void
    {
        $this->storage['address_name2'] = $addressName2;
    }

    public function getAddressStreet(): string
    {
        if (!isset($this->storage['address_street'])) {
            return '';
        }
        return $this->storage['address_street'];
    }

    public function setAddressStreet(string $addressStreet): void
    {
        $this->storage['address_street'] = $addressStreet;
    }

    public function getAddressStreetNo(): string
    {
        if (!isset($this->storage['address_street_no'])) {
            return '';
        }
        return $this->storage['address_street_no'];
    }

    public function setAddressStreetNo(string $addressStreetNo): void
    {
        $this->storage['address_street_no'] = $addressStreetNo;
    }

    public function getAddressZip(): string
    {
        if (!isset($this->storage['address_zip'])) {
            return '';
        }
        return $this->storage['address_zip'];
    }

    public function setAddressZip(string $addressZip): void
    {
        $this->storage['address_zip'] = $addressZip;
    }

    public function getAddressCity(): string
    {
        if (!isset($this->storage['address_city'])) {
            return '';
        }
        return $this->storage['address_city'];
    }

    public function setAddressCity(string $addressCity): void
    {
        $this->storage['address_city'] = $addressCity;
    }

    public function getCountryId(): int
    {
        if (!isset($this->storage['country_id'])) {
            return 0;
        }
        return $this->storage['country_id'];
    }

    public function setCountryId(int $countryId): void
    {
        $this->storage['country_id'] = $countryId;
    }

    public function getCountryIso(): string
    {
        if (!isset($this->storage['country_iso'])) {
            return '';
        }
        return $this->storage['country_iso'];
    }

    public function setCountryIso(string $countryIso): void
    {
        $this->storage['country_iso'] = $countryIso;
    }

    public function getCountryName(): string
    {
        if (!isset($this->storage['country_name'])) {
            return '';
        }
        return $this->storage['country_name'];
    }

    public function setCountryName(string $countryName): void
    {
        $this->storage['country_name'] = $countryName;
    }

    public function getContactUuid(): string
    {
        if (!isset($this->storage['contact_uuid'])) {
            return '';
        }
        return $this->storage['contact_uuid'];
    }

    public function setContactUuid(string $contactUuid): void
    {
        $this->storage['contact_uuid'] = $contactUuid;
    }

    public function getContactLabel(): string
    {
        if (!isset($this->storage['contact_label'])) {
            return '';
        }
        return $this->storage['contact_label'];
    }

    public function setContactLabel(string $contactLabel): void
    {
        $this->storage['contact_label'] = $contactLabel;
    }

    public function getContactSalut(): string
    {
        if (!isset($this->storage['contact_salut'])) {
            return '';
        }
        return $this->storage['contact_salut'];
    }

    public function setContactSalut(string $contactSalut): void
    {
        $this->storage['contact_salut'] = $contactSalut;
    }

    public function getContactName(): string
    {
        if (!isset($this->storage['contact_name'])) {
            return '';
        }
        return $this->storage['contact_name'];
    }

    public function setContactName(string $contactName): void
    {
        $this->storage['contact_name'] = $contactName;
    }

    public function getContactDept(): string
    {
        if (!isset($this->storage['contact_dept'])) {
            return '';
        }
        return $this->storage['contact_dept'];
    }

    public function setContactDept(string $contactDept): void
    {
        $this->storage['contact_dept'] = $contactDept;
    }

    public function getContactTel(): string
    {
        if (!isset($this->storage['contact_tel'])) {
            return '';
        }
        return $this->storage['contact_tel'];
    }

    public function setContactTel(string $contactTel): void
    {
        $this->storage['contact_tel'] = $contactTel;
    }

    public function getContactFax(): string
    {
        if (!isset($this->storage['contact_fax'])) {
            return '';
        }
        return $this->storage['contact_fax'];
    }

    public function setContactFax(string $contactFax): void
    {
        $this->storage['contact_fax'] = $contactFax;
    }

    public function getContactMobile(): string
    {
        if (!isset($this->storage['contact_mobile'])) {
            return '';
        }
        return $this->storage['contact_mobile'];
    }

    public function setContactMobile(string $contactMobile): void
    {
        $this->storage['contact_mobile'] = $contactMobile;
    }

    public function getContactEmail(): string
    {
        if (!isset($this->storage['contact_email'])) {
            return '';
        }
        return $this->storage['contact_email'];
    }

    public function setContactEmail(string $contactEmail): void
    {
        $this->storage['contact_email'] = $contactEmail;
    }

    public function getContactWww(): string
    {
        if (!isset($this->storage['contact_www'])) {
            return '';
        }
        return $this->storage['contact_www'];
    }

    public function setContactWww(string $contactWww): void
    {
        $this->storage['contact_www'] = $contactWww;
    }

    public function getContactDesc(): string
    {
        if (!isset($this->storage['contact_desc'])) {
            return '';
        }
        return $this->storage['contact_desc'];
    }

    public function setContactDesc(string $contactDesc): void
    {
        $this->storage['contact_desc'] = $contactDesc;
    }

    public function getOfferNo(): int
    {
        if (!isset($this->storage['offer_no'])) {
            return 0;
        }
        return $this->storage['offer_no'];
    }

    public function setOfferNo(int $offerNo): void
    {
        $this->storage['offer_no'] = $offerNo;
    }

    public function getOfferTimeCreate(): string
    {
        if (!isset($this->storage['offer_time_create'])) {
            return '';
        }
        return $this->storage['offer_time_create'];
    }

    public function setOfferTimeCreate(string $offerTimeCreate): void
    {
        $this->storage['offer_time_create'] = $offerTimeCreate;
    }

    public function getCostCentreCode(): int
    {
        if (!isset($this->storage['cost_centre_code'])) {
            return 0;
        }
        return $this->storage['cost_centre_code'];
    }

    public function setCostCentreCode(int $costCentreCode): void
    {
        $this->storage['cost_centre_code'] = $costCentreCode;
    }

    public function getCostCentreLabel(): string
    {
        if (!isset($this->storage['cost_centre_label'])) {
            return '';
        }
        return $this->storage['cost_centre_label'];
    }

    public function setCostCentreLabel(string $costCentreLabel): void
    {
        $this->storage['cost_centre_label'] = $costCentreLabel;
    }

    public function getLocationPlaceName(): string
    {
        if (!isset($this->storage['location_place_name'])) {
            return '';
        }
        return $this->storage['location_place_name'];
    }

    public function setLocationPlaceName(string $locationPlaceName): void
    {
        $this->storage['location_place_name'] = $locationPlaceName;
    }

    public function getLocationPlaceLabel(): string
    {
        if (!isset($this->storage['location_place_label'])) {
            return '';
        }
        return $this->storage['location_place_label'];
    }

    public function setLocationPlaceLabel(string $locationPlaceLabel): void
    {
        $this->storage['location_place_label'] = $locationPlaceLabel;
    }

    public function getUserLogin(): string
    {
        if (!isset($this->storage['user_login'])) {
            return '';
        }
        return $this->storage['user_login'];
    }

    public function setUserLogin(string $userLogin): void
    {
        $this->storage['user_login'] = $userLogin;
    }

    public function getUserEmail(): string
    {
        if (!isset($this->storage['user_email'])) {
            return '';
        }
        return $this->storage['user_email'];
    }

    public function setUserEmail(string $userEmail): void
    {
        $this->storage['user_email'] = $userEmail;
    }

    public function getUserDetailsNameFirst(): string
    {
        if (!isset($this->storage['user_details_name_first'])) {
            return '';
        }
        return $this->storage['user_details_name_first'];
    }

    public function setUserDetailsNameFirst(string $userDetailsNameFirst): void
    {
        $this->storage['user_details_name_first'] = $userDetailsNameFirst;
    }

    public function getUserDetailsNameLast(): string
    {
        if (!isset($this->storage['user_details_name_last'])) {
            return '';
        }
        return $this->storage['user_details_name_last'];
    }

    public function setUserDetailsNameLast(string $userDetailsNameLast): void
    {
        $this->storage['user_details_name_last'] = $userDetailsNameLast;
    }

    public function getCountDocOrderConfirm(): int
    {
        if (!isset($this->storage['count_doc_order_confirm'])) {
            return 0;
        }
        return $this->storage['count_doc_order_confirm'];
    }

    public function setCountDocOrderConfirm(int $countDocOrderConfirm): void
    {
        $this->storage['count_doc_order_confirm'] = $countDocOrderConfirm;
    }

    public function getCountDocDelivery(): int
    {
        if (!isset($this->storage['count_doc_delivery'])) {
            return 0;
        }
        return $this->storage['count_doc_delivery'];
    }

    public function setCountDocDelivery(int $countDocDelivery): void
    {
        $this->storage['count_doc_delivery'] = $countDocDelivery;
    }

    public function getCountDocInvoice(): int
    {
        if (!isset($this->storage['count_doc_invoice'])) {
            return 0;
        }
        return $this->storage['count_doc_invoice'];
    }

    public function setCountDocInvoice(int $countDocInvoice): void
    {
        $this->storage['count_doc_invoice'] = $countDocInvoice;
    }

    public function getCountFactoryorder(): int
    {
        if (!isset($this->storage['count_factoryorder'])) {
            return 0;
        }
        return $this->storage['count_factoryorder'];
    }

    public function setCountFactoryorder(int $countFactoryorder): void
    {
        $this->storage['count_factoryorder'] = $countFactoryorder;
    }

    public function getIsTimeFinishDirty(): bool
    {
        if (!isset($this->storage['is_time_finish_dirty'])) {
            return '';
        }
        return $this->storage['is_time_finish_dirty'];
    }

    public function setIsTimeFinishDirty(bool $isTimeFinishDirty): void
    {
        $this->storage['is_time_finish_dirty'] = $isTimeFinishDirty;
    }
}
