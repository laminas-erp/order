<?php

namespace Lerp\Order\Entity\Order\OrderItemMaint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemMaintGottenEntity extends AbstractEntity
{
    protected $primaryKey = 'order_item_maint_gotten_uuid';

    public array $mapping = [
        'order_item_maint_gotten_uuid' => 'order_item_maint_gotten_uuid',
        'order_item_maint_uuid' => 'order_item_maint_uuid',
        'order_item_maint_find_uuid' => 'order_item_maint_find_uuid',
        'order_item_maint_gotten_time_create' => 'order_item_maint_gotten_time_create',
        'order_item_maint_gotten_label' => 'order_item_maint_gotten_label',
        'order_item_maint_gotten_desc' => 'order_item_maint_gotten_desc',
    ];

    /**
     * @return string
     */
    public function getOrderItemMaintGottenUuid()
    {
        if (!isset($this->storage['order_item_maint_gotten_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_gotten_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintUuid()
    {
        if (!isset($this->storage['order_item_maint_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintFindUuid()
    {
        if (!isset($this->storage['order_item_maint_find_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_find_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintGottenTimeCreate()
    {
        if (!isset($this->storage['order_item_maint_gotten_time_create'])) {
            return '';
        }
        return $this->storage['order_item_maint_gotten_time_create'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintGottenLabel()
    {
        if (!isset($this->storage['order_item_maint_gotten_label'])) {
            return '';
        }
        return $this->storage['order_item_maint_gotten_label'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintGottenDesc()
    {
        if (!isset($this->storage['order_item_maint_gotten_desc'])) {
            return '';
        }
        return $this->storage['order_item_maint_gotten_desc'];
    }
}
