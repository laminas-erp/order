<?php

namespace Lerp\Order\Entity\Order\OrderItemMaint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemMaintFindEntity extends AbstractEntity
{

    protected $primaryKey = 'order_item_maint_find_uuid';

    public array $mapping = [
        'order_item_maint_find_uuid' => 'order_item_maint_find_uuid',
        'order_item_maint_uuid' => 'order_item_maint_uuid',
        'order_item_maint_find_time_create' => 'order_item_maint_find_time_create',
        'order_item_maint_find_label' => 'order_item_maint_find_label',
        'order_item_maint_find_desc' => 'order_item_maint_find_desc',
    ];

    /**
     * @return string
     */
    public function getOrderItemMaintFindUuid(): string
    {
        if (!isset($this->storage['order_item_maint_find_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_find_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintUuid(): string
    {
        if (!isset($this->storage['order_item_maint_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintFindTimeCreate(): string
    {
        if (!isset($this->storage['order_item_maint_find_time_create'])) {
            return '';
        }
        return $this->storage['order_item_maint_find_time_create'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintFindLabel(): string
    {
        if (!isset($this->storage['order_item_maint_find_label'])) {
            return '';
        }
        return $this->storage['order_item_maint_find_label'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintFindDesc(): string
    {
        if (!isset($this->storage['order_item_maint_find_desc'])) {
            return '';
        }
        return $this->storage['order_item_maint_find_desc'];
    }
}
