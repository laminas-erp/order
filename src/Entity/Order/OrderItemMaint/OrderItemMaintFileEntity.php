<?php

namespace Lerp\Order\Entity\Order\OrderItemMaint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemMaintFileEntity extends AbstractEntity
{
    protected $primaryKey = 'order_item_maint_file_uuid';

    public array $mapping = [
        'order_item_maint_file_uuid' => 'order_item_maint_file_uuid',
        'order_item_maint_uuid' => 'order_item_maint_uuid',
        'order_item_maint_find_uuid' => 'order_item_maint_find_uuid',
        'order_item_maint_gotten_uuid' => 'order_item_maint_gotten_uuid',
        'order_item_maint_workflow_uuid' => 'order_item_maint_workflow_uuid',
        'order_item_maint_file_label' => 'order_item_maint_file_label',
        'order_item_maint_file_desc' => 'order_item_maint_file_desc',
        'order_item_maint_file_filename' => 'order_item_maint_file_filename',
        'order_item_maint_file_extension' => 'order_item_maint_file_extension',
        'order_item_maint_file_mimetype' => 'order_item_maint_file_mimetype',
        'order_item_maint_file_time_create' => 'order_item_maint_file_time_create',
    ];

    public function getOrderItemMaintUuid(): string
    {
        if (!isset($this->storage['order_item_maint_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_uuid'];
    }

    public function getOrderItemMaintFileLabel(): string
    {
        if (!isset($this->storage['order_item_maint_file_label'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_label'];
    }

    public function getOrderItemMaintFileDesc(): string
    {
        if (!isset($this->storage['order_item_maint_file_desc'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_desc'];
    }

    public function getOrderItemMaintFileFilename(): string
    {
        if (!isset($this->storage['order_item_maint_file_filename'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_filename'];
    }

    public function getOrderItemMaintFileExtension(): string
    {
        if (!isset($this->storage['order_item_maint_file_extension'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_extension'];
    }

    public function getOrderItemMaintFileMimetype(): string
    {
        if (!isset($this->storage['order_item_maint_file_mimetype'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_mimetype'];
    }

    public function getOrderItemMaintFileFilenameComplete(): string
    {
        if (!isset($this->storage['order_item_maint_file_filename']) || !isset($this->storage['order_item_maint_file_extension'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_filename'] . '.' . $this->storage['order_item_maint_file_extension'];
    }

    public function getOrderItemMaintFileTimeCreate(): string
    {
        if (!isset($this->storage['order_item_maint_file_time_create'])) {
            return '';
        }
        return $this->storage['order_item_maint_file_time_create'];
    }

    /**
     * @return \DateTime|null
     */
    public function getOrderItemMaintFileTimeCreateAsDateTime()
    {
        try {
            $dateTime = new \DateTime($this->getOrderItemMaintFileTimeCreate());
        } catch (\Exception $ex) {
            return null;
        }
        if (!$dateTime instanceof \DateTime) {
            return null;
        }
        return $dateTime;
    }

    public function getOrderItemMaintFileTimeCreateAsUnixtime(): int
    {
        if (empty($dateTime = $this->getOrderItemMaintFileTimeCreateAsDateTime())) {
            return -1;
        }
        return $dateTime->getTimestamp();
    }
}
