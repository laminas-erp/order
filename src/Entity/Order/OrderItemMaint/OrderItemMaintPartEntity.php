<?php

namespace Lerp\Order\Entity\Order\OrderItemMaint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemMaintPartEntity extends AbstractEntity
{

    protected $primaryKey = 'order_item_maint_part_uuid';

    public array $mapping = [
        'order_item_maint_part_uuid' => 'order_item_maint_part_uuid',
        'order_item_maint_uuid' => 'order_item_maint_uuid',
        'order_item_maint_gotten_uuid' => 'order_item_maint_gotten_uuid',
        'product_uuid' => 'product_uuid',
        'order_item_maint_part_quantity' => 'order_item_maint_part_quantity',
        'quantityunit_uuid' => 'quantityunit_uuid',
        'order_item_maint_part_time_create' => 'order_item_maint_part_time_create',
    ];

    /**
     * @return string
     */
    public function getOrderItemMaintPartUuid()
    {
        if (!isset($this->storage['order_item_maint_part_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_part_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintUuid()
    {
        if (!isset($this->storage['order_item_maint_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_uuid'];
    }

    /**
     * @return string
     */
    public function getOrderItemMaintGottenUuid()
    {
        if (!isset($this->storage['order_item_maint_gotten_uuid'])) {
            return '';
        }
        return $this->storage['order_item_maint_gotten_uuid'];
    }

    /**
     * @return string
     */
    public function getProductUuid()
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    /**
     * @return float
     */
    public function getOrderItemMaintPartQuantity()
    {
        if (!isset($this->storage['order_item_maint_part_quantity'])) {
            return 0;
        }
        return $this->storage['order_item_maint_part_quantity'];
    }

    public function setOrderItemMaintPartQuantity(float $quantity): void
    {
        $this->storage['order_item_maint_part_quantity'] = $quantity;
    }

    /**
     * @return string
     */
    public function getQuantityunitUuid()
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    /**
     * @return string
     */
    public function getOrderItemMaintPartTimeCreate()
    {
        if (!isset($this->storage['order_item_maint_part_time_create'])) {
            return '';
        }
        return $this->storage['order_item_maint_part_time_create'];
    }
}
