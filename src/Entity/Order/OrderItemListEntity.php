<?php

namespace Lerp\Order\Entity\Order;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OrderItemListEntity extends AbstractEntity
{
    public array $mapping = [
        'order_item_list_uuid' => 'order_item_list_uuid',
        'order_item_uuid' => 'order_item_uuid',
        'product_uuid_parent' => 'product_uuid_parent',
        'product_uuid' => 'product_uuid',
        'order_item_list_quantity' => 'order_item_list_quantity',
        'order_item_list_order_priority' => 'order_item_list_order_priority',
        'product_structure' => 'product_structure',
        'product_text_short' => 'product_text_short',
        'product_text_long' => 'product_text_long',
        'product_briefing' => 'product_briefing',
        'quantityunit_uuid' => 'quantityunit_uuid',
        'product_no_no' => 'product_no_no',
        'order_uuid' => 'order_uuid',
        'cost_centre_id' => 'cost_centre_id',
        'quantityunit_label' => 'quantityunit_label',
        'quantityunit_resolution' => 'quantityunit_resolution',
        'quantityunit_resolution_group' => 'quantityunit_resolution_group',
        'count_factoryorder' => 'count_factoryorder',
    ];

    protected $primaryKey = 'order_item_list_uuid';

    public function getOrderItemListUuid(): string
    {
        if (!isset($this->storage['order_item_list_uuid'])) {
            return '';
        }
        return $this->storage['order_item_list_uuid'];
    }

    public function setOrderItemListUuid(string $orderItemListUuid): void
    {
        $this->storage['order_item_list_uuid'] = $orderItemListUuid;
    }

    public function getOrderItemUuid(): string
    {
        if (!isset($this->storage['order_item_uuid'])) {
            return '';
        }
        return $this->storage['order_item_uuid'];
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        $this->storage['order_item_uuid'] = $orderItemUuid;
    }

    public function getProductUuidParent(): string
    {
        if (!isset($this->storage['product_uuid_parent'])) {
            return '';
        }
        return $this->storage['product_uuid_parent'];
    }

    public function setProductUuidParent(string $productUuidParent): void
    {
        $this->storage['product_uuid_parent'] = $productUuidParent;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getOrderItemListQuantity(): float
    {
        if (!isset($this->storage['order_item_list_quantity'])) {
            return 0;
        }
        return $this->storage['order_item_list_quantity'];
    }

    public function setOrderItemListQuantity(float $orderItemListQuantity): void
    {
        $this->storage['order_item_list_quantity'] = $orderItemListQuantity;
    }

    public function getOrderItemListOrderPriority(): int
    {
        if (!isset($this->storage['order_item_list_order_priority'])) {
            return 0;
        }
        return $this->storage['order_item_list_order_priority'];
    }

    public function setOrderItemListOrderPriority(int $orderItemListOrderPriority): void
    {
        $this->storage['order_item_list_order_priority'] = $orderItemListOrderPriority;
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function setProductStructure(string $productStructure): void
    {
        $this->storage['product_structure'] = $productStructure;
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function setProductTextShort(string $productTextShort): void
    {
        $this->storage['product_text_short'] = $productTextShort;
    }

    public function getProductTextLong(): string
    {
        if (!isset($this->storage['product_text_long'])) {
            return '';
        }
        return $this->storage['product_text_long'];
    }

    public function setProductTextLong(string $productTextLong): void
    {
        $this->storage['product_text_long'] = $productTextLong;
    }

    public function getProductBriefing(): string
    {
        if (!isset($this->storage['product_briefing'])) {
            return '';
        }
        return $this->storage['product_briefing'];
    }

    public function setProductBriefing(string $productBriefing): void
    {
        $this->storage['product_briefing'] = $productBriefing;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function setProductNoNo(int $productNoNo): void
    {
        $this->storage['product_no_no'] = $productNoNo;
    }

    public function getOrderUuid(): string
    {
        if (!isset($this->storage['order_uuid'])) {
            return '';
        }
        return $this->storage['order_uuid'];
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->storage['order_uuid'] = $orderUuid;
    }

    public function getCostCentreId(): int
    {
        if (!isset($this->storage['cost_centre_id'])) {
            return 0;
        }
        return $this->storage['cost_centre_id'];
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->storage['cost_centre_id'] = $costCentreId;
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function setQuantityunitLabel(string $quantityunitLabel): void
    {
        $this->storage['quantityunit_label'] = $quantityunitLabel;
    }

    public function getQuantityunitResolution(): float
    {
        if (!isset($this->storage['quantityunit_resolution'])) {
            return 0;
        }
        return $this->storage['quantityunit_resolution'];
    }

    public function setQuantityunitResolution(float $quantityunitResolution): void
    {
        $this->storage['quantityunit_resolution'] = $quantityunitResolution;
    }

    public function getQuantityunitResolutionGroup(): string
    {
        if (!isset($this->storage['quantityunit_resolution_group'])) {
            return '';
        }
        return $this->storage['quantityunit_resolution_group'];
    }

    public function setQuantityunitResolutionGroup(string $quantityunitResolutionGroup): void
    {
        $this->storage['quantityunit_resolution_group'] = $quantityunitResolutionGroup;
    }

    public function getCountFactoryorder(): int
    {
        if (!isset($this->storage['count_factoryorder'])) {
            return 0;
        }
        return $this->storage['count_factoryorder'];
    }

    public function setCountFactoryorder(int $countFactoryorder): void
    {
        $this->storage['count_factoryorder'] = $countFactoryorder;
    }
}
