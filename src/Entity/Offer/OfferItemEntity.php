<?php

namespace Lerp\Order\Entity\Offer;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OfferItemEntity extends AbstractEntity
{
    public array $mapping = [
        'offer_item_uuid'               => 'offer_item_uuid',
        'offer_item_id'                 => 'offer_item_id',
        'offer_uuid'                    => 'offer_uuid',
        'product_uuid'                  => 'product_uuid',
        'offer_item_text_short'         => 'offer_item_text_short',
        'offer_item_text_long'          => 'offer_item_text_long',
        'offer_item_quantity'           => 'offer_item_quantity',
        'quantityunit_uuid'             => 'quantityunit_uuid',
        'offer_item_price'              => 'offer_item_price',
        'offer_item_price_total'        => 'offer_item_price_total',
        'cost_centre_id'                => 'cost_centre_id',
        'offer_item_time_create'        => 'offer_item_time_create',
        'offer_item_time_update'        => 'offer_item_time_update',
        'offer_item_order_priority'     => 'offer_item_order_priority',
        'offer_item_taxp'               => 'offer_item_taxp',
        'offer_item_price_total_end'    => 'offer_item_price_total_end',
        'offer_no'                      => 'offer_no',
        'offer_no_compl'                => 'offer_no_compl',
        'customer_uuid'                 => 'customer_uuid',
        'cost_centre_id_offer'          => 'cost_centre_id_offer',
        'location_place_uuid'           => 'location_place_uuid',
        'offer_time_create'             => 'offer_time_create',
        'offer_time_create_unix'        => 'offer_time_create_unix',
        'user_uuid_create'              => 'user_uuid_create',
        'user_uuid_update'              => 'user_uuid_update',
        'product_dirty'                 => 'product_dirty',
        'product_structure'             => 'product_structure',
        'product_text_short'            => 'product_text_short',
        'product_no_no'                 => 'product_no_no',
        'cost_centre_label'             => 'cost_centre_label',
        'quantityunit_name'             => 'quantityunit_name',
        'quantityunit_label'            => 'quantityunit_label',
        'quantityunit_resolution'       => 'quantityunit_resolution',
        'quantityunit_resolution_group' => 'quantityunit_resolution_group',
    ];

    protected $primaryKey = 'offer_item_uuid';

    public function getOfferItemUuid(): string
    {
        if (!isset($this->storage['offer_item_uuid'])) {
            return '';
        }
        return $this->storage['offer_item_uuid'];
    }

    public function setOfferItemUuid(string $offerItemUuid): void
    {
        $this->storage['offer_item_uuid'] = $offerItemUuid;
    }

    public function getOfferItemId(): int
    {
        if (!isset($this->storage['offer_item_id'])) {
            return 0;
        }
        return $this->storage['offer_item_id'];
    }

    public function setOfferItemId(int $offerItemId): void
    {
        $this->storage['offer_item_id'] = $offerItemId;
    }

    public function getOfferUuid(): string
    {
        if (!isset($this->storage['offer_uuid'])) {
            return '';
        }
        return $this->storage['offer_uuid'];
    }

    public function setOfferUuid(string $offerUuid): void
    {
        $this->storage['offer_uuid'] = $offerUuid;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getOfferItemTextShort(): string
    {
        if (!isset($this->storage['offer_item_text_short'])) {
            return '';
        }
        return $this->storage['offer_item_text_short'];
    }

    public function setOfferItemTextShort(string $offerItemTextShort): void
    {
        $this->storage['offer_item_text_short'] = $offerItemTextShort;
    }

    public function getOfferItemTextLong(): string
    {
        if (!isset($this->storage['offer_item_text_long'])) {
            return '';
        }
        return $this->storage['offer_item_text_long'];
    }

    public function setOfferItemTextLong(string $offerItemTextLong): void
    {
        $this->storage['offer_item_text_long'] = $offerItemTextLong;
    }

    public function getOfferItemQuantity(): float
    {
        if (!isset($this->storage['offer_item_quantity'])) {
            return 0;
        }
        return $this->storage['offer_item_quantity'];
    }

    public function setOfferItemQuantity(float $offerItemQuantity): void
    {
        $this->storage['offer_item_quantity'] = $offerItemQuantity;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getOfferItemPrice(): float
    {
        if (!isset($this->storage['offer_item_price'])) {
            return 0;
        }
        return $this->storage['offer_item_price'];
    }

    public function setOfferItemPrice(float $offerItemPrice): void
    {
        $this->storage['offer_item_price'] = $offerItemPrice;
    }

    public function getOfferItemPriceTotal(): float
    {
        if (!isset($this->storage['offer_item_price_total'])) {
            return 0;
        }
        return $this->storage['offer_item_price_total'];
    }

    public function setOfferItemPriceTotal(float $offerItemPriceTotal): void
    {
        $this->storage['offer_item_price_total'] = $offerItemPriceTotal;
    }

    public function getCostCentreId(): int
    {
        if (!isset($this->storage['cost_centre_id'])) {
            return 0;
        }
        return $this->storage['cost_centre_id'];
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->storage['cost_centre_id'] = $costCentreId;
    }

    public function getOfferItemTimeCreate(): string
    {
        if (!isset($this->storage['offer_item_time_create'])) {
            return '';
        }
        return $this->storage['offer_item_time_create'];
    }

    public function setOfferItemTimeCreate(string $offerItemTimeCreate): void
    {
        $this->storage['offer_item_time_create'] = $offerItemTimeCreate;
    }

    public function getOfferItemTimeUpdate(): string
    {
        if (!isset($this->storage['offer_item_time_update'])) {
            return '';
        }
        return $this->storage['offer_item_time_update'];
    }

    public function setOfferItemTimeUpdate(string $offerItemTimeUpdate): void
    {
        $this->storage['offer_item_time_update'] = $offerItemTimeUpdate;
    }

    public function getOfferItemOrderPriority(): int
    {
        if (!isset($this->storage['offer_item_order_priority'])) {
            return 0;
        }
        return $this->storage['offer_item_order_priority'];
    }

    public function setOfferItemOrderPriority(int $offerItemOrderPriority): void
    {
        $this->storage['offer_item_order_priority'] = $offerItemOrderPriority;
    }

    public function getOfferItemTaxp(): int
    {
        if (!isset($this->storage['offer_item_taxp'])) {
            return 0;
        }
        return $this->storage['offer_item_taxp'];
    }

    public function setOfferItemTaxp(int $offerItemTaxp): void
    {
        $this->storage['offer_item_taxp'] = $offerItemTaxp;
    }

    public function getOfferItemPriceTotalEnd()
    {
        if (!isset($this->storage['offer_item_price_total_end'])) {
            return '';
        }
        return $this->storage['offer_item_price_total_end'];
    }

    public function setOfferItemPriceTotalEnd($offerItemPriceTotalEnd): void
    {
        $this->storage['offer_item_price_total_end'] = $offerItemPriceTotalEnd;
    }

    public function getOfferNo(): int
    {
        if (!isset($this->storage['offer_no'])) {
            return 0;
        }
        return $this->storage['offer_no'];
    }

    public function setOfferNo(int $offerNo): void
    {
        $this->storage['offer_no'] = $offerNo;
    }

    public function getOfferNoCompl(): string
    {
        if (!isset($this->storage['offer_no_compl'])) {
            return '';
        }
        return $this->storage['offer_no_compl'];
    }

    public function setOfferNoCompl(string $offerNoCompl): void
    {
        $this->storage['offer_no_compl'] = $offerNoCompl;
    }

    public function getCustomerUuid(): string
    {
        if (!isset($this->storage['customer_uuid'])) {
            return '';
        }
        return $this->storage['customer_uuid'];
    }

    public function setCustomerUuid(string $customerUuid): void
    {
        $this->storage['customer_uuid'] = $customerUuid;
    }

    public function getCostCentreIdOffer(): int
    {
        if (!isset($this->storage['cost_centre_id_offer'])) {
            return 0;
        }
        return $this->storage['cost_centre_id_offer'];
    }

    public function setCostCentreIdOffer(int $costCentreIdOffer): void
    {
        $this->storage['cost_centre_id_offer'] = $costCentreIdOffer;
    }

    public function getLocationPlaceUuid(): int
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return 0;
        }
        return $this->storage['location_place_uuid'];
    }

    public function setLocationPlaceUuid(int $locationPlaceUuid): void
    {
        $this->storage['location_place_uuid'] = $locationPlaceUuid;
    }

    public function getOfferTimeCreate(): string
    {
        if (!isset($this->storage['offer_time_create'])) {
            return '';
        }
        return $this->storage['offer_time_create'];
    }

    public function setOfferTimeCreate(string $offerTimeCreate): void
    {
        $this->storage['offer_time_create'] = $offerTimeCreate;
    }

    public function getOfferTimeCreateUnix()
    {
        if (!isset($this->storage['offer_time_create_unix'])) {
            return '';
        }
        return $this->storage['offer_time_create_unix'];
    }

    public function setOfferTimeCreateUnix($offerTimeCreateUnix): void
    {
        $this->storage['offer_time_create_unix'] = $offerTimeCreateUnix;
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function setUserUuidUpdate(string $userUuidUpdate): void
    {
        $this->storage['user_uuid_update'] = $userUuidUpdate;
    }

    public function getProductDirty(): bool
    {
        if (!isset($this->storage['product_dirty'])) {
            return '';
        }
        return $this->storage['product_dirty'];
    }

    public function setProductDirty(bool $productDirty): void
    {
        $this->storage['product_dirty'] = $productDirty;
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function setProductStructure(string $productStructure): void
    {
        $this->storage['product_structure'] = $productStructure;
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function setProductTextShort(string $productTextShort): void
    {
        $this->storage['product_text_short'] = $productTextShort;
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function setProductNoNo(int $productNoNo): void
    {
        $this->storage['product_no_no'] = $productNoNo;
    }

    public function getCostCentreLabel(): string
    {
        if (!isset($this->storage['cost_centre_label'])) {
            return '';
        }
        return $this->storage['cost_centre_label'];
    }

    public function setCostCentreLabel(string $costCentreLabel): void
    {
        $this->storage['cost_centre_label'] = $costCentreLabel;
    }

    public function getQuantityunitName(): string
    {
        if (!isset($this->storage['quantityunit_name'])) {
            return '';
        }
        return $this->storage['quantityunit_name'];
    }

    public function setQuantityunitName(string $quantityunitName): void
    {
        $this->storage['quantityunit_name'] = $quantityunitName;
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function setQuantityunitLabel(string $quantityunitLabel): void
    {
        $this->storage['quantityunit_label'] = $quantityunitLabel;
    }

    public function getQuantityunitResolution(): float
    {
        if (!isset($this->storage['quantityunit_resolution'])) {
            return 0;
        }
        return $this->storage['quantityunit_resolution'];
    }

    public function setQuantityunitResolution(float $quantityunitResolution): void
    {
        $this->storage['quantityunit_resolution'] = $quantityunitResolution;
    }

    public function getQuantityunitResolutionGroup(): string
    {
        if (!isset($this->storage['quantityunit_resolution_group'])) {
            return '';
        }
        return $this->storage['quantityunit_resolution_group'];
    }

    public function setQuantityunitResolutionGroup(string $quantityunitResolutionGroup): void
    {
        $this->storage['quantityunit_resolution_group'] = $quantityunitResolutionGroup;
    }
}
