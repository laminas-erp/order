<?php

namespace Lerp\Order\Entity\Offer;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OfferEntity extends AbstractEntity
{
    public array $mapping = [
        'offer_uuid'                     => 'offer_uuid',
        'offer_no'                       => 'offer_no',
        'offer_no_compl'                 => 'offer_no_compl',
        'customer_uuid'                  => 'customer_uuid',
        'cost_centre_id'                 => 'cost_centre_id',
        'location_place_uuid'            => 'location_place_uuid',
        'offer_time_create'              => 'offer_time_create',
        'offer_time_create_unix'         => 'offer_time_create_unix',
        'offer_time_update'              => 'offer_time_update',
        'user_uuid_create'               => 'user_uuid_create',
        'user_uuid_update'               => 'user_uuid_update',
        'offer_lang_iso'                 => 'offer_lang_iso',
        'customer_no'                    => 'customer_no',
        'customer_name'                  => 'customer_name',
        'customer_lang_iso'              => 'customer_lang_iso',
        'cost_centre_label'              => 'cost_centre_label',
        'location_place_label'           => 'location_place_label',
        'user_login_create'              => 'user_login_create',
        'user_email_create'              => 'user_email_create',
        'user_details_name_first_create' => 'user_details_name_first_create',
        'user_details_name_last_create'  => 'user_details_name_last_create',
        'user_login_update'              => 'user_login_update',
        'user_email_update'              => 'user_email_update',
        'user_details_name_first_update' => 'user_details_name_first_update',
        'user_details_name_last_update'  => 'user_details_name_last_update',
        'order_uuid'                     => 'order_uuid',
        'order_no'                       => 'order_no',
    ];

    protected $primaryKey = 'offer_uuid';
    protected array $databaseFieldsInsert = ['offer_uuid', 'offer_no', 'offer_no_compl', 'customer_uuid', 'cost_centre_id', 'user_uuid_create', 'location_place_uuid'];
    protected array $databaseFieldsUpdate = ['customer_uuid', 'cost_centre_id', 'user_uuid_update', 'offer_lang_iso', 'location_place_uuid'];

    public function getOfferUuid(): string
    {
        if (!isset($this->storage['offer_uuid'])) {
            return '';
        }
        return $this->storage['offer_uuid'];
    }

    public function setOfferUuid(string $offerUuid): void
    {
        $this->storage['offer_uuid'] = $offerUuid;
    }

    public function getOfferNo(): int
    {
        if (!isset($this->storage['offer_no'])) {
            return 0;
        }
        return $this->storage['offer_no'];
    }

    public function setOfferNo(int $offerNo): void
    {
        $this->storage['offer_no'] = $offerNo;
    }

    public function getOfferNoCompl(): string
    {
        if (!isset($this->storage['offer_no_compl'])) {
            return '';
        }
        return $this->storage['offer_no_compl'];
    }

    public function setOfferNoCompl(string $offerNoCompl): void
    {
        $this->storage['offer_no_compl'] = $offerNoCompl;
    }

    public function getCustomerUuid(): string
    {
        if (!isset($this->storage['customer_uuid'])) {
            return '';
        }
        return $this->storage['customer_uuid'];
    }

    public function setCustomerUuid(string $customerUuid): void
    {
        $this->storage['customer_uuid'] = $customerUuid;
    }

    public function getCostCentreId(): int
    {
        if (!isset($this->storage['cost_centre_id'])) {
            return 0;
        }
        return $this->storage['cost_centre_id'];
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->storage['cost_centre_id'] = $costCentreId;
    }

    public function getLocationPlaceUuid(): string
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return '';
        }
        return $this->storage['location_place_uuid'];
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->storage['location_place_uuid'] = $locationPlaceUuid;
    }

    public function getOfferTimeCreate(): string
    {
        if (!isset($this->storage['offer_time_create'])) {
            return '';
        }
        return $this->storage['offer_time_create'];
    }

    public function setOfferTimeCreate(string $offerTimeCreate): void
    {
        $this->storage['offer_time_create'] = $offerTimeCreate;
    }

    public function getOfferTimeCreateUnix()
    {
        if (!isset($this->storage['offer_time_create_unix'])) {
            return '';
        }
        return $this->storage['offer_time_create_unix'];
    }

    public function setOfferTimeCreateUnix($offerTimeCreateUnix): void
    {
        $this->storage['offer_time_create_unix'] = $offerTimeCreateUnix;
    }

    public function getOfferTimeUpdate(): string
    {
        if (!isset($this->storage['offer_time_update'])) {
            return '';
        }
        return $this->storage['offer_time_update'];
    }

    public function setOfferTimeUpdate(string $offerTimeUpdate): void
    {
        $this->storage['offer_time_update'] = $offerTimeUpdate;
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function setUserUuidUpdate(string $userUuidUpdate): void
    {
        $this->storage['user_uuid_update'] = $userUuidUpdate;
    }

    public function getOfferLangIso(): string
    {
        if (!isset($this->storage['offer_lang_iso'])) {
            return '';
        }
        return $this->storage['offer_lang_iso'];
    }

    public function setOfferLangIso(string $offerLangIso): void
    {
        $this->storage['offer_lang_iso'] = $offerLangIso;
    }

    public function getCustomerNo(): int
    {
        if (!isset($this->storage['customer_no'])) {
            return 0;
        }
        return $this->storage['customer_no'];
    }

    public function setCustomerNo(int $customerNo): void
    {
        $this->storage['customer_no'] = $customerNo;
    }

    public function getCustomerName(): string
    {
        if (!isset($this->storage['customer_name'])) {
            return '';
        }
        return $this->storage['customer_name'];
    }

    public function setCustomerName(string $customerName): void
    {
        $this->storage['customer_name'] = $customerName;
    }

    public function getCustomerLangIso(): string
    {
        if (!isset($this->storage['customer_lang_iso'])) {
            return '';
        }
        return $this->storage['customer_lang_iso'];
    }

    public function setCustomerLangIso(string $customerLangIso): void
    {
        $this->storage['customer_lang_iso'] = $customerLangIso;
    }

    public function getCostCentreLabel(): string
    {
        if (!isset($this->storage['cost_centre_label'])) {
            return '';
        }
        return $this->storage['cost_centre_label'];
    }

    public function setCostCentreLabel(string $costCentreLabel): void
    {
        $this->storage['cost_centre_label'] = $costCentreLabel;
    }

    public function getLocationPlaceLabel(): string
    {
        if (!isset($this->storage['location_place_label'])) {
            return '';
        }
        return $this->storage['location_place_label'];
    }

    public function setLocationPlaceLabel(string $locationPlaceLabel): void
    {
        $this->storage['location_place_label'] = $locationPlaceLabel;
    }

    public function getUserLoginCreate(): string
    {
        if (!isset($this->storage['user_login_create'])) {
            return '';
        }
        return $this->storage['user_login_create'];
    }

    public function setUserLoginCreate(string $userLoginCreate): void
    {
        $this->storage['user_login_create'] = $userLoginCreate;
    }

    public function getUserEmailCreate(): string
    {
        if (!isset($this->storage['user_email_create'])) {
            return '';
        }
        return $this->storage['user_email_create'];
    }

    public function setUserEmailCreate(string $userEmailCreate): void
    {
        $this->storage['user_email_create'] = $userEmailCreate;
    }

    public function getUserDetailsNameFirstCreate(): string
    {
        if (!isset($this->storage['user_details_name_first_create'])) {
            return '';
        }
        return $this->storage['user_details_name_first_create'];
    }

    public function setUserDetailsNameFirstCreate(string $userDetailsNameFirstCreate): void
    {
        $this->storage['user_details_name_first_create'] = $userDetailsNameFirstCreate;
    }

    public function getUserDetailsNameLastCreate(): string
    {
        if (!isset($this->storage['user_details_name_last_create'])) {
            return '';
        }
        return $this->storage['user_details_name_last_create'];
    }

    public function setUserDetailsNameLastCreate(string $userDetailsNameLast): void
    {
        $this->storage['user_details_name_last_create'] = $userDetailsNameLast;
    }

    public function getUserLoginUpdate(): string
    {
        if (!isset($this->storage['user_login_update'])) {
            return '';
        }
        return $this->storage['user_login_update'];
    }

    public function setUserLoginUpdate(string $userLoginUpdate): void
    {
        $this->storage['user_login_update'] = $userLoginUpdate;
    }

    public function getUserEmailUpdate(): string
    {
        if (!isset($this->storage['user_email_update'])) {
            return '';
        }
        return $this->storage['user_email_update'];
    }

    public function setUserEmailUpdate(string $userEmailUpdate): void
    {
        $this->storage['user_email_update'] = $userEmailUpdate;
    }

    public function getUserDetailsNameFirstUpdate(): string
    {
        if (!isset($this->storage['user_details_name_first_update'])) {
            return '';
        }
        return $this->storage['user_details_name_first_update'];
    }

    public function setUserDetailsNameFirstUpdate(string $userDetailsNameFirstUpdate): void
    {
        $this->storage['user_details_name_first_update'] = $userDetailsNameFirstUpdate;
    }

    public function getUserDetailsNameLastUpdate(): string
    {
        if (!isset($this->storage['user_details_name_last_update'])) {
            return '';
        }
        return $this->storage['user_details_name_last_update'];
    }

    public function setUserDetailsNameLastUpdate(string $userDetailsNameLastUpdate): void
    {
        $this->storage['user_details_name_last_update'] = $userDetailsNameLastUpdate;
    }

    public function getOrderUuid(): string
    {
        if (!isset($this->storage['order_uuid'])) {
            return '';
        }
        return $this->storage['order_uuid'];
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->storage['order_uuid'] = $orderUuid;
    }

    public function getOrderNo(): int
    {
        if (!isset($this->storage['order_no'])) {
            return 0;
        }
        return $this->storage['order_no'];
    }

    public function setOrderNo(int $orderNo): void
    {
        $this->storage['order_no'] = $orderNo;
    }
}
