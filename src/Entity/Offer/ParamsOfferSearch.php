<?php

namespace Lerp\Order\Entity\Offer;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Filter\DateTimeFormatter;

class ParamsOfferSearch extends ParamsBase
{
    protected DateTimeFormatter $dateTimeFormatter;
    protected string $dateTimeFormat = 'Y-m-d H:i:s.u';
    protected FilterChainStringSanitize $stringFilter;
    protected array $orderFieldsAvailable = ['offer_no_compl', 'customer_no', 'customer_name', 'order_no_compl', 'offer_time_create'];
    protected string $offerNoCompl;
    protected string $orderNoCompl;
    protected int $customerNo;
    protected string $customerName;
    protected int $costCentreId;
    protected string $locationPlaceUuid;
    protected string $timeFrom;
    protected string $timeTo;

    /**
     * ParamsOfferSearch constructor.
     */
    public function __construct()
    {
        $this->dateTimeFormatter = new DateTimeFormatter();
        $this->dateTimeFormatter->setFormat($this->dateTimeFormat);
        $this->stringFilter = new FilterChainStringSanitize();
    }

    public function setOfferNoCompl(string $offerNoCompl): void
    {
        $this->offerNoCompl = $this->stringFilter->filter($offerNoCompl);
    }

    public function setOrderNoCompl(string $orderNoCompl): void
    {
        $this->orderNoCompl = $this->stringFilter->filter($orderNoCompl);
    }

    public function setCustomerNo(int $customerNo): void
    {
        $this->customerNo = $customerNo;
    }

    public function setCustomerName(string $customerName): void
    {
        $this->customerName = filter_var($customerName, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->costCentreId = $costCentreId;
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->locationPlaceUuid = $locationPlaceUuid;
    }

    public function setTimeFrom(string $timeFrom): void
    {
        $timeFrom = $this->dateTimeFormatter->filter($timeFrom);
        if ($timeFrom instanceof \DateTime) {
            $timeFrom = $timeFrom->format($this->dateTimeFormat);
        }
        if (!is_string($timeFrom)) {
            return;
        }
        $this->timeFrom = $timeFrom;
    }

    public function setTimeTo(string $timeTo): void
    {
        $timeTo = $this->dateTimeFormatter->filter($timeTo);
        if ($timeTo instanceof \DateTime) {
            $timeTo = $timeTo->format($this->dateTimeFormat);
        }
        if (!is_string($timeTo)) {
            return;
        }
        $this->timeTo = $timeTo;
    }

    public function getOfferNoCompl(): string
    {
        return $this->offerNoCompl;
    }

    public function getOrderNoCompl(): string
    {
        return $this->orderNoCompl;
    }

    public function getCustomerNo(): int
    {
        return $this->customerNo;
    }

    public function getCustomerName(): string
    {
        return $this->customerName;
    }

    public function getCostCentreId(): int
    {
        return $this->costCentreId;
    }

    public function getLocationPlaceUuid(): string
    {
        return $this->locationPlaceUuid;
    }

    public function getTimeFrom(): string
    {
        return $this->timeFrom;
    }

    public function getTimeTo(): string
    {
        return $this->timeTo;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setOfferNoCompl($qp['offer_no_compl'] ?? '');
        $this->setOrderNoCompl($qp['order_no_compl'] ?? '');
        $this->setCustomerNo(intval($qp['customer_no']) ?? 0);
        $this->setCustomerName($qp['customer_name'] ?? '');
        $this->setCostCentreId($qp['cost_centre_id'] ?? 0);
        $this->setLocationPlaceUuid($qp['location_place_uuid'] ?? 0);
        $this->setTimeFrom($qp['offer_time_create_from'] ?? '');
        $this->setTimeTo($qp['offer_time_create_to'] ?? '');
    }

    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        parent::computeSelect($select);
        if (!empty($this->offerNoCompl)) {
            $select->where->like(new Expression('offer_no_compl'), '%' . $this->offerNoCompl . '%');
        }
        if (!empty($this->orderNoCompl)) {
            $select->where->like(new Expression('order_no_compl'), '%' . $this->orderNoCompl . '%');
        }
        if (!empty($this->customerNo)) {
            $select->where->like(new Expression('CAST(customer_no AS TEXT)'), '%' . $this->customerNo . '%');
        }
        if (!empty($this->customerName)) {
            $select->where->like('customer_name', '%' . $this->customerName . '%');
        }
        if (!empty($this->costCentreId)) {
            $select->where(['cost_centre_id' => $this->costCentreId]);
        }
        if (!empty($this->locationPlaceUuid)) {
            $select->where(['location_place_uuid' => $this->locationPlaceUuid]);
        }
        if (!empty($this->timeFrom)) {
            $select->where->greaterThanOrEqualTo('offer_time_create', $this->timeFrom);
        }
        if (!empty($this->timeTo)) {
            $select->where->lessThanOrEqualTo('offer_time_create', $this->timeTo);
        }
    }
}
